import { __decorate, __extends } from "tslib";
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { ElementoTablaComponent } from '../elemento-tabla/elemento-tabla.component';
import { BotonIconoEstilo } from './boton-icono-estilo';
import { Configuracion } from '../../configuracion';
var BotonIconoComponent = /** @class */ (function (_super) {
    __extends(BotonIconoComponent, _super);
    function BotonIconoComponent(cd) {
        var _this = _super.call(this, cd) || this;
        _this.cd = cd;
        return _this;
    }
    BotonIconoComponent.prototype.ngOnInit = function () {
    };
    BotonIconoComponent.prototype.getClase = function () {
        if (this.col.estilo != null) {
            return Configuracion.obtenerClaseIcono(this.col.estilo);
        }
        else {
            return new BotonIconoEstilo(this.col.icon, this.col.clase);
        }
    };
    BotonIconoComponent.prototype.click = function () {
        if (this.col.estilo) {
            this.ejecutarFuncionGenerico();
        }
        else {
            this.ejecutarFuncion(this.elemento);
        }
    };
    BotonIconoComponent.prototype.ejecutarFuncion = function (item) {
        throw new Error("Method not implemented.");
    };
    BotonIconoComponent.prototype.ejecutarFuncionGenerico = function () {
        switch (this.col.estilo) {
            case "editar":
                this.observador.editar(this.elemento.id);
                break;
            case "eliminar":
                this.observador.eliminar(this.elemento.id);
                break;
            case "ver":
                this.observador.ver(this.elemento.id);
                break;
            default:
                console.log("no esta implementado el metodo " + this.col.estilo);
                break;
        }
    };
    BotonIconoComponent.ctorParameters = function () { return [
        { type: ChangeDetectorRef }
    ]; };
    BotonIconoComponent = __decorate([
        Component({
            selector: 'app-boton-icono',
            template: "<button type=\"button\" (click)=\"click()\" [ngClass]=\"[getClase().clase]\"><i [ngClass]=\"[getClase().icono]\"></i></button>",
            styles: [""]
        })
    ], BotonIconoComponent);
    return BotonIconoComponent;
}(ElementoTablaComponent));
export { BotonIconoComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm90b24taWNvbm8uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaXZhaS1nZW5lcmljby1saWJyZXJpYS8iLCJzb3VyY2VzIjpbImxpYi9nZW5lcmljby9mb3JtbHlzL2JvdG9uLWljb25vL2JvdG9uLWljb25vLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsaUJBQWlCLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzVFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQ3BGLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBRXhELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQU9wRDtJQUF5Qyx1Q0FBc0I7SUFFN0QsNkJBQW1CLEVBQW9CO1FBQXZDLFlBQ0Usa0JBQU0sRUFBRSxDQUFDLFNBQ1Y7UUFGa0IsUUFBRSxHQUFGLEVBQUUsQ0FBa0I7O0lBRXZDLENBQUM7SUFFRCxzQ0FBUSxHQUFSO0lBQ0EsQ0FBQztJQUVELHNDQUFRLEdBQVI7UUFDRSxJQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxJQUFFLElBQUksRUFBQztZQUN2QixPQUFPLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQ3pEO2FBQUk7WUFDSCxPQUFPLElBQUksZ0JBQWdCLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMzRDtJQUNILENBQUM7SUFFRCxtQ0FBSyxHQUFMO1FBQ0UsSUFBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBQztZQUNqQixJQUFJLENBQUMsdUJBQXVCLEVBQUUsQ0FBQztTQUNoQzthQUFJO1lBQ0gsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDckM7SUFDSCxDQUFDO0lBRUQsNkNBQWUsR0FBZixVQUFnQixJQUFTO1FBQ3ZCLE1BQU0sSUFBSSxLQUFLLENBQUMseUJBQXlCLENBQUMsQ0FBQztJQUM3QyxDQUFDO0lBRUQscURBQXVCLEdBQXZCO1FBQ0ksUUFBUSxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRTtZQUNyQixLQUFLLFFBQVE7Z0JBQ1QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQU8sSUFBSSxDQUFDLFFBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDaEQsTUFBTTtZQUNWLEtBQUssVUFBVTtnQkFDYixJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBTyxJQUFJLENBQUMsUUFBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUNsRCxNQUFNO1lBQ1IsS0FBSyxLQUFLO2dCQUNSLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFPLElBQUksQ0FBQyxRQUFTLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQzVDLE1BQU07WUFDVDtnQkFDSSxPQUFPLENBQUMsR0FBRyxDQUFDLGlDQUFpQyxHQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQy9ELE1BQU07U0FDakI7SUFDRCxDQUFDOztnQkExQ3FCLGlCQUFpQjs7SUFGNUIsbUJBQW1CO1FBTC9CLFNBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxpQkFBaUI7WUFDM0IsMElBQTJDOztTQUU1QyxDQUFDO09BQ1csbUJBQW1CLENBOEMvQjtJQUFELDBCQUFDO0NBQUEsQUE5Q0QsQ0FBeUMsc0JBQXNCLEdBOEM5RDtTQTlDWSxtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgQ2hhbmdlRGV0ZWN0b3JSZWYsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBFbGVtZW50b1RhYmxhQ29tcG9uZW50IH0gZnJvbSAnLi4vZWxlbWVudG8tdGFibGEvZWxlbWVudG8tdGFibGEuY29tcG9uZW50JztcbmltcG9ydCB7IEJvdG9uSWNvbm9Fc3RpbG8gfSBmcm9tICcuL2JvdG9uLWljb25vLWVzdGlsbyc7XG5pbXBvcnQgeyBBY2Npb25lc0dlbmVyYWxlcyB9IGZyb20gJy4uL2FjY2lvbmVzL2FjY2lvbmVzLWdlbmVyYWxlcyc7XG5pbXBvcnQgeyBDb25maWd1cmFjaW9uIH0gZnJvbSAnLi4vLi4vY29uZmlndXJhY2lvbic7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2FwcC1ib3Rvbi1pY29ubycsXG4gIHRlbXBsYXRlVXJsOiAnLi9ib3Rvbi1pY29uby5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2JvdG9uLWljb25vLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgQm90b25JY29ub0NvbXBvbmVudCBleHRlbmRzIEVsZW1lbnRvVGFibGFDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBcbiAgY29uc3RydWN0b3IocHVibGljIGNkOkNoYW5nZURldGVjdG9yUmVmKSB7XG4gICAgc3VwZXIoY2QpO1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuICBnZXRDbGFzZSgpe1xuICAgIGlmKHRoaXMuY29sLmVzdGlsbyE9bnVsbCl7XG4gICAgICByZXR1cm4gQ29uZmlndXJhY2lvbi5vYnRlbmVyQ2xhc2VJY29ubyh0aGlzLmNvbC5lc3RpbG8pO1xuICAgIH1lbHNle1xuICAgICAgcmV0dXJuIG5ldyBCb3Rvbkljb25vRXN0aWxvKHRoaXMuY29sLmljb24sdGhpcy5jb2wuY2xhc2UpO1xuICAgIH1cbiAgfVxuXG4gIGNsaWNrKCl7XG4gICAgaWYodGhpcy5jb2wuZXN0aWxvKXtcbiAgICAgIHRoaXMuZWplY3V0YXJGdW5jaW9uR2VuZXJpY28oKTtcbiAgICB9ZWxzZXtcbiAgICAgIHRoaXMuZWplY3V0YXJGdW5jaW9uKHRoaXMuZWxlbWVudG8pO1xuICAgIH1cbiAgfVxuXG4gIGVqZWN1dGFyRnVuY2lvbihpdGVtOiBhbnkpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoXCJNZXRob2Qgbm90IGltcGxlbWVudGVkLlwiKTtcbiAgfVxuICBcbiAgZWplY3V0YXJGdW5jaW9uR2VuZXJpY28oKSB7ICAgIFxuICAgICAgc3dpdGNoICh0aGlzLmNvbC5lc3RpbG8pIHtcbiAgICAgICAgICBjYXNlIFwiZWRpdGFyXCI6XG4gICAgICAgICAgICAgIHRoaXMub2JzZXJ2YWRvci5lZGl0YXIoKDxhbnk+dGhpcy5lbGVtZW50bykuaWQpO1xuICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICBjYXNlIFwiZWxpbWluYXJcIjpcbiAgICAgICAgICAgIHRoaXMub2JzZXJ2YWRvci5lbGltaW5hcigoPGFueT50aGlzLmVsZW1lbnRvKS5pZCk7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICBjYXNlIFwidmVyXCI6XG4gICAgICAgICAgICB0aGlzLm9ic2VydmFkb3IudmVyKCg8YW55PnRoaXMuZWxlbWVudG8pLmlkKTtcbiAgICAgICAgICAgICBicmVhazsgICAgICAgICAgXG4gICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJubyBlc3RhIGltcGxlbWVudGFkbyBlbCBtZXRvZG8gXCIrdGhpcy5jb2wuZXN0aWxvKTtcbiAgICAgICAgICAgICAgYnJlYWs7XG4gIH1cbiAgfVxuXG59XG4iXX0=