import { __decorate, __extends } from "tslib";
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ElementoTablaComponent } from '../elemento-tabla/elemento-tabla.component';
var BotonComponent = /** @class */ (function (_super) {
    __extends(BotonComponent, _super);
    function BotonComponent(cd) {
        var _this = _super.call(this, cd) || this;
        _this.cd = cd;
        return _this;
    }
    BotonComponent.prototype.ngOnInit = function () {
    };
    BotonComponent.ctorParameters = function () { return [
        { type: ChangeDetectorRef }
    ]; };
    BotonComponent = __decorate([
        Component({
            selector: 'app-boton',
            template: "<button type=\"button\" [ngClass]=\"[col.clase]\">{{elemento[col.prop]}}</button>",
            styles: [""]
        })
    ], BotonComponent);
    return BotonComponent;
}(ElementoTablaComponent));
export { BotonComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm90b24uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaXZhaS1nZW5lcmljby1saWJyZXJpYS8iLCJzb3VyY2VzIjpbImxpYi9nZW5lcmljby9mb3JtbHlzL2JvdG9uL2JvdG9uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDckUsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFPcEY7SUFBb0Msa0NBQXNCO0lBRXhELHdCQUFtQixFQUFvQjtRQUF2QyxZQUNFLGtCQUFNLEVBQUUsQ0FBQyxTQUNWO1FBRmtCLFFBQUUsR0FBRixFQUFFLENBQWtCOztJQUV2QyxDQUFDO0lBRUQsaUNBQVEsR0FBUjtJQUNBLENBQUM7O2dCQUxxQixpQkFBaUI7O0lBRjVCLGNBQWM7UUFMMUIsU0FBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLFdBQVc7WUFDckIsNkZBQXFDOztTQUV0QyxDQUFDO09BQ1csY0FBYyxDQVMxQjtJQUFELHFCQUFDO0NBQUEsQUFURCxDQUFvQyxzQkFBc0IsR0FTekQ7U0FUWSxjQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIENoYW5nZURldGVjdG9yUmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBFbGVtZW50b1RhYmxhQ29tcG9uZW50IH0gZnJvbSAnLi4vZWxlbWVudG8tdGFibGEvZWxlbWVudG8tdGFibGEuY29tcG9uZW50JztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYXBwLWJvdG9uJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2JvdG9uLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vYm90b24uY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBCb3RvbkNvbXBvbmVudCBleHRlbmRzIEVsZW1lbnRvVGFibGFDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBjZDpDaGFuZ2VEZXRlY3RvclJlZikge1xuICAgIHN1cGVyKGNkKTtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cbn1cbiJdfQ==