import { __decorate, __extends } from "tslib";
import { Component, OnInit, Input, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { ElementoTablaComponent } from '../elemento-tabla/elemento-tabla.component';
var TextoComponent = /** @class */ (function (_super) {
    __extends(TextoComponent, _super);
    function TextoComponent(cd) {
        var _this = _super.call(this, cd) || this;
        _this.cd = cd;
        return _this;
    }
    TextoComponent.prototype.ngAfterViewInit = function () {
    };
    TextoComponent.prototype.ngOnInit = function () {
    };
    TextoComponent.ctorParameters = function () { return [
        { type: ChangeDetectorRef }
    ]; };
    TextoComponent = __decorate([
        Component({
            selector: 'app-texto',
            template: "\n{{propiedad}}\n",
            styles: [""]
        })
    ], TextoComponent);
    return TextoComponent;
}(ElementoTablaComponent));
export { TextoComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dG8uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaXZhaS1nZW5lcmljby1saWJyZXJpYS8iLCJzb3VyY2VzIjpbImxpYi9nZW5lcmljby9mb3JtbHlzL3RleHRvL3RleHRvLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLGlCQUFpQixFQUFFLGFBQWEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzRixPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQVFwRjtJQUFvQyxrQ0FBc0I7SUFFeEQsd0JBQW1CLEVBQW9CO1FBQXZDLFlBQ0Usa0JBQU0sRUFBRSxDQUFDLFNBQ1Y7UUFGa0IsUUFBRSxHQUFGLEVBQUUsQ0FBa0I7O0lBRXZDLENBQUM7SUFFRCx3Q0FBZSxHQUFmO0lBQ0EsQ0FBQztJQUVELGlDQUFRLEdBQVI7SUFDQSxDQUFDOztnQkFScUIsaUJBQWlCOztJQUY1QixjQUFjO1FBTDFCLFNBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxXQUFXO1lBQ3JCLDZCQUFxQzs7U0FFdEMsQ0FBQztPQUNXLGNBQWMsQ0FnQjFCO0lBQUQscUJBQUM7Q0FBQSxBQWhCRCxDQUFvQyxzQkFBc0IsR0FnQnpEO1NBaEJZLGNBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIENoYW5nZURldGVjdG9yUmVmLCBBZnRlclZpZXdJbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBFbGVtZW50b1RhYmxhQ29tcG9uZW50IH0gZnJvbSAnLi4vZWxlbWVudG8tdGFibGEvZWxlbWVudG8tdGFibGEuY29tcG9uZW50JztcblxuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhcHAtdGV4dG8nLFxuICB0ZW1wbGF0ZVVybDogJy4vdGV4dG8uY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi90ZXh0by5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIFRleHRvQ29tcG9uZW50IGV4dGVuZHMgRWxlbWVudG9UYWJsYUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCxBZnRlclZpZXdJbml0ICB7XG4gIFxuICBjb25zdHJ1Y3RvcihwdWJsaWMgY2Q6Q2hhbmdlRGV0ZWN0b3JSZWYpIHtcbiAgICBzdXBlcihjZCk7XG4gIH1cblxuICBuZ0FmdGVyVmlld0luaXQoKTogdm9pZCB7ICAgIFxuICB9XG5cbiAgbmdPbkluaXQoKSB7ICAgICAgXG4gIH1cblxuIFxuXG4gXG5cbn1cbiJdfQ==