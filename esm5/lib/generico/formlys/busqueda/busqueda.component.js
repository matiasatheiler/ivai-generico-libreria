import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
var BusquedaComponent = /** @class */ (function () {
    function BusquedaComponent() {
        this.texto = "";
        this.habilitado = true;
    }
    BusquedaComponent.prototype.ngOnInit = function () {
    };
    BusquedaComponent.prototype.limpiar = function () {
        this.texto = "";
        this.buscar(this.texto);
        this.habilitado = true;
    };
    BusquedaComponent.prototype.buscar = function (texto) {
        this.habilitado = false;
        this.buscador.buscar(texto);
    };
    __decorate([
        Input()
    ], BusquedaComponent.prototype, "buscador", void 0);
    __decorate([
        Input()
    ], BusquedaComponent.prototype, "texto", void 0);
    BusquedaComponent = __decorate([
        Component({
            selector: 'app-busqueda',
            template: "<div class=\"input-group mb-3\">  \n  <input [disabled]=\"!habilitado\" [(ngModel)]=\"texto\" type=\"text\" class=\"form-control\" placeholder=\"Buscar...\" aria-label=\"Realizar b\u00FAsqueda\" aria-describedby=\"basic-addon2\">\n  <div class=\"input-group-append\" *ngIf=\"!habilitado\">\n    <button  (click)=\"limpiar()\" class=\"btn btn-outline-danger\" type=\"button\"><i class=\"fa fa-times-circle\"></i></button>\n  </div>\n  <div class=\"input-group-append\"  *ngIf=\"habilitado\">\n    <button  (click)=\"buscar(texto)\" class=\"btn btn-outline-primary\" type=\"button\"><i class=\"fa fa-search\"></i></button>\n  </div>\n</div> ",
            styles: [""]
        })
    ], BusquedaComponent);
    return BusquedaComponent;
}());
export { BusquedaComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnVzcXVlZGEuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaXZhaS1nZW5lcmljby1saWJyZXJpYS8iLCJzb3VyY2VzIjpbImxpYi9nZW5lcmljby9mb3JtbHlzL2J1c3F1ZWRhL2J1c3F1ZWRhLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFRekQ7SUFNRTtRQUZBLFVBQUssR0FBUSxFQUFFLENBQUM7UUFDaEIsZUFBVSxHQUFDLElBQUksQ0FBQztJQUNBLENBQUM7SUFFakIsb0NBQVEsR0FBUjtJQUNBLENBQUM7SUFFRCxtQ0FBTyxHQUFQO1FBQ0UsSUFBSSxDQUFDLEtBQUssR0FBQyxFQUFFLENBQUM7UUFDZCxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN4QixJQUFJLENBQUMsVUFBVSxHQUFDLElBQUksQ0FBQztJQUN2QixDQUFDO0lBRUQsa0NBQU0sR0FBTixVQUFPLEtBQUs7UUFDVixJQUFJLENBQUMsVUFBVSxHQUFDLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM5QixDQUFDO0lBbEJEO1FBREMsS0FBSyxFQUFFO3VEQUNVO0lBRWxCO1FBREMsS0FBSyxFQUFFO29EQUNRO0lBSkwsaUJBQWlCO1FBTDdCLFNBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxjQUFjO1lBQ3hCLDJvQkFBd0M7O1NBRXpDLENBQUM7T0FDVyxpQkFBaUIsQ0FzQjdCO0lBQUQsd0JBQUM7Q0FBQSxBQXRCRCxJQXNCQztTQXRCWSxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEJ1c2NhZG9yIH0gZnJvbSAnLi9idXNjYWRvcic7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2FwcC1idXNxdWVkYScsXG4gIHRlbXBsYXRlVXJsOiAnLi9idXNxdWVkYS5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2J1c3F1ZWRhLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgQnVzcXVlZGFDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQgeyAgXG4gIEBJbnB1dCgpXG4gIGJ1c2NhZG9yOkJ1c2NhZG9yOyBcbiAgQElucHV0KClcbiAgdGV4dG86c3RyaW5nPVwiXCI7XG4gIGhhYmlsaXRhZG89dHJ1ZTtcbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG4gIGxpbXBpYXIoKXtcbiAgICB0aGlzLnRleHRvPVwiXCI7XG4gICAgdGhpcy5idXNjYXIodGhpcy50ZXh0byk7XG4gICAgdGhpcy5oYWJpbGl0YWRvPXRydWU7XG4gIH1cblxuICBidXNjYXIodGV4dG8pe1xuICAgIHRoaXMuaGFiaWxpdGFkbz1mYWxzZTtcbiAgICB0aGlzLmJ1c2NhZG9yLmJ1c2Nhcih0ZXh0byk7ICAgIFxuICB9XG5cbn1cbiJdfQ==