import { __decorate } from "tslib";
import { Component, OnInit, ViewChild, ViewContainerRef, Type, Input, AfterViewInit, ComponentFactoryResolver } from '@angular/core';
import { TextoComponent } from '../texto/texto.component';
import { BotonComponent } from '../boton/boton.component';
import { BotonIconoComponent } from '../boton-icono/boton-icono.component';
var FieldGenericoComponent = /** @class */ (function () {
    function FieldGenericoComponent(cfr) {
        this.cfr = cfr;
        this.col = { prop: '' };
    }
    FieldGenericoComponent.prototype.ngOnInit = function () {
    };
    FieldGenericoComponent.prototype.ngAfterViewInit = function () {
        var compFactory = this.cfr.resolveComponentFactory(this.getComponent(this.col.tipo));
        var ref = this.viewContainerRef.createComponent(compFactory);
        ref.instance.col = this.col;
        ref.instance.elemento = this.elemento;
        ref.instance.observador = this.observador;
    };
    FieldGenericoComponent.prototype.getComponent = function (prop) {
        switch (prop) {
            case 'texto':
                return TextoComponent;
                break;
            case 'boton':
                return BotonComponent;
                break;
            case 'boton-icono':
                return BotonIconoComponent;
                break;
            default:
                return TextoComponent;
                break;
        }
    };
    FieldGenericoComponent.ctorParameters = function () { return [
        { type: ComponentFactoryResolver }
    ]; };
    __decorate([
        Input()
    ], FieldGenericoComponent.prototype, "col", void 0);
    __decorate([
        Input()
    ], FieldGenericoComponent.prototype, "elemento", void 0);
    __decorate([
        Input()
    ], FieldGenericoComponent.prototype, "observador", void 0);
    __decorate([
        ViewChild('dynamic', { read: ViewContainerRef })
    ], FieldGenericoComponent.prototype, "viewContainerRef", void 0);
    FieldGenericoComponent = __decorate([
        Component({
            selector: 'app-field-generico',
            template: " <ng-template #dynamic></ng-template>\n",
            styles: [""]
        })
    ], FieldGenericoComponent);
    return FieldGenericoComponent;
}());
export { FieldGenericoComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGQtZ2VuZXJpY28uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaXZhaS1nZW5lcmljby1saWJyZXJpYS8iLCJzb3VyY2VzIjpbImxpYi9nZW5lcmljby9mb3JtbHlzL2ZpZWxkLWdlbmVyaWNvL2ZpZWxkLWdlbmVyaWNvLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLGdCQUFnQixFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsYUFBYSxFQUFFLHdCQUF3QixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRXJJLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUUxRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDMUQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFRM0U7SUFRRSxnQ0FBb0IsR0FBNkI7UUFBN0IsUUFBRyxHQUFILEdBQUcsQ0FBMEI7UUFQeEMsUUFBRyxHQUFRLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxDQUFDO0lBUWpDLENBQUM7SUFDRCx5Q0FBUSxHQUFSO0lBQ0EsQ0FBQztJQUVELGdEQUFlLEdBQWY7UUFDRSxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQ3JGLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDN0QsR0FBRyxDQUFDLFFBQVEsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQztRQUM1QixHQUFHLENBQUMsUUFBUSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQ3RDLEdBQUcsQ0FBQyxRQUFRLENBQUMsVUFBVSxHQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDMUMsQ0FBQztJQUVELDZDQUFZLEdBQVosVUFBYSxJQUFZO1FBQ3ZCLFFBQVEsSUFBSSxFQUFFO1lBQ1osS0FBSyxPQUFPO2dCQUNWLE9BQU8sY0FBYyxDQUFDO2dCQUN0QixNQUFNO1lBQ1IsS0FBSyxPQUFPO2dCQUNWLE9BQU8sY0FBYyxDQUFDO2dCQUN0QixNQUFNO1lBQ04sS0FBSyxhQUFhO2dCQUNsQixPQUFPLG1CQUFtQixDQUFDO2dCQUMzQixNQUFNO1lBQ1I7Z0JBQ0UsT0FBTyxjQUFjLENBQUM7Z0JBQ3RCLE1BQU07U0FDVDtJQUNILENBQUM7O2dCQTVCd0Isd0JBQXdCOztJQVB4QztRQUFSLEtBQUssRUFBRTt1REFBeUI7SUFDeEI7UUFBUixLQUFLLEVBQUU7NERBQWU7SUFDZDtRQUFSLEtBQUssRUFBRTs4REFBOEI7SUFHdEM7UUFEQyxTQUFTLENBQUMsU0FBUyxFQUFFLEVBQUMsSUFBSSxFQUFFLGdCQUFnQixFQUFDLENBQUM7b0VBQ1o7SUFOeEIsc0JBQXNCO1FBTGxDLFNBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxvQkFBb0I7WUFDOUIsbURBQThDOztTQUUvQyxDQUFDO09BQ1csc0JBQXNCLENBeUNsQztJQUFELDZCQUFDO0NBQUEsQUF6Q0QsSUF5Q0M7U0F6Q1ksc0JBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIFZpZXdDaGlsZCwgVmlld0NvbnRhaW5lclJlZiwgVHlwZSwgSW5wdXQsIEFmdGVyVmlld0luaXQsIENvbXBvbmVudEZhY3RvcnlSZXNvbHZlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBUZXh0b0NvbXBvbmVudCB9IGZyb20gJy4uL3RleHRvL3RleHRvLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBFbGVtZW50b1RhYmxhQ29tcG9uZW50IH0gZnJvbSAnLi4vZWxlbWVudG8tdGFibGEvZWxlbWVudG8tdGFibGEuY29tcG9uZW50JztcbmltcG9ydCB7IEJvdG9uQ29tcG9uZW50IH0gZnJvbSAnLi4vYm90b24vYm90b24uY29tcG9uZW50JztcbmltcG9ydCB7IEJvdG9uSWNvbm9Db21wb25lbnQgfSBmcm9tICcuLi9ib3Rvbi1pY29uby9ib3Rvbi1pY29uby5jb21wb25lbnQnO1xuaW1wb3J0IHsgQWNjaW9uZXNHZW5lcmFsZXMgfSBmcm9tICcuLi9hY2Npb25lcy9hY2Npb25lcy1nZW5lcmFsZXMnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhcHAtZmllbGQtZ2VuZXJpY28nLFxuICB0ZW1wbGF0ZVVybDogJy4vZmllbGQtZ2VuZXJpY28uY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9maWVsZC1nZW5lcmljby5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIEZpZWxkR2VuZXJpY29Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIEFmdGVyVmlld0luaXQge1xuICBASW5wdXQoKSBjb2w6IGFueSA9IHsgcHJvcDogJycgfTtcbiAgQElucHV0KCkgZWxlbWVudG86IGFueTtcbiAgQElucHV0KCkgb2JzZXJ2YWRvcjpBY2Npb25lc0dlbmVyYWxlcztcblxuICBAVmlld0NoaWxkKCdkeW5hbWljJywge3JlYWQ6IFZpZXdDb250YWluZXJSZWZ9KSBcbiAgdmlld0NvbnRhaW5lclJlZjogVmlld0NvbnRhaW5lclJlZjtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGNmcjogQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyKSB7XG4gIH1cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuICBuZ0FmdGVyVmlld0luaXQoKTogdm9pZCB7XG4gICAgbGV0IGNvbXBGYWN0b3J5ID0gdGhpcy5jZnIucmVzb2x2ZUNvbXBvbmVudEZhY3RvcnkodGhpcy5nZXRDb21wb25lbnQodGhpcy5jb2wudGlwbykpO1xuICAgIGxldCByZWYgPSB0aGlzLnZpZXdDb250YWluZXJSZWYuY3JlYXRlQ29tcG9uZW50KGNvbXBGYWN0b3J5KTtcbiAgICByZWYuaW5zdGFuY2UuY29sID0gdGhpcy5jb2w7XG4gICAgcmVmLmluc3RhbmNlLmVsZW1lbnRvID0gdGhpcy5lbGVtZW50bztcbiAgICByZWYuaW5zdGFuY2Uub2JzZXJ2YWRvcj10aGlzLm9ic2VydmFkb3I7XG4gIH1cblxuICBnZXRDb21wb25lbnQocHJvcDogc3RyaW5nKTogVHlwZTxFbGVtZW50b1RhYmxhQ29tcG9uZW50PiB7XG4gICAgc3dpdGNoIChwcm9wKSB7XG4gICAgICBjYXNlICd0ZXh0byc6XG4gICAgICAgIHJldHVybiBUZXh0b0NvbXBvbmVudDtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlICdib3Rvbic6XG4gICAgICAgIHJldHVybiBCb3RvbkNvbXBvbmVudDtcbiAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgJ2JvdG9uLWljb25vJzpcbiAgICAgICAgcmV0dXJuIEJvdG9uSWNvbm9Db21wb25lbnQ7XG4gICAgICAgIGJyZWFrO1xuICAgICAgZGVmYXVsdDpcbiAgICAgICAgcmV0dXJuIFRleHRvQ29tcG9uZW50O1xuICAgICAgICBicmVhaztcbiAgICB9XG4gIH1cblxuXG5cblxufVxuIl19