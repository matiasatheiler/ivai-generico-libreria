import { __decorate, __extends } from "tslib";
import { Component, ViewChild } from '@angular/core';
import { FieldType } from '@ngx-formly/material';
import { MatInput } from '@angular/material/input';
import { MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { startWith, switchMap } from 'rxjs/operators';
var AutocompleteTypeComponent = /** @class */ (function (_super) {
    __extends(AutocompleteTypeComponent, _super);
    function AutocompleteTypeComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AutocompleteTypeComponent.prototype.ngOnInit = function () {
        var _this = this;
        _super.prototype.ngOnInit.call(this);
        this.filter = this.formControl.valueChanges
            .pipe(startWith(''), switchMap(function (term) { return _this.to.filter(term); }));
    };
    AutocompleteTypeComponent.prototype.ngAfterViewInit = function () {
        _super.prototype.ngAfterViewInit.call(this);
        // temporary fix for https://github.com/angular/material2/issues/6728
        this.autocomplete._formField = this.formField;
    };
    AutocompleteTypeComponent.prototype.borrar = function () {
        this.value = "";
    };
    AutocompleteTypeComponent.prototype.seleccionar = function (value) {
    };
    __decorate([
        ViewChild(MatInput)
    ], AutocompleteTypeComponent.prototype, "formFieldControl", void 0);
    __decorate([
        ViewChild(MatAutocompleteTrigger)
    ], AutocompleteTypeComponent.prototype, "autocomplete", void 0);
    AutocompleteTypeComponent = __decorate([
        Component({
            selector: 'formly-autocomplete-type',
            template: "  <div class=\"row\">\n    \n    \n     <input class=\"col\" style=\"padding-left: 1rem;\" matInput \n      [matAutocomplete]=\"auto\"\n      [formControl]=\"formControl\"\n      [formlyAttributes]=\"field\"\n      [placeholder]=\"to.placeholder\"\n      [errorStateMatcher]=\"errorStateMatcher\"\n      >\n      <div class=\"col-1\" style=\"color: red;\">\n        <button  style=\"padding-left: 0px;\" class=\"btn btn-link \" *ngIf=\"value\" aria-label=\"Clear\" (click)=\"borrar()\">\n          <i class=\"fa fa-trash\"></i>\n         </button>\n      </div>\n      <mat-autocomplete  #auto=\"matAutocomplete\">\n      \n        <mat-option  (click)=\"seleccionar(value)\" *ngFor=\"let value of filter | async\" [value]=\"value\">\n          {{ value }}\n        </mat-option>      \n      </mat-autocomplete>\n  </div>\n  \n\n \n  \n  \n    \n"
        })
    ], AutocompleteTypeComponent);
    return AutocompleteTypeComponent;
}(FieldType));
export { AutocompleteTypeComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0b2NvbXBsZXRlLXR5cGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaXZhaS1nZW5lcmljby1saWJyZXJpYS8iLCJzb3VyY2VzIjpbImxpYi9nZW5lcmljby9mb3JtbHlzL2F1dG9jb21wbGV0ZS10eXBlL2F1dG9jb21wbGV0ZS10eXBlLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQXlCLE1BQU0sZUFBZSxDQUFDO0FBQzVFLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDbkQsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFFeEUsT0FBTyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQU10RDtJQUErQyw2Q0FBUztJQUF4RDs7SUFvQ0EsQ0FBQztJQTlCQyw0Q0FBUSxHQUFSO1FBQUEsaUJBT0M7UUFOQyxpQkFBTSxRQUFRLFdBQUUsQ0FBQztRQUNqQixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWTthQUN4QyxJQUFJLENBQ0gsU0FBUyxDQUFDLEVBQUUsQ0FBQyxFQUNiLFNBQVMsQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLEtBQUksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFwQixDQUFvQixDQUFDLENBQ3hDLENBQUM7SUFDTixDQUFDO0lBRUQsbURBQWUsR0FBZjtRQUNFLGlCQUFNLGVBQWUsV0FBRSxDQUFDO1FBQ3hCLHFFQUFxRTtRQUM5RCxJQUFJLENBQUMsWUFBYSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBRXhELENBQUM7SUFJQSwwQ0FBTSxHQUFOO1FBRUUsSUFBSSxDQUFDLEtBQUssR0FBQyxFQUFFLENBQUM7SUFFaEIsQ0FBQztJQUVELCtDQUFXLEdBQVgsVUFBWSxLQUFLO0lBRWpCLENBQUM7SUEvQm1CO1FBQXBCLFNBQVMsQ0FBQyxRQUFRLENBQUM7dUVBQTRCO0lBQ2I7UUFBbEMsU0FBUyxDQUFDLHNCQUFzQixDQUFDO21FQUFzQztJQUY3RCx5QkFBeUI7UUFKckMsU0FBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLDBCQUEwQjtZQUNwQywyMUJBQThDO1NBQy9DLENBQUM7T0FDVyx5QkFBeUIsQ0FvQ3JDO0lBQUQsZ0NBQUM7Q0FBQSxBQXBDRCxDQUErQyxTQUFTLEdBb0N2RDtTQXBDWSx5QkFBeUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIFZpZXdDaGlsZCwgT25Jbml0LCBBZnRlclZpZXdJbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGaWVsZFR5cGUgfSBmcm9tICdAbmd4LWZvcm1seS9tYXRlcmlhbCc7XG5pbXBvcnQgeyBNYXRJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2lucHV0JztcbmltcG9ydCB7IE1hdEF1dG9jb21wbGV0ZVRyaWdnZXIgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9hdXRvY29tcGxldGUnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgc3RhcnRXaXRoLCBzd2l0Y2hNYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2Zvcm1seS1hdXRvY29tcGxldGUtdHlwZScsXG4gIHRlbXBsYXRlVXJsOidhdXRvY29tcGxldGUtdHlwZS5jb21wb25lbnQuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgQXV0b2NvbXBsZXRlVHlwZUNvbXBvbmVudCBleHRlbmRzIEZpZWxkVHlwZSBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCB7XG4gIEBWaWV3Q2hpbGQoTWF0SW5wdXQpIGZvcm1GaWVsZENvbnRyb2w6IE1hdElucHV0O1xuICBAVmlld0NoaWxkKE1hdEF1dG9jb21wbGV0ZVRyaWdnZXIpIGF1dG9jb21wbGV0ZTogTWF0QXV0b2NvbXBsZXRlVHJpZ2dlcjtcblxuICBmaWx0ZXI6IE9ic2VydmFibGU8YW55PjtcbiAgXG4gIG5nT25Jbml0KCkge1xuICAgIHN1cGVyLm5nT25Jbml0KCk7XG4gICAgdGhpcy5maWx0ZXIgPSB0aGlzLmZvcm1Db250cm9sLnZhbHVlQ2hhbmdlc1xuICAgICAgLnBpcGUoXG4gICAgICAgIHN0YXJ0V2l0aCgnJyksXG4gICAgICAgIHN3aXRjaE1hcCh0ZXJtID0+IHRoaXMudG8uZmlsdGVyKHRlcm0pKSxcbiAgICAgICk7XG4gIH1cblxuICBuZ0FmdGVyVmlld0luaXQoKSB7XG4gICAgc3VwZXIubmdBZnRlclZpZXdJbml0KCk7XG4gICAgLy8gdGVtcG9yYXJ5IGZpeCBmb3IgaHR0cHM6Ly9naXRodWIuY29tL2FuZ3VsYXIvbWF0ZXJpYWwyL2lzc3Vlcy82NzI4XG4gICAgKDxhbnk+IHRoaXMuYXV0b2NvbXBsZXRlKS5fZm9ybUZpZWxkID0gdGhpcy5mb3JtRmllbGQ7XG4gICAgXG4gIH1cblxuICBcblxuICAgYm9ycmFyKCl7XG4gICAgXG4gICAgIHRoaXMudmFsdWU9XCJcIjtcbiAgICAgXG4gICB9XG5cbiAgIHNlbGVjY2lvbmFyKHZhbHVlKXtcbiAgICAgXG4gICB9XG5cbiAgIFxuICBcbn1cbiJdfQ==