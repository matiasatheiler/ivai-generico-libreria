import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
var AccionesComponent = /** @class */ (function () {
    function AccionesComponent() {
    }
    AccionesComponent.prototype.ngOnInit = function () {
    };
    AccionesComponent.prototype.nuevo = function () {
        this.componente.nuevo();
    };
    __decorate([
        Input()
    ], AccionesComponent.prototype, "componente", void 0);
    AccionesComponent = __decorate([
        Component({
            selector: 'app-acciones',
            template: "<div class=\"input-group mb-3\">  \n  <button  (click)=\"nuevo()\" class=\"btn btn-outline-success\" type=\"button\">\n    <i class=\"fa fa-plus-circle\"></i>\n    <span> Nuevo</span>\n  </button>\n</div>\n",
            styles: [""]
        })
    ], AccionesComponent);
    return AccionesComponent;
}());
export { AccionesComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjaW9uZXMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaXZhaS1nZW5lcmljby1saWJyZXJpYS8iLCJzb3VyY2VzIjpbImxpYi9nZW5lcmljby9mb3JtbHlzL2FjY2lvbmVzL2FjY2lvbmVzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFRekQ7SUFHRTtJQUFnQixDQUFDO0lBRWpCLG9DQUFRLEdBQVI7SUFDQSxDQUFDO0lBRUQsaUNBQUssR0FBTDtRQUNFLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDMUIsQ0FBQztJQVJEO1FBREMsS0FBSyxFQUFFO3lEQUNxQjtJQUZsQixpQkFBaUI7UUFMN0IsU0FBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGNBQWM7WUFDeEIsME5BQXdDOztTQUV6QyxDQUFDO09BQ1csaUJBQWlCLENBVzdCO0lBQUQsd0JBQUM7Q0FBQSxBQVhELElBV0M7U0FYWSxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEFjY2lvbmVzR2VuZXJhbGVzIH0gZnJvbSAnLi9hY2Npb25lcy1nZW5lcmFsZXMnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhcHAtYWNjaW9uZXMnLFxuICB0ZW1wbGF0ZVVybDogJy4vYWNjaW9uZXMuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9hY2Npb25lcy5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIEFjY2lvbmVzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQElucHV0KClcbiAgY29tcG9uZW50ZTpBY2Npb25lc0dlbmVyYWxlczsgXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuICBudWV2bygpe1xuICAgIHRoaXMuY29tcG9uZW50ZS5udWV2bygpO1xuICB9XG59XG4iXX0=