import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LogisoftStorage } from '../../storage/logisoft-storage';
var PanelBotonAtrasComponent = /** @class */ (function () {
    function PanelBotonAtrasComponent(router) {
        this.router = router;
    }
    PanelBotonAtrasComponent.prototype.ngOnInit = function () {
    };
    PanelBotonAtrasComponent.prototype.existePantallaAtras = function () {
        return LogisoftStorage.isAtras();
    };
    PanelBotonAtrasComponent.prototype.atras = function () {
        this.router.navigate([LogisoftStorage.getPantallaAtras().url]);
    };
    PanelBotonAtrasComponent.ctorParameters = function () { return [
        { type: Router }
    ]; };
    PanelBotonAtrasComponent = __decorate([
        Component({
            selector: 'app-panel-boton-atras',
            template: "<div class=\"row mb-4 ml-0\">\n \n    <button type=\"button\" class=\"btn btn-info\" *ngIf=\"existePantallaAtras()\" (click)=\"atras()\"><i class=\"fa fa-chevron-circle-left\"></i> Atr\u00E1s</button>\n \n</div>\n",
            styles: [""]
        })
    ], PanelBotonAtrasComponent);
    return PanelBotonAtrasComponent;
}());
export { PanelBotonAtrasComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFuZWwtYm90b24tYXRyYXMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaXZhaS1nZW5lcmljby1saWJyZXJpYS8iLCJzb3VyY2VzIjpbImxpYi9nZW5lcmljby9ib3Rvbi1hdHJhcy9wYW5lbC1ib3Rvbi1hdHJhcy9wYW5lbC1ib3Rvbi1hdHJhcy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFFbEQsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ3pDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQU9qRTtJQUVFLGtDQUFvQixNQUFhO1FBQWIsV0FBTSxHQUFOLE1BQU0sQ0FBTztJQUFJLENBQUM7SUFFdEMsMkNBQVEsR0FBUjtJQUNBLENBQUM7SUFFRCxzREFBbUIsR0FBbkI7UUFDRSxPQUFPLGVBQWUsQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNuQyxDQUFDO0lBRUQsd0NBQUssR0FBTDtRQUNFLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsZUFBZSxDQUFDLGdCQUFnQixFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztJQUNqRSxDQUFDOztnQkFYMEIsTUFBTTs7SUFGdEIsd0JBQXdCO1FBTHBDLFNBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSx1QkFBdUI7WUFDakMsaU9BQWlEOztTQUVsRCxDQUFDO09BQ1csd0JBQXdCLENBZXBDO0lBQUQsK0JBQUM7Q0FBQSxBQWZELElBZUM7U0FmWSx3QkFBd0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgTG9naXNvZnRTdG9yYWdlIH0gZnJvbSAnLi4vLi4vc3RvcmFnZS9sb2dpc29mdC1zdG9yYWdlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYXBwLXBhbmVsLWJvdG9uLWF0cmFzJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3BhbmVsLWJvdG9uLWF0cmFzLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vcGFuZWwtYm90b24tYXRyYXMuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBQYW5lbEJvdG9uQXRyYXNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyOlJvdXRlcikgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuICBleGlzdGVQYW50YWxsYUF0cmFzKCl7XG4gICAgcmV0dXJuIExvZ2lzb2Z0U3RvcmFnZS5pc0F0cmFzKCk7XG4gIH1cblxuICBhdHJhcygpe1xuICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFtMb2dpc29mdFN0b3JhZ2UuZ2V0UGFudGFsbGFBdHJhcygpLnVybF0pO1xuICB9XG5cbn1cbiJdfQ==