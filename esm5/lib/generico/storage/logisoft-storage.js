var LogisoftStorage = /** @class */ (function () {
    function LogisoftStorage() {
    }
    LogisoftStorage.borrarPantallaAtras = function () {
        localStorage.removeItem('atras');
    };
    LogisoftStorage.getPantallaAtras = function () {
        return JSON.parse(localStorage.getItem('atras'));
    };
    LogisoftStorage.isAtras = function () {
        if (localStorage.getItem('atras')) {
            return true;
        }
        return false;
    };
    LogisoftStorage.setPantallaAtras = function (pantalla) {
        localStorage.setItem('atras', JSON.stringify(pantalla));
    };
    LogisoftStorage.isUsuario = function () {
        if (localStorage.getItem('currentUser')) {
            return true;
        }
        return false;
    };
    LogisoftStorage.cerrarSesion = function () {
        localStorage.removeItem('currentUser');
    };
    LogisoftStorage.getNombreUsuario = function () {
        if (!localStorage.getItem('currentUser')) {
            return '';
        }
        return JSON.parse(localStorage.getItem('currentUser')).nombre;
    };
    return LogisoftStorage;
}());
export { LogisoftStorage };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naXNvZnQtc3RvcmFnZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2l2YWktZ2VuZXJpY28tbGlicmVyaWEvIiwic291cmNlcyI6WyJsaWIvZ2VuZXJpY28vc3RvcmFnZS9sb2dpc29mdC1zdG9yYWdlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0lBQUE7SUFrQ0EsQ0FBQztJQWpDUSxtQ0FBbUIsR0FBMUI7UUFDRSxZQUFZLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFFYSxnQ0FBZ0IsR0FBOUI7UUFDQyxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFFYSx1QkFBTyxHQUFyQjtRQUNFLElBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsRUFBQztZQUMvQixPQUFPLElBQUksQ0FBQztTQUNiO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDO0lBRWEsZ0NBQWdCLEdBQTlCLFVBQStCLFFBQTZCO1FBQzFELFlBQVksQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztJQUN6RCxDQUFDO0lBRWEseUJBQVMsR0FBdkI7UUFDRSxJQUFJLFlBQVksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLEVBQUU7WUFDckMsT0FBTyxJQUFJLENBQUM7U0FDZjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQztJQUNjLDRCQUFZLEdBQTFCO1FBQ0ssWUFBWSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRWEsZ0NBQWdCLEdBQTlCO1FBQ0ksSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLEVBQUU7WUFBQyxPQUFPLEVBQUUsQ0FBQztTQUFFO1FBQ3ZELE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDO0lBQ2xFLENBQUM7SUFDTCxzQkFBQztBQUFELENBQUMsQUFsQ0QsSUFrQ0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQYW50YWxsYUF0cmFzU3RvcmFnZSB9IGZyb20gJy4uL2JvdG9uLWF0cmFzL3BhbnRhbGxhLWF0cmFzLXN0b3JhZ2UnO1xuXG5leHBvcnQgY2xhc3MgTG9naXNvZnRTdG9yYWdlIHtcbiAgc3RhdGljIGJvcnJhclBhbnRhbGxhQXRyYXMoKSB7XG4gICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oJ2F0cmFzJyk7XG4gIH1cbiAgXG4gIHB1YmxpYyBzdGF0aWMgZ2V0UGFudGFsbGFBdHJhcygpOlBhbnRhbGxhQXRyYXNTdG9yYWdlIHtcbiAgIHJldHVybiBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdhdHJhcycpKTtcbiAgfVxuICBcbiAgcHVibGljIHN0YXRpYyBpc0F0cmFzKCkge1xuICAgIGlmKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdhdHJhcycpKXtcbiAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxuICBwdWJsaWMgc3RhdGljIHNldFBhbnRhbGxhQXRyYXMocGFudGFsbGE6UGFudGFsbGFBdHJhc1N0b3JhZ2Upe1xuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdhdHJhcycsSlNPTi5zdHJpbmdpZnkocGFudGFsbGEpKTtcbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgaXNVc3VhcmlvKCkge1xuICAgIGlmIChsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnY3VycmVudFVzZXInKSkge1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG4gICBwdWJsaWMgc3RhdGljIGNlcnJhclNlc2lvbigpIHtcbiAgICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oJ2N1cnJlbnRVc2VyJyk7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBnZXROb21icmVVc3VhcmlvKCk6IHN0cmluZyB7XG4gICAgICAgIGlmICghbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2N1cnJlbnRVc2VyJykpIHtyZXR1cm4gJyc7IH1cbiAgICAgICAgcmV0dXJuIEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2N1cnJlbnRVc2VyJykpLm5vbWJyZTtcbiAgICB9XG59XG4iXX0=