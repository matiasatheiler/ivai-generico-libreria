import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
var GenericoService = /** @class */ (function () {
    function GenericoService(http) {
        this.http = http;
        this.size = 10;
        this.urlObjeto = '';
        this.json = {};
    }
    GenericoService.prototype.get = function (id) {
        return this.http.get(this.getUrl() + "/" + id);
    };
    GenericoService.prototype.getAll = function () {
        return this.http.get(this.getUrl());
    };
    GenericoService.prototype.getPagina = function () {
        return this.getPaginaServidor(0, this.size);
    };
    GenericoService.prototype.getPaginaServidor = function (pageIndex, pageSize) {
        var params = new HttpParams()
            .set('page', pageIndex.toString())
            .set('pagination', 'true')
            .set('size', pageSize.toString())
            .set('sort', 'id,asc');
        return this.http.get(this.getUrl(), { params: params });
    };
    GenericoService.prototype.getPaginaServidorBusqueda = function (pageIndex, pageSize, parametros) {
        var params = new HttpParams()
            .set('page', pageIndex.toString())
            .set('pagination', 'true')
            .set('size', pageSize.toString())
            .set('sort', 'id,asc');
        for (var i = 0; i < parametros.length; i++) {
            var p = parametros[i];
            params = params.append(p[0], p[1]);
        }
        return this.http.get(this.getUrl(), { params: params });
    };
    GenericoService.prototype.getUrl = function () {
        return this.url + this.urlObjeto;
    };
    GenericoService.prototype.getCampos = function () {
        return this.json;
    };
    GenericoService.prototype.nuevo = function (objeto) {
        return this.http.post(this.getUrl(), objeto);
    };
    GenericoService.prototype.delete = function (id) {
        return this.http.delete(this.getUrl() + "/" + id);
    };
    GenericoService.ctorParameters = function () { return [
        { type: HttpClient }
    ]; };
    GenericoService.ɵprov = i0.ɵɵdefineInjectable({ factory: function GenericoService_Factory() { return new GenericoService(i0.ɵɵinject(i1.HttpClient)); }, token: GenericoService, providedIn: "root" });
    GenericoService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], GenericoService);
    return GenericoService;
}());
export { GenericoService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2VuZXJpY28uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2l2YWktZ2VuZXJpY28tbGlicmVyaWEvIiwic291cmNlcyI6WyJsaWIvZ2VuZXJpY28vc2VydmljaW9zL2dlbmVyaWNvLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQzs7O0FBVzlEO0lBUUUseUJBQW1CLElBQWdCO1FBQWhCLFNBQUksR0FBSixJQUFJLENBQVk7UUFMbkMsU0FBSSxHQUFHLEVBQUUsQ0FBQztRQUVWLGNBQVMsR0FBRyxFQUFFLENBQUM7UUFDZixTQUFJLEdBQUssRUFBRSxDQUFDO0lBRTJCLENBQUM7SUFFeEMsNkJBQUcsR0FBSCxVQUFJLEVBQUU7UUFDSixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBQyxHQUFHLEdBQUMsRUFBRSxDQUFDLENBQUM7SUFDN0MsQ0FBQztJQUVELGdDQUFNLEdBQU47UUFDRSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO0lBQ3RDLENBQUM7SUFFRCxtQ0FBUyxHQUFUO1FBQ0MsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBRUQsMkNBQWlCLEdBQWpCLFVBQWtCLFNBQWlCLEVBQUUsUUFBZ0I7UUFDbkQsSUFBTSxNQUFNLEdBQUcsSUFBSSxVQUFVLEVBQUU7YUFDaEMsR0FBRyxDQUFDLE1BQU0sRUFBRSxTQUFTLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDakMsR0FBRyxDQUFDLFlBQVksRUFBRSxNQUFNLENBQUM7YUFDekIsR0FBRyxDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUU7YUFDakMsR0FBRyxDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUMsQ0FBQztRQUNyQixPQUErQixJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEVBQUUsRUFBQyxNQUFNLFFBQUEsRUFBQyxDQUFDLENBQUM7SUFDeEUsQ0FBQztJQUVELG1EQUF5QixHQUF6QixVQUEwQixTQUFpQixFQUFFLFFBQWdCLEVBQUUsVUFBVTtRQUN2RSxJQUFJLE1BQU0sR0FBRyxJQUFJLFVBQVUsRUFBRTthQUM5QixHQUFHLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUNqQyxHQUFHLENBQUMsWUFBWSxFQUFFLE1BQU0sQ0FBQzthQUN6QixHQUFHLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBRTthQUNqQyxHQUFHLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQ3ZCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxVQUFVLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQzFDLElBQU0sQ0FBQyxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN4QixNQUFNLEdBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDakM7UUFDQyxPQUErQixJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEVBQUUsRUFBQyxNQUFNLFFBQUEsRUFBQyxDQUFDLENBQUM7SUFDeEUsQ0FBQztJQUNELGdDQUFNLEdBQU47UUFDRSxPQUFPLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUNuQyxDQUFDO0lBRUQsbUNBQVMsR0FBVDtRQUNFLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQztJQUNuQixDQUFDO0lBRUQsK0JBQUssR0FBTCxVQUFNLE1BQVE7UUFDWixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsRUFBQyxNQUFNLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBRUQsZ0NBQU0sR0FBTixVQUFPLEVBQU87UUFDWixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBQyxHQUFHLEdBQUMsRUFBRSxDQUFDLENBQUM7SUFDaEQsQ0FBQzs7Z0JBakR3QixVQUFVOzs7SUFSeEIsZUFBZTtRQUgzQixVQUFVLENBQUM7WUFDVixVQUFVLEVBQUUsTUFBTTtTQUNuQixDQUFDO09BQ1csZUFBZSxDQTJEM0I7MEJBdkVEO0NBdUVDLEFBM0RELElBMkRDO1NBM0RZLGVBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwUGFyYW1zIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgQ29uZmlndXJhY2lvbiB9IGZyb20gJy4uL2NvbmZpZ3VyYWNpb24nO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuXG5pbXBvcnQgeyBTZXJ2aWNpbyB9IGZyb20gJy4vc2VydmljaW8nO1xuaW1wb3J0IHsgUGFnaW5hIH0gZnJvbSAnLi4vcGFnaW5hY2lvbi9wYWdpbmEnO1xuXG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIEdlbmVyaWNvU2VydmljZTxUPiBpbXBsZW1lbnRzIFNlcnZpY2lvPFQ+IHtcbiAgXG4gXG4gIHNpemUgPSAxMDtcbiAgdXJsOiBzdHJpbmc7XG4gIHVybE9iamV0byA9ICcnO1xuICBqc29uOmFueT17fTtcblxuICBjb25zdHJ1Y3RvcihwdWJsaWMgaHR0cDogSHR0cENsaWVudCkgeyB9XG5cbiAgZ2V0KGlkKTpPYnNlcnZhYmxlPGFueT57XG4gICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQodGhpcy5nZXRVcmwoKStcIi9cIitpZCk7XG4gIH1cblxuICBnZXRBbGwoKXtcbiAgICByZXR1cm4gdGhpcy5odHRwLmdldCh0aGlzLmdldFVybCgpKTtcbiAgfVxuXG4gIGdldFBhZ2luYSgpOiBPYnNlcnZhYmxlPFBhZ2luYTxUPj4ge1xuICAgcmV0dXJuIHRoaXMuZ2V0UGFnaW5hU2Vydmlkb3IoMCx0aGlzLnNpemUpO1xuICB9XG5cbiAgZ2V0UGFnaW5hU2Vydmlkb3IocGFnZUluZGV4OiBudW1iZXIsIHBhZ2VTaXplOiBudW1iZXIpOiBPYnNlcnZhYmxlPFBhZ2luYTxUPj4ge1xuICAgIGNvbnN0IHBhcmFtcyA9IG5ldyBIdHRwUGFyYW1zKClcbiAgLnNldCgncGFnZScsIHBhZ2VJbmRleC50b1N0cmluZygpKVxuICAuc2V0KCdwYWdpbmF0aW9uJywgJ3RydWUnKVxuICAuc2V0KCdzaXplJywgcGFnZVNpemUudG9TdHJpbmcoKSApXG4gIC5zZXQoJ3NvcnQnLCAnaWQsYXNjJyk7XG4gICAgcmV0dXJuIDxPYnNlcnZhYmxlPFBhZ2luYTxUPj4+IHRoaXMuaHR0cC5nZXQodGhpcy5nZXRVcmwoKSwge3BhcmFtc30pO1xuICB9XG5cbiAgZ2V0UGFnaW5hU2Vydmlkb3JCdXNxdWVkYShwYWdlSW5kZXg6IG51bWJlciwgcGFnZVNpemU6IG51bWJlciwgcGFyYW1ldHJvcykge1xuICAgIGxldCBwYXJhbXMgPSBuZXcgSHR0cFBhcmFtcygpXG4gIC5zZXQoJ3BhZ2UnLCBwYWdlSW5kZXgudG9TdHJpbmcoKSlcbiAgLnNldCgncGFnaW5hdGlvbicsICd0cnVlJylcbiAgLnNldCgnc2l6ZScsIHBhZ2VTaXplLnRvU3RyaW5nKCkgKVxuICAuc2V0KCdzb3J0JywgJ2lkLGFzYycpO1xuICBmb3IgKGxldCBpID0gMDsgaSA8IHBhcmFtZXRyb3MubGVuZ3RoOyBpKyspIHtcbiAgICBjb25zdCBwID0gcGFyYW1ldHJvc1tpXTtcbiAgICBwYXJhbXM9cGFyYW1zLmFwcGVuZChwWzBdLHBbMV0pOyAgICBcbiAgfVxuICAgIHJldHVybiA8T2JzZXJ2YWJsZTxQYWdpbmE8VD4+PiB0aGlzLmh0dHAuZ2V0KHRoaXMuZ2V0VXJsKCksIHtwYXJhbXN9KTtcbiAgfVxuICBnZXRVcmwoKTogc3RyaW5nIHtcbiAgICByZXR1cm4gdGhpcy51cmwgKyB0aGlzLnVybE9iamV0bztcbiAgfVxuXG4gIGdldENhbXBvcygpOmFueXtcbiAgICByZXR1cm4gdGhpcy5qc29uO1xuICB9XG5cbiAgbnVldm8ob2JqZXRvOlQpe1xuICAgIHJldHVybiB0aGlzLmh0dHAucG9zdCh0aGlzLmdldFVybCgpLG9iamV0byk7XG4gIH1cblxuICBkZWxldGUoaWQ6IGFueSkge1xuICAgIHJldHVybiB0aGlzLmh0dHAuZGVsZXRlKHRoaXMuZ2V0VXJsKCkrXCIvXCIraWQpO1xuICB9XG5cbn1cbiJdfQ==