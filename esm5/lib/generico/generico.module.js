import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListaComponent } from './lista/lista.component';
import { TablaComponent } from './tabla/tabla.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';
import { TextoComponent } from './formlys/texto/texto.component';
import { FieldGenericoComponent } from './formlys/field-generico/field-generico.component';
import { ElementoTablaComponent } from './formlys/elemento-tabla/elemento-tabla.component';
import { BotonComponent } from './formlys/boton/boton.component';
import { BusquedaComponent } from './formlys/busqueda/busqueda.component';
import { ModalTablaComponent } from './tabla/modal-tabla/modal-tabla.component';
import { AccionesComponent } from './formlys/acciones/acciones.component';
import { BotonIconoComponent } from './formlys/boton-icono/boton-icono.component';
import { EditarComponent } from './editar/editar.component';
import { MostrarTextoComponent } from './formlys/mostrar-texto/mostrar-texto.component';
import { MaterialModule } from '../material/material.module';
import { AutocompleteTypeComponent } from './formlys/autocomplete-type/autocomplete-type.component';
import { PanelBotonAtrasComponent } from './boton-atras/panel-boton-atras/panel-boton-atras.component';
var GenericoModule = /** @class */ (function () {
    function GenericoModule() {
    }
    GenericoModule = __decorate([
        NgModule({
            declarations: [ListaComponent, TablaComponent, TextoComponent, FieldGenericoComponent, ElementoTablaComponent, BotonComponent, BusquedaComponent, ModalTablaComponent, AccionesComponent, BotonIconoComponent, EditarComponent, MostrarTextoComponent, AutocompleteTypeComponent, PanelBotonAtrasComponent],
            imports: [
                CommonModule,
                ReactiveFormsModule,
                FormlyModule.forRoot({ types: [{
                            name: 'autocomplete',
                            component: AutocompleteTypeComponent,
                            wrappers: ['form-field'],
                        }],
                    validationMessages: [
                        { name: 'required', message: 'Campo requerido.' },
                    ] }),
                MaterialModule
            ],
            exports: [FormlyModule, TablaComponent, TextoComponent, FieldGenericoComponent, BotonComponent, BusquedaComponent, ModalTablaComponent, AccionesComponent, MaterialModule, PanelBotonAtrasComponent, MaterialModule]
        })
    ], GenericoModule);
    return GenericoModule;
}());
export { GenericoModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2VuZXJpY28ubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vaXZhaS1nZW5lcmljby1saWJyZXJpYS8iLCJzb3VyY2VzIjpbImxpYi9nZW5lcmljby9nZW5lcmljby5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUN6RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDekQsT0FBTyxFQUFlLG1CQUFtQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDbEUsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLGtCQUFrQixDQUFDO0FBRTlDLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUNqRSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSxtREFBbUQsQ0FBQztBQUMzRixPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSxtREFBbUQsQ0FBQztBQUMzRixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFDakUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDMUUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sMkNBQTJDLENBQUM7QUFDaEYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDMUUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFDbEYsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQzVELE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGlEQUFpRCxDQUFDO0FBQ3hGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUM3RCxPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSx5REFBeUQsQ0FBQztBQUNwRyxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSw2REFBNkQsQ0FBQztBQW1Cdkc7SUFBQTtJQUE4QixDQUFDO0lBQWxCLGNBQWM7UUFsQjFCLFFBQVEsQ0FBQztZQUNSLFlBQVksRUFBRSxDQUFDLGNBQWMsRUFBRSxjQUFjLEVBQUUsY0FBYyxFQUFFLHNCQUFzQixFQUFFLHNCQUFzQixFQUFFLGNBQWMsRUFBRSxpQkFBaUIsRUFBRSxtQkFBbUIsRUFBRSxpQkFBaUIsRUFBRSxtQkFBbUIsRUFBRSxlQUFlLEVBQUUscUJBQXFCLEVBQUUseUJBQXlCLEVBQUUsd0JBQXdCLENBQUM7WUFDM1MsT0FBTyxFQUFFO2dCQUNQLFlBQVk7Z0JBRVosbUJBQW1CO2dCQUNuQixZQUFZLENBQUMsT0FBTyxDQUFDLEVBQUMsS0FBSyxFQUFFLENBQUM7NEJBQzVCLElBQUksRUFBRSxjQUFjOzRCQUNwQixTQUFTLEVBQUUseUJBQXlCOzRCQUNwQyxRQUFRLEVBQUUsQ0FBQyxZQUFZLENBQUM7eUJBQ3pCLENBQUM7b0JBQ0Ysa0JBQWtCLEVBQUU7d0JBQ2xCLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUU7cUJBQ2xELEVBQUMsQ0FBQztnQkFDSCxjQUFjO2FBQ2Y7WUFDRCxPQUFPLEVBQUUsQ0FBQyxZQUFZLEVBQUMsY0FBYyxFQUFFLGNBQWMsRUFBRSxzQkFBc0IsRUFBRSxjQUFjLEVBQUUsaUJBQWlCLEVBQUUsbUJBQW1CLEVBQUUsaUJBQWlCLEVBQUMsY0FBYyxFQUFDLHdCQUF3QixFQUFDLGNBQWMsQ0FBQztTQUNqTixDQUFDO09BQ1csY0FBYyxDQUFJO0lBQUQscUJBQUM7Q0FBQSxBQUEvQixJQUErQjtTQUFsQixjQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBMaXN0YUNvbXBvbmVudCB9IGZyb20gJy4vbGlzdGEvbGlzdGEuY29tcG9uZW50JztcbmltcG9ydCB7IFRhYmxhQ29tcG9uZW50IH0gZnJvbSAnLi90YWJsYS90YWJsYS5jb21wb25lbnQnO1xuaW1wb3J0IHsgRm9ybXNNb2R1bGUsIFJlYWN0aXZlRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQge0Zvcm1seU1vZHVsZX0gZnJvbSAnQG5neC1mb3JtbHkvY29yZSc7XG5cbmltcG9ydCB7IFRleHRvQ29tcG9uZW50IH0gZnJvbSAnLi9mb3JtbHlzL3RleHRvL3RleHRvLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBGaWVsZEdlbmVyaWNvQ29tcG9uZW50IH0gZnJvbSAnLi9mb3JtbHlzL2ZpZWxkLWdlbmVyaWNvL2ZpZWxkLWdlbmVyaWNvLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBFbGVtZW50b1RhYmxhQ29tcG9uZW50IH0gZnJvbSAnLi9mb3JtbHlzL2VsZW1lbnRvLXRhYmxhL2VsZW1lbnRvLXRhYmxhLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBCb3RvbkNvbXBvbmVudCB9IGZyb20gJy4vZm9ybWx5cy9ib3Rvbi9ib3Rvbi5jb21wb25lbnQnO1xuaW1wb3J0IHsgQnVzcXVlZGFDb21wb25lbnQgfSBmcm9tICcuL2Zvcm1seXMvYnVzcXVlZGEvYnVzcXVlZGEuY29tcG9uZW50JztcbmltcG9ydCB7IE1vZGFsVGFibGFDb21wb25lbnQgfSBmcm9tICcuL3RhYmxhL21vZGFsLXRhYmxhL21vZGFsLXRhYmxhLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBBY2Npb25lc0NvbXBvbmVudCB9IGZyb20gJy4vZm9ybWx5cy9hY2Npb25lcy9hY2Npb25lcy5jb21wb25lbnQnO1xuaW1wb3J0IHsgQm90b25JY29ub0NvbXBvbmVudCB9IGZyb20gJy4vZm9ybWx5cy9ib3Rvbi1pY29uby9ib3Rvbi1pY29uby5jb21wb25lbnQnO1xuaW1wb3J0IHsgRWRpdGFyQ29tcG9uZW50IH0gZnJvbSAnLi9lZGl0YXIvZWRpdGFyLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBNb3N0cmFyVGV4dG9Db21wb25lbnQgfSBmcm9tICcuL2Zvcm1seXMvbW9zdHJhci10ZXh0by9tb3N0cmFyLXRleHRvLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBNYXRlcmlhbE1vZHVsZSB9IGZyb20gJy4uL21hdGVyaWFsL21hdGVyaWFsLm1vZHVsZSc7XG5pbXBvcnQgeyBBdXRvY29tcGxldGVUeXBlQ29tcG9uZW50IH0gZnJvbSAnLi9mb3JtbHlzL2F1dG9jb21wbGV0ZS10eXBlL2F1dG9jb21wbGV0ZS10eXBlLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBQYW5lbEJvdG9uQXRyYXNDb21wb25lbnQgfSBmcm9tICcuL2JvdG9uLWF0cmFzL3BhbmVsLWJvdG9uLWF0cmFzL3BhbmVsLWJvdG9uLWF0cmFzLmNvbXBvbmVudCc7XG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtMaXN0YUNvbXBvbmVudCwgVGFibGFDb21wb25lbnQsIFRleHRvQ29tcG9uZW50LCBGaWVsZEdlbmVyaWNvQ29tcG9uZW50LCBFbGVtZW50b1RhYmxhQ29tcG9uZW50LCBCb3RvbkNvbXBvbmVudCwgQnVzcXVlZGFDb21wb25lbnQsIE1vZGFsVGFibGFDb21wb25lbnQsIEFjY2lvbmVzQ29tcG9uZW50LCBCb3Rvbkljb25vQ29tcG9uZW50LCBFZGl0YXJDb21wb25lbnQsIE1vc3RyYXJUZXh0b0NvbXBvbmVudCwgQXV0b2NvbXBsZXRlVHlwZUNvbXBvbmVudCwgUGFuZWxCb3RvbkF0cmFzQ29tcG9uZW50XSxcbiAgaW1wb3J0czogW1xuICAgIENvbW1vbk1vZHVsZSxcbiAgICBcbiAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlLFxuICAgIEZvcm1seU1vZHVsZS5mb3JSb290KHt0eXBlczogW3tcbiAgICAgIG5hbWU6ICdhdXRvY29tcGxldGUnLFxuICAgICAgY29tcG9uZW50OiBBdXRvY29tcGxldGVUeXBlQ29tcG9uZW50LFxuICAgICAgd3JhcHBlcnM6IFsnZm9ybS1maWVsZCddLFxuICAgIH1dLFxuICAgIHZhbGlkYXRpb25NZXNzYWdlczogW1xuICAgICAgeyBuYW1lOiAncmVxdWlyZWQnLCBtZXNzYWdlOiAnQ2FtcG8gcmVxdWVyaWRvLicgfSxcbiAgICBdfSksXG4gICAgTWF0ZXJpYWxNb2R1bGUgIFxuICBdLFxuICBleHBvcnRzOiBbRm9ybWx5TW9kdWxlLFRhYmxhQ29tcG9uZW50LCBUZXh0b0NvbXBvbmVudCwgRmllbGRHZW5lcmljb0NvbXBvbmVudCwgQm90b25Db21wb25lbnQsIEJ1c3F1ZWRhQ29tcG9uZW50LCBNb2RhbFRhYmxhQ29tcG9uZW50LCBBY2Npb25lc0NvbXBvbmVudCxNYXRlcmlhbE1vZHVsZSxQYW5lbEJvdG9uQXRyYXNDb21wb25lbnQsTWF0ZXJpYWxNb2R1bGVdXG59KVxuZXhwb3J0IGNsYXNzIEdlbmVyaWNvTW9kdWxlIHsgfVxuIl19