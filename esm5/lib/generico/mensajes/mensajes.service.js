import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import * as i0 from "@angular/core";
export var TipoMensaje;
(function (TipoMensaje) {
    TipoMensaje["EXITO"] = "EXITO";
    TipoMensaje["ERROR"] = "ERROR";
    TipoMensaje["ELIMINAR"] = "ELIMINAR";
    TipoMensaje["EXITO_ELIMINAR"] = "EXITO_ELIMINAR";
})(TipoMensaje || (TipoMensaje = {}));
var MensajesService = /** @class */ (function () {
    function MensajesService() {
        this.jsonMensajes = this.getMensajes();
    }
    MensajesService.prototype.mostrarMensaje = function (tipo, entidadMensaje) {
        switch (tipo) {
            case TipoMensaje.EXITO:
                return this.mensajeExito(entidadMensaje);
            case TipoMensaje.ELIMINAR:
                return this.mensajeEliminar(entidadMensaje);
            case TipoMensaje.EXITO_ELIMINAR:
                return this.mensajeExitoEliminar(entidadMensaje);
            default:
                break;
        }
    };
    MensajesService.prototype.mensajeExitoEliminar = function (entidadMensaje) {
        var mensaje = Object.assign({}, this.jsonMensajes.EXITO_ELIMINAR);
        mensaje.text = entidadMensaje.getSingularConArticulo() + mensaje.text;
        return Swal.fire(mensaje);
    };
    MensajesService.prototype.mensajeEliminar = function (entidadMensaje) {
        var mensaje = Object.assign({}, this.jsonMensajes.ELIMINAR);
        mensaje.text = mensaje.text + entidadMensaje.getSingularConArticulo();
        return Swal.fire(mensaje);
    };
    MensajesService.prototype.mensajeExito = function (entidadMensaje) {
        var mensaje = Object.assign({}, this.jsonMensajes.EXITO);
        mensaje.text = entidadMensaje.getSingularConArticulo() + mensaje.text;
        return Swal.fire(mensaje);
    };
    MensajesService.prototype.getMensajes = function () {
        return {
            "CARGANDO": {
                "title": "Cargando..!!!",
                "width": 600,
                "padding": "3em",
                "background": "#fff",
                "backdrop": "rgba(0,0,0,0.7)"
            },
            "EXITO": {
                "icon": "success",
                "title": "Carga Exitosa",
                "text": " se guardo correctamente"
            },
            "EXITO_ELIMINAR": {
                "icon": "success",
                "title": "Eliminación Exitosa",
                "text": " se ha eliminado correctamente"
            },
            "ERROR": {
                "icon": "error",
                "title": "Error",
                "text": "Algo salió mal!"
            },
            "ELIMINAR": {
                "title": "Eliminar",
                "text": "¿ Esta seguro que desea eliminar ?",
                "icon": "warning",
                "showCancelButton": true,
                "confirmButtonColor": "#3085d6",
                "cancelButtonColor": "#d33",
                "confirmButtonText": "Si"
            }
        };
    };
    MensajesService.ɵprov = i0.ɵɵdefineInjectable({ factory: function MensajesService_Factory() { return new MensajesService(); }, token: MensajesService, providedIn: "root" });
    MensajesService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], MensajesService);
    return MensajesService;
}());
export { MensajesService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVuc2FqZXMuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2l2YWktZ2VuZXJpY28tbGlicmVyaWEvIiwic291cmNlcyI6WyJsaWIvZ2VuZXJpY28vbWVuc2FqZXMvbWVuc2FqZXMuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLElBQTBCLE1BQU0sYUFBYSxDQUFDOztBQUVyRCxNQUFNLENBQU4sSUFBWSxXQUtYO0FBTEQsV0FBWSxXQUFXO0lBQ3JCLDhCQUFhLENBQUE7SUFDYiw4QkFBYSxDQUFBO0lBQ2Isb0NBQW1CLENBQUE7SUFDbkIsZ0RBQStCLENBQUE7QUFDakMsQ0FBQyxFQUxXLFdBQVcsS0FBWCxXQUFXLFFBS3RCO0FBSUQ7SUFJRTtRQURBLGlCQUFZLEdBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ2hCLENBQUM7SUFFakIsd0NBQWMsR0FBZCxVQUFlLElBQWdCLEVBQUMsY0FBNkI7UUFDM0QsUUFBUSxJQUFJLEVBQUU7WUFDWixLQUFLLFdBQVcsQ0FBQyxLQUFLO2dCQUNwQixPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDM0MsS0FBSyxXQUFXLENBQUMsUUFBUTtnQkFDdkIsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQzlDLEtBQUssV0FBVyxDQUFDLGNBQWM7Z0JBQzdCLE9BQU8sSUFBSSxDQUFDLG9CQUFvQixDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQ25EO2dCQUNFLE1BQU07U0FDVDtJQUNILENBQUM7SUFFRCw4Q0FBb0IsR0FBcEIsVUFBcUIsY0FBOEI7UUFDakQsSUFBSSxPQUFPLEdBQUssTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNuRSxPQUFPLENBQUMsSUFBSSxHQUFDLGNBQWMsQ0FBQyxzQkFBc0IsRUFBRSxHQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7UUFDbEUsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQzVCLENBQUM7SUFFRCx5Q0FBZSxHQUFmLFVBQWdCLGNBQThCO1FBQzVDLElBQUksT0FBTyxHQUFLLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDN0QsT0FBTyxDQUFDLElBQUksR0FBQyxPQUFPLENBQUMsSUFBSSxHQUFDLGNBQWMsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1FBQ2xFLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBQ0Qsc0NBQVksR0FBWixVQUFhLGNBQThCO1FBQ3pDLElBQUksT0FBTyxHQUFLLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDMUQsT0FBTyxDQUFDLElBQUksR0FBQyxjQUFjLENBQUMsc0JBQXNCLEVBQUUsR0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1FBQ2xFLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBQ0YscUNBQVcsR0FBWDtRQUNELE9BQU87WUFDTCxVQUFVLEVBQUM7Z0JBQ1AsT0FBTyxFQUFFLGVBQWU7Z0JBQ3hCLE9BQU8sRUFBRSxHQUFHO2dCQUNaLFNBQVMsRUFBRSxLQUFLO2dCQUNoQixZQUFZLEVBQUUsTUFBTTtnQkFDcEIsVUFBVSxFQUFFLGlCQUFpQjthQUM5QjtZQUNELE9BQU8sRUFBRTtnQkFDUCxNQUFNLEVBQUUsU0FBUztnQkFDakIsT0FBTyxFQUFFLGVBQWU7Z0JBQ3hCLE1BQU0sRUFBRSwwQkFBMEI7YUFDbkM7WUFDRCxnQkFBZ0IsRUFBRTtnQkFDaEIsTUFBTSxFQUFFLFNBQVM7Z0JBQ2pCLE9BQU8sRUFBRSxxQkFBcUI7Z0JBQzlCLE1BQU0sRUFBRSxnQ0FBZ0M7YUFDekM7WUFDRCxPQUFPLEVBQUU7Z0JBQ1AsTUFBTSxFQUFFLE9BQU87Z0JBQ2YsT0FBTyxFQUFFLE9BQU87Z0JBQ2hCLE1BQU0sRUFBRSxpQkFBaUI7YUFDMUI7WUFDRCxVQUFVLEVBQUM7Z0JBQ0wsT0FBTyxFQUFFLFVBQVU7Z0JBQ25CLE1BQU0sRUFBRSxvQ0FBb0M7Z0JBQzVDLE1BQU0sRUFBRSxTQUFTO2dCQUNqQixrQkFBa0IsRUFBRSxJQUFJO2dCQUN4QixvQkFBb0IsRUFBRSxTQUFTO2dCQUMvQixtQkFBbUIsRUFBRSxNQUFNO2dCQUMzQixtQkFBbUIsRUFBRSxJQUFJO2FBQzlCO1NBQ0YsQ0FBQTtJQUNGLENBQUM7O0lBckVXLGVBQWU7UUFIM0IsVUFBVSxDQUFDO1lBQ1YsVUFBVSxFQUFFLE1BQU07U0FDbkIsQ0FBQztPQUNXLGVBQWUsQ0F1RTNCOzBCQW5GRDtDQW1GQyxBQXZFRCxJQXVFQztTQXZFWSxlQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IFN3YWwsIHsgU3dlZXRBbGVydFJlc3VsdCB9IGZyb20gJ3N3ZWV0YWxlcnQyJztcbmltcG9ydCB7IEVudGlkYWRNZW5zYWplIH0gZnJvbSAnLi9lbnRpZGFkLW1lbnNhamUnO1xuZXhwb3J0IGVudW0gVGlwb01lbnNhamV7XG4gIEVYSVRPPVwiRVhJVE9cIixcbiAgRVJST1I9XCJFUlJPUlwiLFxuICBFTElNSU5BUj1cIkVMSU1JTkFSXCIsXG4gIEVYSVRPX0VMSU1JTkFSPVwiRVhJVE9fRUxJTUlOQVJcIlxufVxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgTWVuc2FqZXNTZXJ2aWNlIHtcblxuXG4gIGpzb25NZW5zYWplcz10aGlzLmdldE1lbnNhamVzKCk7XG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbW9zdHJhck1lbnNhamUodGlwbzpUaXBvTWVuc2FqZSxlbnRpZGFkTWVuc2FqZTpFbnRpZGFkTWVuc2FqZSk6UHJvbWlzZTxTd2VldEFsZXJ0UmVzdWx0PntcbiAgICBzd2l0Y2ggKHRpcG8pIHtcbiAgICAgIGNhc2UgVGlwb01lbnNhamUuRVhJVE86XG4gICAgICAgIHJldHVybiB0aGlzLm1lbnNhamVFeGl0byhlbnRpZGFkTWVuc2FqZSk7XG4gICAgICBjYXNlIFRpcG9NZW5zYWplLkVMSU1JTkFSOlxuICAgICAgICByZXR1cm4gdGhpcy5tZW5zYWplRWxpbWluYXIoZW50aWRhZE1lbnNhamUpO1xuICAgICAgY2FzZSBUaXBvTWVuc2FqZS5FWElUT19FTElNSU5BUjpcbiAgICAgICAgcmV0dXJuIHRoaXMubWVuc2FqZUV4aXRvRWxpbWluYXIoZW50aWRhZE1lbnNhamUpO1xuICAgICAgZGVmYXVsdDpcbiAgICAgICAgYnJlYWs7XG4gICAgfVxuICB9XG5cbiAgbWVuc2FqZUV4aXRvRWxpbWluYXIoZW50aWRhZE1lbnNhamU6IEVudGlkYWRNZW5zYWplKTogUHJvbWlzZTxTd2VldEFsZXJ0UmVzdWx0PiB7XG4gICAgbGV0IG1lbnNhamU6YW55PU9iamVjdC5hc3NpZ24oe30sdGhpcy5qc29uTWVuc2FqZXMuRVhJVE9fRUxJTUlOQVIpO1xuICAgIG1lbnNhamUudGV4dD1lbnRpZGFkTWVuc2FqZS5nZXRTaW5ndWxhckNvbkFydGljdWxvKCkrbWVuc2FqZS50ZXh0O1xuICAgIHJldHVybiBTd2FsLmZpcmUobWVuc2FqZSk7XG4gIH1cblxuICBtZW5zYWplRWxpbWluYXIoZW50aWRhZE1lbnNhamU6IEVudGlkYWRNZW5zYWplKTpQcm9taXNlPFN3ZWV0QWxlcnRSZXN1bHQ+IHtcbiAgICBsZXQgbWVuc2FqZTphbnk9T2JqZWN0LmFzc2lnbih7fSx0aGlzLmpzb25NZW5zYWplcy5FTElNSU5BUik7XG4gICAgbWVuc2FqZS50ZXh0PW1lbnNhamUudGV4dCtlbnRpZGFkTWVuc2FqZS5nZXRTaW5ndWxhckNvbkFydGljdWxvKCk7XG4gICAgcmV0dXJuIFN3YWwuZmlyZShtZW5zYWplKTtcbiAgfVxuICBtZW5zYWplRXhpdG8oZW50aWRhZE1lbnNhamU6IEVudGlkYWRNZW5zYWplKTpQcm9taXNlPFN3ZWV0QWxlcnRSZXN1bHQ+IHtcbiAgICBsZXQgbWVuc2FqZTphbnk9T2JqZWN0LmFzc2lnbih7fSx0aGlzLmpzb25NZW5zYWplcy5FWElUTyk7XG4gICAgbWVuc2FqZS50ZXh0PWVudGlkYWRNZW5zYWplLmdldFNpbmd1bGFyQ29uQXJ0aWN1bG8oKSttZW5zYWplLnRleHQ7XG4gICAgcmV0dXJuIFN3YWwuZmlyZShtZW5zYWplKTtcbiAgfVxuIGdldE1lbnNhamVzKCl7XG5yZXR1cm4ge1xuICBcIkNBUkdBTkRPXCI6e1xuICAgICAgXCJ0aXRsZVwiOiBcIkNhcmdhbmRvLi4hISFcIixcbiAgICAgIFwid2lkdGhcIjogNjAwLFxuICAgICAgXCJwYWRkaW5nXCI6IFwiM2VtXCIsXG4gICAgICBcImJhY2tncm91bmRcIjogXCIjZmZmXCIsXG4gICAgICBcImJhY2tkcm9wXCI6IFwicmdiYSgwLDAsMCwwLjcpXCJcbiAgICB9LFxuICAgIFwiRVhJVE9cIjoge1xuICAgICAgXCJpY29uXCI6IFwic3VjY2Vzc1wiLFxuICAgICAgXCJ0aXRsZVwiOiBcIkNhcmdhIEV4aXRvc2FcIixcbiAgICAgIFwidGV4dFwiOiBcIiBzZSBndWFyZG8gY29ycmVjdGFtZW50ZVwiXG4gICAgfSxcbiAgICBcIkVYSVRPX0VMSU1JTkFSXCI6IHtcbiAgICAgIFwiaWNvblwiOiBcInN1Y2Nlc3NcIixcbiAgICAgIFwidGl0bGVcIjogXCJFbGltaW5hY2nDs24gRXhpdG9zYVwiLFxuICAgICAgXCJ0ZXh0XCI6IFwiIHNlIGhhIGVsaW1pbmFkbyBjb3JyZWN0YW1lbnRlXCJcbiAgICB9LFxuICAgIFwiRVJST1JcIjoge1xuICAgICAgXCJpY29uXCI6IFwiZXJyb3JcIixcbiAgICAgIFwidGl0bGVcIjogXCJFcnJvclwiLFxuICAgICAgXCJ0ZXh0XCI6IFwiQWxnbyBzYWxpw7MgbWFsIVwiXG4gICAgfSxcbiAgICBcIkVMSU1JTkFSXCI6eyAgICBcbiAgICAgICAgICBcInRpdGxlXCI6IFwiRWxpbWluYXJcIixcbiAgICAgICAgICBcInRleHRcIjogXCLCvyBFc3RhIHNlZ3VybyBxdWUgZGVzZWEgZWxpbWluYXIgP1wiLFxuICAgICAgICAgIFwiaWNvblwiOiBcIndhcm5pbmdcIixcbiAgICAgICAgICBcInNob3dDYW5jZWxCdXR0b25cIjogdHJ1ZSxcbiAgICAgICAgICBcImNvbmZpcm1CdXR0b25Db2xvclwiOiBcIiMzMDg1ZDZcIixcbiAgICAgICAgICBcImNhbmNlbEJ1dHRvbkNvbG9yXCI6IFwiI2QzM1wiLFxuICAgICAgICAgIFwiY29uZmlybUJ1dHRvblRleHRcIjogXCJTaVwiICAgICAgXG4gICAgfVxuICB9XG4gfVxuICBcbn1cbiJdfQ==