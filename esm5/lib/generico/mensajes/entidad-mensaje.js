var EntidadMensaje = /** @class */ (function () {
    function EntidadMensaje(entidadO) {
        var _this = this;
        this.articulo = null;
        this.singular = null;
        this.articuloPlural = null;
        this.plural = null;
        this.femenino = false;
        //recorro las propiedades del nuevo objeto
        //este metodo se utiliza para sobreescribir las propiedades del objeto        
        Object.keys(this).forEach(function (k) {
            console.log(Object.keys(entidadO).indexOf(k));
            if (Object.keys(entidadO).includes(k)) {
                _this[k] = entidadO[k];
            }
        });
    }
    EntidadMensaje.prototype.getSingular = function () {
        return this.singular;
    };
    EntidadMensaje.prototype.getPlural = function () {
        if (this.plural != null) {
            return this.plural;
        }
        return this.singular + "s";
    };
    EntidadMensaje.prototype.getSingularConArticulo = function () {
        if (this.articulo != null) {
            return this.articulo + " " + this.getSingular();
        }
        this.articulo = "El";
        if (this.femenino) {
            this.articulo = "La";
        }
        return this.articulo + " " + this.getSingular();
    };
    EntidadMensaje.prototype.getPluralConArticulo = function () {
        if (this.articuloPlural != null) {
            return this.articuloPlural + " " + this.getPlural();
        }
        this.articuloPlural = "Los";
        if (this.femenino) {
            this.articulo = "Las";
        }
        return this.articuloPlural + " " + this.getPlural();
    };
    return EntidadMensaje;
}());
export { EntidadMensaje };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW50aWRhZC1tZW5zYWplLmpzIiwic291cmNlUm9vdCI6Im5nOi8vaXZhaS1nZW5lcmljby1saWJyZXJpYS8iLCJzb3VyY2VzIjpbImxpYi9nZW5lcmljby9tZW5zYWplcy9lbnRpZGFkLW1lbnNhamUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFRSSx3QkFBWSxRQUFZO1FBQXhCLGlCQVNDO1FBaEJELGFBQVEsR0FBUSxJQUFJLENBQUM7UUFDckIsYUFBUSxHQUFRLElBQUksQ0FBQztRQUNyQixtQkFBYyxHQUFRLElBQUksQ0FBQztRQUMzQixXQUFNLEdBQVEsSUFBSSxDQUFDO1FBQ25CLGFBQVEsR0FBUyxLQUFLLENBQUM7UUFJbkIsMENBQTBDO1FBQzFDLDhFQUE4RTtRQUM5RSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFBLENBQUM7WUFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzlDLElBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUM7Z0JBQ2pDLEtBQUksQ0FBQyxDQUFDLENBQUMsR0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDdkI7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFHRCxvQ0FBVyxHQUFYO1FBQ0ksT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQ3pCLENBQUM7SUFFRCxrQ0FBUyxHQUFUO1FBQ0ksSUFBRyxJQUFJLENBQUMsTUFBTSxJQUFFLElBQUksRUFBQztZQUFDLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQTtTQUFDO1FBQ3pDLE9BQU8sSUFBSSxDQUFDLFFBQVEsR0FBQyxHQUFHLENBQUM7SUFDN0IsQ0FBQztJQUVELCtDQUFzQixHQUF0QjtRQUNJLElBQUcsSUFBSSxDQUFDLFFBQVEsSUFBRSxJQUFJLEVBQUM7WUFDbkIsT0FBTyxJQUFJLENBQUMsUUFBUSxHQUFDLEdBQUcsR0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDL0M7UUFDRCxJQUFJLENBQUMsUUFBUSxHQUFDLElBQUksQ0FBQztRQUNuQixJQUFHLElBQUksQ0FBQyxRQUFRLEVBQUM7WUFBQyxJQUFJLENBQUMsUUFBUSxHQUFDLElBQUksQ0FBQztTQUFDO1FBQ3RDLE9BQU8sSUFBSSxDQUFDLFFBQVEsR0FBQyxHQUFHLEdBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ2hELENBQUM7SUFFRCw2Q0FBb0IsR0FBcEI7UUFDSSxJQUFHLElBQUksQ0FBQyxjQUFjLElBQUUsSUFBSSxFQUFDO1lBQ3pCLE9BQU8sSUFBSSxDQUFDLGNBQWMsR0FBQyxHQUFHLEdBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1NBQ25EO1FBQ0QsSUFBSSxDQUFDLGNBQWMsR0FBQyxLQUFLLENBQUM7UUFDMUIsSUFBRyxJQUFJLENBQUMsUUFBUSxFQUFDO1lBQUMsSUFBSSxDQUFDLFFBQVEsR0FBQyxLQUFLLENBQUM7U0FBQztRQUN2QyxPQUFPLElBQUksQ0FBQyxjQUFjLEdBQUMsR0FBRyxHQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztJQUNwRCxDQUFDO0lBQ0wscUJBQUM7QUFBRCxDQUFDLEFBOUNELElBOENDIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIEVudGlkYWRNZW5zYWplIHtcbiAgICBhcnRpY3VsbzpzdHJpbmc9bnVsbDtcbiAgICBzaW5ndWxhcjpzdHJpbmc9bnVsbDtcbiAgICBhcnRpY3Vsb1BsdXJhbDpzdHJpbmc9bnVsbDtcbiAgICBwbHVyYWw6c3RyaW5nPW51bGw7XG4gICAgZmVtZW5pbm86Ym9vbGVhbj1mYWxzZTtcbiAgIFxuXG4gICAgY29uc3RydWN0b3IoZW50aWRhZE86YW55KXsgICAgICAgXG4gICAgICAgIC8vcmVjb3JybyBsYXMgcHJvcGllZGFkZXMgZGVsIG51ZXZvIG9iamV0b1xuICAgICAgICAvL2VzdGUgbWV0b2RvIHNlIHV0aWxpemEgcGFyYSBzb2JyZWVzY3JpYmlyIGxhcyBwcm9waWVkYWRlcyBkZWwgb2JqZXRvICAgICAgICBcbiAgICAgICAgT2JqZWN0LmtleXModGhpcykuZm9yRWFjaChrID0+IHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKE9iamVjdC5rZXlzKGVudGlkYWRPKS5pbmRleE9mKGspKTtcbiAgICAgICAgICAgIGlmKE9iamVjdC5rZXlzKGVudGlkYWRPKS5pbmNsdWRlcyhrKSl7XG4gICAgICAgICAgICAgICAgdGhpc1trXT1lbnRpZGFkT1trXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxuXG5cbiAgICBnZXRTaW5ndWxhcigpOnN0cmluZ3tcbiAgICAgICAgcmV0dXJuIHRoaXMuc2luZ3VsYXI7XG4gICAgfVxuXG4gICAgZ2V0UGx1cmFsKCk6c3RyaW5ne1xuICAgICAgICBpZih0aGlzLnBsdXJhbCE9bnVsbCl7cmV0dXJuIHRoaXMucGx1cmFsfVxuICAgICAgICByZXR1cm4gdGhpcy5zaW5ndWxhcitcInNcIjtcbiAgICB9XG5cbiAgICBnZXRTaW5ndWxhckNvbkFydGljdWxvKCk6c3RyaW5ne1xuICAgICAgICBpZih0aGlzLmFydGljdWxvIT1udWxsKXtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmFydGljdWxvK1wiIFwiK3RoaXMuZ2V0U2luZ3VsYXIoKTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLmFydGljdWxvPVwiRWxcIjtcbiAgICAgICAgaWYodGhpcy5mZW1lbmlubyl7dGhpcy5hcnRpY3Vsbz1cIkxhXCI7fVxuICAgICAgICByZXR1cm4gdGhpcy5hcnRpY3VsbytcIiBcIit0aGlzLmdldFNpbmd1bGFyKCk7XG4gICAgfVxuXG4gICAgZ2V0UGx1cmFsQ29uQXJ0aWN1bG8oKTpzdHJpbmd7XG4gICAgICAgIGlmKHRoaXMuYXJ0aWN1bG9QbHVyYWwhPW51bGwpe1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuYXJ0aWN1bG9QbHVyYWwrXCIgXCIrdGhpcy5nZXRQbHVyYWwoKTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLmFydGljdWxvUGx1cmFsPVwiTG9zXCI7XG4gICAgICAgIGlmKHRoaXMuZmVtZW5pbm8pe3RoaXMuYXJ0aWN1bG89XCJMYXNcIjt9XG4gICAgICAgIHJldHVybiB0aGlzLmFydGljdWxvUGx1cmFsK1wiIFwiK3RoaXMuZ2V0UGx1cmFsKCk7XG4gICAgfVxufVxuIl19