import { BotonIconoEstilo } from './formlys/boton-icono/boton-icono-estilo';
var Configuracion = /** @class */ (function () {
    function Configuracion() {
    }
    Configuracion.obtenerClaseIcono = function (tipo) {
        switch (tipo) {
            case "editar":
                return new BotonIconoEstilo("fa fa-edit", "btn btn-primary");
            case "eliminar":
                return new BotonIconoEstilo("fa fa-trash", "btn btn-danger");
            case "ver":
                return new BotonIconoEstilo("fa fa-search", "btn btn-info");
            default:
                return new BotonIconoEstilo("fa fa-fingerprint", "btn btn-warning");
        }
    };
    Configuracion.MODOS = { VER: 0, EDITAR: 1, NUEVO: 2 };
    return Configuracion;
}());
export { Configuracion };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlndXJhY2lvbi5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2l2YWktZ2VuZXJpY28tbGlicmVyaWEvIiwic291cmNlcyI6WyJsaWIvZ2VuZXJpY28vY29uZmlndXJhY2lvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQU01RTtJQUFBO0lBZ0JBLENBQUM7SUFaVSwrQkFBaUIsR0FBeEIsVUFBeUIsSUFBWTtRQUNqQyxRQUFRLElBQUksRUFBRTtZQUNWLEtBQUssUUFBUTtnQkFDVCxPQUFPLElBQUksZ0JBQWdCLENBQUMsWUFBWSxFQUFDLGlCQUFpQixDQUFDLENBQUM7WUFDaEUsS0FBSyxVQUFVO2dCQUNYLE9BQU8sSUFBSSxnQkFBZ0IsQ0FBQyxhQUFhLEVBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUNoRSxLQUFLLEtBQUs7Z0JBQ04sT0FBTyxJQUFJLGdCQUFnQixDQUFDLGNBQWMsRUFBQyxjQUFjLENBQUMsQ0FBQztZQUMvRDtnQkFDSSxPQUFPLElBQUksZ0JBQWdCLENBQUMsbUJBQW1CLEVBQUMsaUJBQWlCLENBQUMsQ0FBQztTQUMxRTtJQUNMLENBQUM7SUFiTSxtQkFBSyxHQUFDLEVBQUMsR0FBRyxFQUFDLENBQUMsRUFBQyxNQUFNLEVBQUMsQ0FBQyxFQUFDLEtBQUssRUFBQyxDQUFDLEVBQUMsQ0FBQztJQWMxQyxvQkFBQztDQUFBLEFBaEJELElBZ0JDO1NBaEJZLGFBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBCb3Rvbkljb25vRXN0aWxvIH0gZnJvbSAnLi9mb3JtbHlzL2JvdG9uLWljb25vL2JvdG9uLWljb25vLWVzdGlsbyc7XG5cbmludGVyZmFjZSBHZW5lcmljb3tcbiAgICBodG1sO1xuICAgIGNzcztcbn1cbmV4cG9ydCBjbGFzcyBDb25maWd1cmFjaW9uIHtcbiAgICAgICBcbiAgICBzdGF0aWMgTU9ET1M9e1ZFUjowLEVESVRBUjoxLE5VRVZPOjJ9O1xuXG4gICAgc3RhdGljIG9idGVuZXJDbGFzZUljb25vKHRpcG86IHN0cmluZyk6IEJvdG9uSWNvbm9Fc3RpbG8ge1xuICAgICAgICBzd2l0Y2ggKHRpcG8pIHtcbiAgICAgICAgICAgIGNhc2UgXCJlZGl0YXJcIjpcbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3IEJvdG9uSWNvbm9Fc3RpbG8oXCJmYSBmYS1lZGl0XCIsXCJidG4gYnRuLXByaW1hcnlcIik7XG4gICAgICAgICAgICBjYXNlIFwiZWxpbWluYXJcIjpcbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3IEJvdG9uSWNvbm9Fc3RpbG8oXCJmYSBmYS10cmFzaFwiLFwiYnRuIGJ0bi1kYW5nZXJcIik7XG4gICAgICAgICAgICBjYXNlIFwidmVyXCI6XG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBCb3Rvbkljb25vRXN0aWxvKFwiZmEgZmEtc2VhcmNoXCIsXCJidG4gYnRuLWluZm9cIik7ICAgICAgICAgICBcbiAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBCb3Rvbkljb25vRXN0aWxvKFwiZmEgZmEtZmluZ2VycHJpbnRcIixcImJ0biBidG4td2FybmluZ1wiKTsgXG4gICAgICAgIH1cbiAgICB9XG59XG4iXX0=