import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/public_api';
var ModalTablaComponent = /** @class */ (function () {
    function ModalTablaComponent(modalRef) {
        this.modalRef = modalRef;
        this.item = {};
        this.columnas = [];
        this.acciones = [];
    }
    ModalTablaComponent.prototype.getThis = function () {
        return this;
    };
    ModalTablaComponent.prototype.nuevo = function () {
        this.modalRef.hide();
        this.observador.nuevo();
    };
    ModalTablaComponent.prototype.editar = function (id) {
        this.modalRef.hide();
        this.observador.editar(id);
    };
    ModalTablaComponent.prototype.eliminar = function (id) {
        this.modalRef.hide();
        this.observador.eliminar(id);
    };
    ModalTablaComponent.prototype.ver = function (id) {
        this.modalRef.hide();
        this.observador.ver(id);
    };
    ModalTablaComponent.prototype.ngOnInit = function () {
    };
    ModalTablaComponent.prototype.getTitulo = function () {
        var titulo = this.columnas.find(function (col) { return col.titulo; });
        if (titulo) {
            return this.item[titulo.prop];
        }
        return "Logisoft";
    };
    ModalTablaComponent.ctorParameters = function () { return [
        { type: BsModalRef }
    ]; };
    ModalTablaComponent = __decorate([
        Component({
            selector: 'app-modal-tabla',
            template: "<div class=\"modal-header\">\n  <h4 class=\"modal-title pull-left\" id=\"my-modal-title\">{{getTitulo()}}</h4>\n  <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"modalRef.hide()\">\n    <span aria-hidden=\"true\">&times;</span>\n  </button>   \n  \n</div>\n\n<div class=\"modal-body\">\n  <div class=\"col-12 \">\n    <app-field-generico class=\"float-right\" style=\"padding: 0.1rem;\" *ngFor=\"let accion of acciones\" [col]=\"accion\" [elemento]=\"item\" [observador]=\"getThis()\" ></app-field-generico>\n  </div>\n  <br>\n  \n    <ul class=\"list-group col-12\">    \n      <li *ngFor=\"let columna of columnas\" class=\"list-group-item disabled\">\n        <div class=\"row \"><h6>{{columna.nombre}}</h6></div>      \n        <div class=\"row justify-content-center align-items-center\">\n          <app-field-generico  [col]=\"columna\" [elemento]=\"item\">\n          </app-field-generico>\n        </div>\n       \n      </li>\n      \n    </ul>\n  \n  \n  \n</div>",
            styles: [""]
        })
    ], ModalTablaComponent);
    return ModalTablaComponent;
}());
export { ModalTablaComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwtdGFibGEuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaXZhaS1nZW5lcmljby1saWJyZXJpYS8iLCJzb3VyY2VzIjpbImxpYi9nZW5lcmljby90YWJsYS9tb2RhbC10YWJsYS9tb2RhbC10YWJsYS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFDbEQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBUzVEO0lBS0UsNkJBQW1CLFFBQW1CO1FBQW5CLGFBQVEsR0FBUixRQUFRLENBQVc7UUFKckMsU0FBSSxHQUFDLEVBQUUsQ0FBQztRQUNSLGFBQVEsR0FBQyxFQUFFLENBQUM7UUFDWixhQUFRLEdBQUMsRUFBRSxDQUFDO0lBRTZCLENBQUM7SUFFM0MscUNBQU8sR0FBUDtRQUNFLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUNELG1DQUFLLEdBQUw7UUFDRSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDMUIsQ0FBQztJQUNELG9DQUFNLEdBQU4sVUFBTyxFQUFPO1FBQ1osSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUM3QixDQUFDO0lBQ0Qsc0NBQVEsR0FBUixVQUFTLEVBQU87UUFDZCxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFDRCxpQ0FBRyxHQUFILFVBQUksRUFBTztRQUNULElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDMUIsQ0FBQztJQUVELHNDQUFRLEdBQVI7SUFDQSxDQUFDO0lBR0QsdUNBQVMsR0FBVDtRQUNFLElBQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxDQUFDLE1BQU0sRUFBVixDQUFVLENBQUMsQ0FBQztRQUNyRCxJQUFHLE1BQU0sRUFBQztZQUFDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7U0FBQztRQUMxQyxPQUFPLFVBQVUsQ0FBQztJQUNwQixDQUFDOztnQkE5QjJCLFVBQVU7O0lBTDNCLG1CQUFtQjtRQUwvQixTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsaUJBQWlCO1lBQzNCLDQvQkFBMkM7O1NBRTVDLENBQUM7T0FDVyxtQkFBbUIsQ0FxQy9CO0lBQUQsMEJBQUM7Q0FBQSxBQXJDRCxJQXFDQztTQXJDWSxtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQnNNb2RhbFJlZiB9IGZyb20gJ25neC1ib290c3RyYXAvbW9kYWwvcHVibGljX2FwaSc7XG5pbXBvcnQgeyBBY2Npb25lc0dlbmVyYWxlcyB9IGZyb20gJy4uLy4uL2Zvcm1seXMvYWNjaW9uZXMvYWNjaW9uZXMtZ2VuZXJhbGVzJztcbmltcG9ydCB7IFRhYmxhQ29tcG9uZW50IH0gZnJvbSAnLi4vdGFibGEuY29tcG9uZW50JztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYXBwLW1vZGFsLXRhYmxhJyxcbiAgdGVtcGxhdGVVcmw6ICcuL21vZGFsLXRhYmxhLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vbW9kYWwtdGFibGEuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBNb2RhbFRhYmxhQ29tcG9uZW50PFQ+IGltcGxlbWVudHMgT25Jbml0LEFjY2lvbmVzR2VuZXJhbGVzIHtcbiAgIGl0ZW09e307XG4gICBjb2x1bW5hcz1bXTtcbiAgIGFjY2lvbmVzPVtdO1xuICAgb2JzZXJ2YWRvcjogVGFibGFDb21wb25lbnQ8VD47XG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBtb2RhbFJlZjpCc01vZGFsUmVmKSB7IH1cblxuICBnZXRUaGlzKCl7XG4gICAgcmV0dXJuIHRoaXM7XG4gIH1cbiAgbnVldm8oKSB7XG4gICAgdGhpcy5tb2RhbFJlZi5oaWRlKCk7XG4gICAgdGhpcy5vYnNlcnZhZG9yLm51ZXZvKCk7XG4gIH1cbiAgZWRpdGFyKGlkOiBhbnkpIHtcbiAgICB0aGlzLm1vZGFsUmVmLmhpZGUoKTtcbiAgICB0aGlzLm9ic2VydmFkb3IuZWRpdGFyKGlkKTtcbiAgfVxuICBlbGltaW5hcihpZDogYW55KSB7XG4gICAgdGhpcy5tb2RhbFJlZi5oaWRlKCk7XG4gICAgdGhpcy5vYnNlcnZhZG9yLmVsaW1pbmFyKGlkKTtcbiAgfVxuICB2ZXIoaWQ6IGFueSkge1xuICAgIHRoaXMubW9kYWxSZWYuaGlkZSgpO1xuICAgIHRoaXMub2JzZXJ2YWRvci52ZXIoaWQpO1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuXG4gIGdldFRpdHVsbygpOnN0cmluZ3tcbiAgICBjb25zdCB0aXR1bG8gPSB0aGlzLmNvbHVtbmFzLmZpbmQoY29sID0+IGNvbC50aXR1bG8pO1xuICAgIGlmKHRpdHVsbyl7cmV0dXJuIHRoaXMuaXRlbVt0aXR1bG8ucHJvcF07fVxuICAgIHJldHVybiBcIkxvZ2lzb2Z0XCI7XG4gIH1cblxufVxuIl19