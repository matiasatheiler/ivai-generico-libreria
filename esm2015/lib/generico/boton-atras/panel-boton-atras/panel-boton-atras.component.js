import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LogisoftStorage } from '../../storage/logisoft-storage';
let PanelBotonAtrasComponent = class PanelBotonAtrasComponent {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
    }
    existePantallaAtras() {
        return LogisoftStorage.isAtras();
    }
    atras() {
        this.router.navigate([LogisoftStorage.getPantallaAtras().url]);
    }
};
PanelBotonAtrasComponent.ctorParameters = () => [
    { type: Router }
];
PanelBotonAtrasComponent = __decorate([
    Component({
        selector: 'app-panel-boton-atras',
        template: "<div class=\"row mb-4 ml-0\">\n \n    <button type=\"button\" class=\"btn btn-info\" *ngIf=\"existePantallaAtras()\" (click)=\"atras()\"><i class=\"fa fa-chevron-circle-left\"></i> Atr\u00E1s</button>\n \n</div>\n",
        styles: [""]
    })
], PanelBotonAtrasComponent);
export { PanelBotonAtrasComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFuZWwtYm90b24tYXRyYXMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaXZhaS1nZW5lcmljby1saWJyZXJpYS8iLCJzb3VyY2VzIjpbImxpYi9nZW5lcmljby9ib3Rvbi1hdHJhcy9wYW5lbC1ib3Rvbi1hdHJhcy9wYW5lbC1ib3Rvbi1hdHJhcy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFFbEQsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ3pDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQU9qRSxJQUFhLHdCQUF3QixHQUFyQyxNQUFhLHdCQUF3QjtJQUVuQyxZQUFvQixNQUFhO1FBQWIsV0FBTSxHQUFOLE1BQU0sQ0FBTztJQUFJLENBQUM7SUFFdEMsUUFBUTtJQUNSLENBQUM7SUFFRCxtQkFBbUI7UUFDakIsT0FBTyxlQUFlLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDbkMsQ0FBQztJQUVELEtBQUs7UUFDSCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDakUsQ0FBQztDQUVGLENBQUE7O1lBYjRCLE1BQU07O0FBRnRCLHdCQUF3QjtJQUxwQyxTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsdUJBQXVCO1FBQ2pDLGlPQUFpRDs7S0FFbEQsQ0FBQztHQUNXLHdCQUF3QixDQWVwQztTQWZZLHdCQUF3QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBMb2dpc29mdFN0b3JhZ2UgfSBmcm9tICcuLi8uLi9zdG9yYWdlL2xvZ2lzb2Z0LXN0b3JhZ2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhcHAtcGFuZWwtYm90b24tYXRyYXMnLFxuICB0ZW1wbGF0ZVVybDogJy4vcGFuZWwtYm90b24tYXRyYXMuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9wYW5lbC1ib3Rvbi1hdHJhcy5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIFBhbmVsQm90b25BdHJhc0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSByb3V0ZXI6Um91dGVyKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG4gIGV4aXN0ZVBhbnRhbGxhQXRyYXMoKXtcbiAgICByZXR1cm4gTG9naXNvZnRTdG9yYWdlLmlzQXRyYXMoKTtcbiAgfVxuXG4gIGF0cmFzKCl7XG4gICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW0xvZ2lzb2Z0U3RvcmFnZS5nZXRQYW50YWxsYUF0cmFzKCkudXJsXSk7XG4gIH1cblxufVxuIl19