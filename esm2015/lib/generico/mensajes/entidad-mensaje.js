export class EntidadMensaje {
    constructor(entidadO) {
        this.articulo = null;
        this.singular = null;
        this.articuloPlural = null;
        this.plural = null;
        this.femenino = false;
        //recorro las propiedades del nuevo objeto
        //este metodo se utiliza para sobreescribir las propiedades del objeto        
        Object.keys(this).forEach(k => {
            console.log(Object.keys(entidadO).indexOf(k));
            if (Object.keys(entidadO).includes(k)) {
                this[k] = entidadO[k];
            }
        });
    }
    getSingular() {
        return this.singular;
    }
    getPlural() {
        if (this.plural != null) {
            return this.plural;
        }
        return this.singular + "s";
    }
    getSingularConArticulo() {
        if (this.articulo != null) {
            return this.articulo + " " + this.getSingular();
        }
        this.articulo = "El";
        if (this.femenino) {
            this.articulo = "La";
        }
        return this.articulo + " " + this.getSingular();
    }
    getPluralConArticulo() {
        if (this.articuloPlural != null) {
            return this.articuloPlural + " " + this.getPlural();
        }
        this.articuloPlural = "Los";
        if (this.femenino) {
            this.articulo = "Las";
        }
        return this.articuloPlural + " " + this.getPlural();
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW50aWRhZC1tZW5zYWplLmpzIiwic291cmNlUm9vdCI6Im5nOi8vaXZhaS1nZW5lcmljby1saWJyZXJpYS8iLCJzb3VyY2VzIjpbImxpYi9nZW5lcmljby9tZW5zYWplcy9lbnRpZGFkLW1lbnNhamUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsTUFBTSxPQUFPLGNBQWM7SUFRdkIsWUFBWSxRQUFZO1FBUHhCLGFBQVEsR0FBUSxJQUFJLENBQUM7UUFDckIsYUFBUSxHQUFRLElBQUksQ0FBQztRQUNyQixtQkFBYyxHQUFRLElBQUksQ0FBQztRQUMzQixXQUFNLEdBQVEsSUFBSSxDQUFDO1FBQ25CLGFBQVEsR0FBUyxLQUFLLENBQUM7UUFJbkIsMENBQTBDO1FBQzFDLDhFQUE4RTtRQUM5RSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUMxQixPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDOUMsSUFBRyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBQztnQkFDakMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUN2QjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUdELFdBQVc7UUFDUCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDekIsQ0FBQztJQUVELFNBQVM7UUFDTCxJQUFHLElBQUksQ0FBQyxNQUFNLElBQUUsSUFBSSxFQUFDO1lBQUMsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFBO1NBQUM7UUFDekMsT0FBTyxJQUFJLENBQUMsUUFBUSxHQUFDLEdBQUcsQ0FBQztJQUM3QixDQUFDO0lBRUQsc0JBQXNCO1FBQ2xCLElBQUcsSUFBSSxDQUFDLFFBQVEsSUFBRSxJQUFJLEVBQUM7WUFDbkIsT0FBTyxJQUFJLENBQUMsUUFBUSxHQUFDLEdBQUcsR0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDL0M7UUFDRCxJQUFJLENBQUMsUUFBUSxHQUFDLElBQUksQ0FBQztRQUNuQixJQUFHLElBQUksQ0FBQyxRQUFRLEVBQUM7WUFBQyxJQUFJLENBQUMsUUFBUSxHQUFDLElBQUksQ0FBQztTQUFDO1FBQ3RDLE9BQU8sSUFBSSxDQUFDLFFBQVEsR0FBQyxHQUFHLEdBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ2hELENBQUM7SUFFRCxvQkFBb0I7UUFDaEIsSUFBRyxJQUFJLENBQUMsY0FBYyxJQUFFLElBQUksRUFBQztZQUN6QixPQUFPLElBQUksQ0FBQyxjQUFjLEdBQUMsR0FBRyxHQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztTQUNuRDtRQUNELElBQUksQ0FBQyxjQUFjLEdBQUMsS0FBSyxDQUFDO1FBQzFCLElBQUcsSUFBSSxDQUFDLFFBQVEsRUFBQztZQUFDLElBQUksQ0FBQyxRQUFRLEdBQUMsS0FBSyxDQUFDO1NBQUM7UUFDdkMsT0FBTyxJQUFJLENBQUMsY0FBYyxHQUFDLEdBQUcsR0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDcEQsQ0FBQztDQUNKIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIEVudGlkYWRNZW5zYWplIHtcbiAgICBhcnRpY3VsbzpzdHJpbmc9bnVsbDtcbiAgICBzaW5ndWxhcjpzdHJpbmc9bnVsbDtcbiAgICBhcnRpY3Vsb1BsdXJhbDpzdHJpbmc9bnVsbDtcbiAgICBwbHVyYWw6c3RyaW5nPW51bGw7XG4gICAgZmVtZW5pbm86Ym9vbGVhbj1mYWxzZTtcbiAgIFxuXG4gICAgY29uc3RydWN0b3IoZW50aWRhZE86YW55KXsgICAgICAgXG4gICAgICAgIC8vcmVjb3JybyBsYXMgcHJvcGllZGFkZXMgZGVsIG51ZXZvIG9iamV0b1xuICAgICAgICAvL2VzdGUgbWV0b2RvIHNlIHV0aWxpemEgcGFyYSBzb2JyZWVzY3JpYmlyIGxhcyBwcm9waWVkYWRlcyBkZWwgb2JqZXRvICAgICAgICBcbiAgICAgICAgT2JqZWN0LmtleXModGhpcykuZm9yRWFjaChrID0+IHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKE9iamVjdC5rZXlzKGVudGlkYWRPKS5pbmRleE9mKGspKTtcbiAgICAgICAgICAgIGlmKE9iamVjdC5rZXlzKGVudGlkYWRPKS5pbmNsdWRlcyhrKSl7XG4gICAgICAgICAgICAgICAgdGhpc1trXT1lbnRpZGFkT1trXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxuXG5cbiAgICBnZXRTaW5ndWxhcigpOnN0cmluZ3tcbiAgICAgICAgcmV0dXJuIHRoaXMuc2luZ3VsYXI7XG4gICAgfVxuXG4gICAgZ2V0UGx1cmFsKCk6c3RyaW5ne1xuICAgICAgICBpZih0aGlzLnBsdXJhbCE9bnVsbCl7cmV0dXJuIHRoaXMucGx1cmFsfVxuICAgICAgICByZXR1cm4gdGhpcy5zaW5ndWxhcitcInNcIjtcbiAgICB9XG5cbiAgICBnZXRTaW5ndWxhckNvbkFydGljdWxvKCk6c3RyaW5ne1xuICAgICAgICBpZih0aGlzLmFydGljdWxvIT1udWxsKXtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmFydGljdWxvK1wiIFwiK3RoaXMuZ2V0U2luZ3VsYXIoKTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLmFydGljdWxvPVwiRWxcIjtcbiAgICAgICAgaWYodGhpcy5mZW1lbmlubyl7dGhpcy5hcnRpY3Vsbz1cIkxhXCI7fVxuICAgICAgICByZXR1cm4gdGhpcy5hcnRpY3VsbytcIiBcIit0aGlzLmdldFNpbmd1bGFyKCk7XG4gICAgfVxuXG4gICAgZ2V0UGx1cmFsQ29uQXJ0aWN1bG8oKTpzdHJpbmd7XG4gICAgICAgIGlmKHRoaXMuYXJ0aWN1bG9QbHVyYWwhPW51bGwpe1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuYXJ0aWN1bG9QbHVyYWwrXCIgXCIrdGhpcy5nZXRQbHVyYWwoKTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLmFydGljdWxvUGx1cmFsPVwiTG9zXCI7XG4gICAgICAgIGlmKHRoaXMuZmVtZW5pbm8pe3RoaXMuYXJ0aWN1bG89XCJMYXNcIjt9XG4gICAgICAgIHJldHVybiB0aGlzLmFydGljdWxvUGx1cmFsK1wiIFwiK3RoaXMuZ2V0UGx1cmFsKCk7XG4gICAgfVxufVxuIl19