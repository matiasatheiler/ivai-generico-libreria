import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import * as i0 from "@angular/core";
export var TipoMensaje;
(function (TipoMensaje) {
    TipoMensaje["EXITO"] = "EXITO";
    TipoMensaje["ERROR"] = "ERROR";
    TipoMensaje["ELIMINAR"] = "ELIMINAR";
    TipoMensaje["EXITO_ELIMINAR"] = "EXITO_ELIMINAR";
})(TipoMensaje || (TipoMensaje = {}));
let MensajesService = class MensajesService {
    constructor() {
        this.jsonMensajes = this.getMensajes();
    }
    mostrarMensaje(tipo, entidadMensaje) {
        switch (tipo) {
            case TipoMensaje.EXITO:
                return this.mensajeExito(entidadMensaje);
            case TipoMensaje.ELIMINAR:
                return this.mensajeEliminar(entidadMensaje);
            case TipoMensaje.EXITO_ELIMINAR:
                return this.mensajeExitoEliminar(entidadMensaje);
            default:
                break;
        }
    }
    mensajeExitoEliminar(entidadMensaje) {
        let mensaje = Object.assign({}, this.jsonMensajes.EXITO_ELIMINAR);
        mensaje.text = entidadMensaje.getSingularConArticulo() + mensaje.text;
        return Swal.fire(mensaje);
    }
    mensajeEliminar(entidadMensaje) {
        let mensaje = Object.assign({}, this.jsonMensajes.ELIMINAR);
        mensaje.text = mensaje.text + entidadMensaje.getSingularConArticulo();
        return Swal.fire(mensaje);
    }
    mensajeExito(entidadMensaje) {
        let mensaje = Object.assign({}, this.jsonMensajes.EXITO);
        mensaje.text = entidadMensaje.getSingularConArticulo() + mensaje.text;
        return Swal.fire(mensaje);
    }
    getMensajes() {
        return {
            "CARGANDO": {
                "title": "Cargando..!!!",
                "width": 600,
                "padding": "3em",
                "background": "#fff",
                "backdrop": "rgba(0,0,0,0.7)"
            },
            "EXITO": {
                "icon": "success",
                "title": "Carga Exitosa",
                "text": " se guardo correctamente"
            },
            "EXITO_ELIMINAR": {
                "icon": "success",
                "title": "Eliminación Exitosa",
                "text": " se ha eliminado correctamente"
            },
            "ERROR": {
                "icon": "error",
                "title": "Error",
                "text": "Algo salió mal!"
            },
            "ELIMINAR": {
                "title": "Eliminar",
                "text": "¿ Esta seguro que desea eliminar ?",
                "icon": "warning",
                "showCancelButton": true,
                "confirmButtonColor": "#3085d6",
                "cancelButtonColor": "#d33",
                "confirmButtonText": "Si"
            }
        };
    }
};
MensajesService.ɵprov = i0.ɵɵdefineInjectable({ factory: function MensajesService_Factory() { return new MensajesService(); }, token: MensajesService, providedIn: "root" });
MensajesService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], MensajesService);
export { MensajesService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVuc2FqZXMuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2l2YWktZ2VuZXJpY28tbGlicmVyaWEvIiwic291cmNlcyI6WyJsaWIvZ2VuZXJpY28vbWVuc2FqZXMvbWVuc2FqZXMuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLElBQTBCLE1BQU0sYUFBYSxDQUFDOztBQUVyRCxNQUFNLENBQU4sSUFBWSxXQUtYO0FBTEQsV0FBWSxXQUFXO0lBQ3JCLDhCQUFhLENBQUE7SUFDYiw4QkFBYSxDQUFBO0lBQ2Isb0NBQW1CLENBQUE7SUFDbkIsZ0RBQStCLENBQUE7QUFDakMsQ0FBQyxFQUxXLFdBQVcsS0FBWCxXQUFXLFFBS3RCO0FBSUQsSUFBYSxlQUFlLEdBQTVCLE1BQWEsZUFBZTtJQUkxQjtRQURBLGlCQUFZLEdBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ2hCLENBQUM7SUFFakIsY0FBYyxDQUFDLElBQWdCLEVBQUMsY0FBNkI7UUFDM0QsUUFBUSxJQUFJLEVBQUU7WUFDWixLQUFLLFdBQVcsQ0FBQyxLQUFLO2dCQUNwQixPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDM0MsS0FBSyxXQUFXLENBQUMsUUFBUTtnQkFDdkIsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQzlDLEtBQUssV0FBVyxDQUFDLGNBQWM7Z0JBQzdCLE9BQU8sSUFBSSxDQUFDLG9CQUFvQixDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQ25EO2dCQUNFLE1BQU07U0FDVDtJQUNILENBQUM7SUFFRCxvQkFBb0IsQ0FBQyxjQUE4QjtRQUNqRCxJQUFJLE9BQU8sR0FBSyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBQyxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ25FLE9BQU8sQ0FBQyxJQUFJLEdBQUMsY0FBYyxDQUFDLHNCQUFzQixFQUFFLEdBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztRQUNsRSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDNUIsQ0FBQztJQUVELGVBQWUsQ0FBQyxjQUE4QjtRQUM1QyxJQUFJLE9BQU8sR0FBSyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzdELE9BQU8sQ0FBQyxJQUFJLEdBQUMsT0FBTyxDQUFDLElBQUksR0FBQyxjQUFjLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztRQUNsRSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDNUIsQ0FBQztJQUNELFlBQVksQ0FBQyxjQUE4QjtRQUN6QyxJQUFJLE9BQU8sR0FBSyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzFELE9BQU8sQ0FBQyxJQUFJLEdBQUMsY0FBYyxDQUFDLHNCQUFzQixFQUFFLEdBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztRQUNsRSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDNUIsQ0FBQztJQUNGLFdBQVc7UUFDWixPQUFPO1lBQ0wsVUFBVSxFQUFDO2dCQUNQLE9BQU8sRUFBRSxlQUFlO2dCQUN4QixPQUFPLEVBQUUsR0FBRztnQkFDWixTQUFTLEVBQUUsS0FBSztnQkFDaEIsWUFBWSxFQUFFLE1BQU07Z0JBQ3BCLFVBQVUsRUFBRSxpQkFBaUI7YUFDOUI7WUFDRCxPQUFPLEVBQUU7Z0JBQ1AsTUFBTSxFQUFFLFNBQVM7Z0JBQ2pCLE9BQU8sRUFBRSxlQUFlO2dCQUN4QixNQUFNLEVBQUUsMEJBQTBCO2FBQ25DO1lBQ0QsZ0JBQWdCLEVBQUU7Z0JBQ2hCLE1BQU0sRUFBRSxTQUFTO2dCQUNqQixPQUFPLEVBQUUscUJBQXFCO2dCQUM5QixNQUFNLEVBQUUsZ0NBQWdDO2FBQ3pDO1lBQ0QsT0FBTyxFQUFFO2dCQUNQLE1BQU0sRUFBRSxPQUFPO2dCQUNmLE9BQU8sRUFBRSxPQUFPO2dCQUNoQixNQUFNLEVBQUUsaUJBQWlCO2FBQzFCO1lBQ0QsVUFBVSxFQUFDO2dCQUNMLE9BQU8sRUFBRSxVQUFVO2dCQUNuQixNQUFNLEVBQUUsb0NBQW9DO2dCQUM1QyxNQUFNLEVBQUUsU0FBUztnQkFDakIsa0JBQWtCLEVBQUUsSUFBSTtnQkFDeEIsb0JBQW9CLEVBQUUsU0FBUztnQkFDL0IsbUJBQW1CLEVBQUUsTUFBTTtnQkFDM0IsbUJBQW1CLEVBQUUsSUFBSTthQUM5QjtTQUNGLENBQUE7SUFDRixDQUFDO0NBRUQsQ0FBQTs7QUF2RVksZUFBZTtJQUgzQixVQUFVLENBQUM7UUFDVixVQUFVLEVBQUUsTUFBTTtLQUNuQixDQUFDO0dBQ1csZUFBZSxDQXVFM0I7U0F2RVksZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCBTd2FsLCB7IFN3ZWV0QWxlcnRSZXN1bHQgfSBmcm9tICdzd2VldGFsZXJ0Mic7XG5pbXBvcnQgeyBFbnRpZGFkTWVuc2FqZSB9IGZyb20gJy4vZW50aWRhZC1tZW5zYWplJztcbmV4cG9ydCBlbnVtIFRpcG9NZW5zYWple1xuICBFWElUTz1cIkVYSVRPXCIsXG4gIEVSUk9SPVwiRVJST1JcIixcbiAgRUxJTUlOQVI9XCJFTElNSU5BUlwiLFxuICBFWElUT19FTElNSU5BUj1cIkVYSVRPX0VMSU1JTkFSXCJcbn1cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIE1lbnNhamVzU2VydmljZSB7XG5cblxuICBqc29uTWVuc2FqZXM9dGhpcy5nZXRNZW5zYWplcygpO1xuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gIG1vc3RyYXJNZW5zYWplKHRpcG86VGlwb01lbnNhamUsZW50aWRhZE1lbnNhamU6RW50aWRhZE1lbnNhamUpOlByb21pc2U8U3dlZXRBbGVydFJlc3VsdD57XG4gICAgc3dpdGNoICh0aXBvKSB7XG4gICAgICBjYXNlIFRpcG9NZW5zYWplLkVYSVRPOlxuICAgICAgICByZXR1cm4gdGhpcy5tZW5zYWplRXhpdG8oZW50aWRhZE1lbnNhamUpO1xuICAgICAgY2FzZSBUaXBvTWVuc2FqZS5FTElNSU5BUjpcbiAgICAgICAgcmV0dXJuIHRoaXMubWVuc2FqZUVsaW1pbmFyKGVudGlkYWRNZW5zYWplKTtcbiAgICAgIGNhc2UgVGlwb01lbnNhamUuRVhJVE9fRUxJTUlOQVI6XG4gICAgICAgIHJldHVybiB0aGlzLm1lbnNhamVFeGl0b0VsaW1pbmFyKGVudGlkYWRNZW5zYWplKTtcbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIGJyZWFrO1xuICAgIH1cbiAgfVxuXG4gIG1lbnNhamVFeGl0b0VsaW1pbmFyKGVudGlkYWRNZW5zYWplOiBFbnRpZGFkTWVuc2FqZSk6IFByb21pc2U8U3dlZXRBbGVydFJlc3VsdD4ge1xuICAgIGxldCBtZW5zYWplOmFueT1PYmplY3QuYXNzaWduKHt9LHRoaXMuanNvbk1lbnNhamVzLkVYSVRPX0VMSU1JTkFSKTtcbiAgICBtZW5zYWplLnRleHQ9ZW50aWRhZE1lbnNhamUuZ2V0U2luZ3VsYXJDb25BcnRpY3VsbygpK21lbnNhamUudGV4dDtcbiAgICByZXR1cm4gU3dhbC5maXJlKG1lbnNhamUpO1xuICB9XG5cbiAgbWVuc2FqZUVsaW1pbmFyKGVudGlkYWRNZW5zYWplOiBFbnRpZGFkTWVuc2FqZSk6UHJvbWlzZTxTd2VldEFsZXJ0UmVzdWx0PiB7XG4gICAgbGV0IG1lbnNhamU6YW55PU9iamVjdC5hc3NpZ24oe30sdGhpcy5qc29uTWVuc2FqZXMuRUxJTUlOQVIpO1xuICAgIG1lbnNhamUudGV4dD1tZW5zYWplLnRleHQrZW50aWRhZE1lbnNhamUuZ2V0U2luZ3VsYXJDb25BcnRpY3VsbygpO1xuICAgIHJldHVybiBTd2FsLmZpcmUobWVuc2FqZSk7XG4gIH1cbiAgbWVuc2FqZUV4aXRvKGVudGlkYWRNZW5zYWplOiBFbnRpZGFkTWVuc2FqZSk6UHJvbWlzZTxTd2VldEFsZXJ0UmVzdWx0PiB7XG4gICAgbGV0IG1lbnNhamU6YW55PU9iamVjdC5hc3NpZ24oe30sdGhpcy5qc29uTWVuc2FqZXMuRVhJVE8pO1xuICAgIG1lbnNhamUudGV4dD1lbnRpZGFkTWVuc2FqZS5nZXRTaW5ndWxhckNvbkFydGljdWxvKCkrbWVuc2FqZS50ZXh0O1xuICAgIHJldHVybiBTd2FsLmZpcmUobWVuc2FqZSk7XG4gIH1cbiBnZXRNZW5zYWplcygpe1xucmV0dXJuIHtcbiAgXCJDQVJHQU5ET1wiOntcbiAgICAgIFwidGl0bGVcIjogXCJDYXJnYW5kby4uISEhXCIsXG4gICAgICBcIndpZHRoXCI6IDYwMCxcbiAgICAgIFwicGFkZGluZ1wiOiBcIjNlbVwiLFxuICAgICAgXCJiYWNrZ3JvdW5kXCI6IFwiI2ZmZlwiLFxuICAgICAgXCJiYWNrZHJvcFwiOiBcInJnYmEoMCwwLDAsMC43KVwiXG4gICAgfSxcbiAgICBcIkVYSVRPXCI6IHtcbiAgICAgIFwiaWNvblwiOiBcInN1Y2Nlc3NcIixcbiAgICAgIFwidGl0bGVcIjogXCJDYXJnYSBFeGl0b3NhXCIsXG4gICAgICBcInRleHRcIjogXCIgc2UgZ3VhcmRvIGNvcnJlY3RhbWVudGVcIlxuICAgIH0sXG4gICAgXCJFWElUT19FTElNSU5BUlwiOiB7XG4gICAgICBcImljb25cIjogXCJzdWNjZXNzXCIsXG4gICAgICBcInRpdGxlXCI6IFwiRWxpbWluYWNpw7NuIEV4aXRvc2FcIixcbiAgICAgIFwidGV4dFwiOiBcIiBzZSBoYSBlbGltaW5hZG8gY29ycmVjdGFtZW50ZVwiXG4gICAgfSxcbiAgICBcIkVSUk9SXCI6IHtcbiAgICAgIFwiaWNvblwiOiBcImVycm9yXCIsXG4gICAgICBcInRpdGxlXCI6IFwiRXJyb3JcIixcbiAgICAgIFwidGV4dFwiOiBcIkFsZ28gc2FsacOzIG1hbCFcIlxuICAgIH0sXG4gICAgXCJFTElNSU5BUlwiOnsgICAgXG4gICAgICAgICAgXCJ0aXRsZVwiOiBcIkVsaW1pbmFyXCIsXG4gICAgICAgICAgXCJ0ZXh0XCI6IFwiwr8gRXN0YSBzZWd1cm8gcXVlIGRlc2VhIGVsaW1pbmFyID9cIixcbiAgICAgICAgICBcImljb25cIjogXCJ3YXJuaW5nXCIsXG4gICAgICAgICAgXCJzaG93Q2FuY2VsQnV0dG9uXCI6IHRydWUsXG4gICAgICAgICAgXCJjb25maXJtQnV0dG9uQ29sb3JcIjogXCIjMzA4NWQ2XCIsXG4gICAgICAgICAgXCJjYW5jZWxCdXR0b25Db2xvclwiOiBcIiNkMzNcIixcbiAgICAgICAgICBcImNvbmZpcm1CdXR0b25UZXh0XCI6IFwiU2lcIiAgICAgIFxuICAgIH1cbiAgfVxuIH1cbiAgXG59XG4iXX0=