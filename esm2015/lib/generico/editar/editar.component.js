import { __decorate } from "tslib";
import { Component, OnInit, Injector, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormArray, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { map } from 'rxjs/operators';
import { GenericoService } from '../servicios/generico.service';
import { MensajesService, TipoMensaje } from '../mensajes/mensajes.service';
import { Configuracion } from '../configuracion';
import { EntidadMensaje } from '../mensajes/entidad-mensaje';
let EditarComponent = class EditarComponent {
    constructor(service, injector, rutaActiva) {
        this.service = service;
        this.injector = injector;
        this.rutaActiva = rutaActiva;
        this.cargando = 0;
        this.activedStep = 0;
        this.model = {};
        this.steps = [];
        this.form = new FormArray(this.steps.map(() => new FormGroup({})), Validators.required);
        this.options = this.steps.map(() => ({}));
        this.compareWith = function (o1, o2) {
            if (!o2 && !o1) {
                return true;
            }
            if (!o2) {
                return false;
            }
            return o1.id === o2.id;
        };
        this.servicioMensajes = this.injector.get(MensajesService);
        this.location = this.injector.get(Location);
    }
    ngOnChanges(changes) {
        console.log(changes);
    }
    ngOnInit() {
        this.setearModo();
        this.jsonEditar = this.service.json.camposEditar;
        this.jsonEditar.forEach(step => {
            step.fields.forEach(field => {
                if (this.modo == Configuracion.MODOS.VER) {
                    field.templateOptions.disabled = true;
                    this.form.disable({ onlySelf: false, emitEvent: true });
                }
                else {
                    field.templateOptions.disabled = false;
                    this.form.disable({ onlySelf: true, emitEvent: true });
                }
                if (field.type == 'select' && !field.templateOptions.multiple) {
                    // el nombre del key debe ser igual al del servicio inyectado         
                    this.procesarTipoSelect(field);
                }
                if (field.type == 'select' && field.templateOptions.multiple) {
                    // el nombre del key debe ser igual al del servicio inyectado         
                    this.procesarTipoSelectMultiple(field);
                }
                if (field.type == 'autocomplete') {
                    this.procesarTipoAutocomplete(field);
                }
            });
        });
        this.steps = this.jsonEditar;
        this.form = new FormArray(this.steps.map(() => new FormGroup({})), Validators.required);
        this.options = this.steps.map(() => ({}));
        if (this.modo == Configuracion.MODOS.VER) {
            this.form.disable({ onlySelf: false, emitEvent: false });
        }
    }
    procesarTipoSelectMultiple(field) {
        const service = this.injector.get(this.serviciosConfig.servicios[field.data.servicio.toLowerCase()].servicio);
        if (field.data.lista && field.data.lista == 'eager') {
            this.cargando++;
        }
        service.getAll().subscribe(r => {
            field.templateOptions.options = this.convertirOpciones(r, field.prop);
            field.templateOptions.compareWith = this.compareWith;
            if (field.data.lista && field.data.lista == 'eager') {
                this.cargando--;
            }
        });
    }
    procesarTipoAutocomplete(field) {
        field.templateOptions.compareWith = this.compareWith;
        field.validators = {
            ip: {
                expression: (c) => c.value.id,
                message: (error, field) => `"${field.formControl.value}", ingrese un valor existente.`,
            }
        };
        if (this.model[field.key]) {
            this.model[field.key].toString = function () { return this.model[field.key][field.data.prop]; };
        }
        field.templateOptions.filter = ((term) => this.getTerm(field.data.servicio.toLowerCase(), term, field.data.prop));
    }
    procesarTipoSelect(field) {
        const service = this.injector.get(this.serviciosConfig.servicios[field.data.servicio.toLowerCase()].servicio);
        if (field.data.lista && field.data.lista == 'eager') {
            this.cargando++;
        }
        service.getAll().subscribe(r => {
            field.templateOptions.options = this.convertirOpciones(r, field.prop);
            field.templateOptions.compareWith = this.compareWith;
            if (field.data.lista && field.data.lista == 'eager') {
                this.cargando--;
            }
        });
    }
    setearModo() {
        if (this.rutaActiva.snapshot.params.ver && this.rutaActiva.snapshot.params.ver == "ver") {
            this.modo = Configuracion.MODOS.VER;
            this.cargando++;
            this.modoEditar(this.rutaActiva.snapshot.params.id);
            return;
        }
        if (this.rutaActiva.snapshot.params.id) {
            this.modo = Configuracion.MODOS.EDITAR;
            this.cargando++;
            this.modoEditar(this.rutaActiva.snapshot.params.id);
            return;
        }
        this.modo = Configuracion.MODOS.NUEVO;
    }
    modoEditar(id) {
        this.service.get(id).subscribe(r => {
            this.model = this.convertirToString(r);
            this.cargando--;
        });
    }
    convertirToString(r) {
        this.jsonEditar.forEach(step => {
            step.fields.forEach(field => {
                if (field.type == 'autocomplete') {
                    r[field.key].toString = function () { return r[field.key][field.data.prop]; };
                }
            });
        });
        return r;
    }
    convertirOpciones(r, opcion) {
        const opciones = [];
        for (let i = 0; i < r.length; i++) {
            const element = r[i];
            opciones.push({ value: element, label: element[opcion] });
        }
        return opciones;
    }
    prevStep(step) {
        this.activedStep = step - 1;
    }
    nextStep(step) {
        this.activedStep = step + 1;
    }
    submit() {
        console.log(this.model);
        this.service.nuevo(this.model).subscribe(r => {
            this.servicioMensajes.mostrarMensaje(TipoMensaje.EXITO, new EntidadMensaje(this.service.json.mensaje));
            this.location.back();
        }, e => {
            console.log(e);
        });
    }
    getTexto(field) {
        let texto = this.model[field.key];
        if (!texto) {
            return "";
        }
        if (field.type == "select") {
            texto = texto[field.prop];
        }
        if (field.type == "autocomplete") {
            texto = texto[field.data.prop];
        }
        return texto;
    }
    getLabel(field) {
        return field.templateOptions.label;
    }
    /* getTerm(term: any) {
       return this.injector.get(ServiciosConfig.servicios.zona.servicio).getAll();
     }
   */
    getTerm(servicioString, term, arg) {
        return this.injector.get(this.serviciosConfig.servicios[servicioString].servicio).
            getPaginaServidorBusqueda(0, 20, [[arg, term]]).pipe(map(n => this.convertirRespuestaAutocomplete(n, arg)));
    }
    convertirRespuestaAutocomplete(array, arg) {
        let arrayConvert = [];
        console.log(array);
        for (let index = 0; index < array.content.length; index++) {
            let element = array.content[index];
            element.toString = function () { return element[arg]; };
            arrayConvert.push(element);
        }
        return arrayConvert;
    }
};
EditarComponent.ctorParameters = () => [
    { type: GenericoService },
    { type: Injector },
    { type: ActivatedRoute }
];
EditarComponent = __decorate([
    Component({
        selector: 'app-editar',
        template: "<mat-progress-bar mode=\"indeterminate\"   [hidden]=\"0==cargando\"></mat-progress-bar>\n<form [formGroup]=\"form\" (ngSubmit)=\"submit()\" [hidden]=\"cargando>0\">\n\n  <mat-horizontal-stepper>\n    \n    <mat-step *ngFor=\"let step of steps; let index = index; let last = last;\"   >\n      <mat-list *ngIf=\"modo==0\" class=\"row\">\n        <mat-list-item *ngFor=\"let field of step.fields\" >\n          <mat-form-field class=\"example-full-width\">\n            <mat-label>{{getLabel(field)}}</mat-label>\n            <input matInput readonly [value]=\"getTexto(field)\">\n          </mat-form-field>\n           </mat-list-item>\n        \n       </mat-list>\n      <ng-template matStepLabel>{{ step.label }}</ng-template>\n      <formly-form *ngIf=\"modo!=0\"\n        [form]=\"form.at(index)\"\n        [model]=\"model\"\n        [fields]=\"step.fields\"\n        [options]=\"options[index]\">\n      </formly-form>\n      \n      <div>\n        <button *ngIf=\"index !== 0\" matStepperPrevious class=\"btn btn-primary\" type=\"button\" (click)=\"prevStep(index)\"><i class=\"fa fa-angle-double-left\"></i></button>\n        <button *ngIf=\"!last && modo!=0\" matStepperNext class=\"btn btn-primary\" type=\"button\" [disabled]=\"!form.at(index).valid\" (click)=\"nextStep(index)\"><i class=\"fa fa-angle-double-right\"></i></button>\n        <button *ngIf=\"!last && modo==0\" matStepperNext class=\"btn btn-primary\" type=\"button\"  (click)=\"nextStep(index)\"><i class=\"fa fa-angle-double-right\"></i></button>\n        <button *ngIf=\"last\" class=\"btn btn-primary\" [disabled]=\"!form.valid\" type=\"submit\" style=\"margin-left: 1em;\">Aceptar</button>\n      </div>\n    </mat-step>\n  </mat-horizontal-stepper>\n</form>\n\n",
        styles: [".editar-item input:disabled{color:#000!important}"]
    })
], EditarComponent);
export { EditarComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWRpdGFyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2l2YWktZ2VuZXJpY28tbGlicmVyaWEvIiwic291cmNlcyI6WyJsaWIvZ2VuZXJpY28vZWRpdGFyL2VkaXRhci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRSxTQUFTLEVBQUUsYUFBYSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3RGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFFekQsT0FBTyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDbEUsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzNDLE9BQU8sRUFBRSxHQUFHLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQUNwQyxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFDaEUsT0FBTyxFQUFFLGVBQWUsRUFBRSxXQUFXLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUM1RSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFDakQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBWTdELElBQWEsZUFBZSxHQUE1QixNQUFhLGVBQWU7SUFHMUIsWUFBbUIsT0FBMkIsRUFBUyxRQUFrQixFQUFTLFVBQTBCO1FBQXpGLFlBQU8sR0FBUCxPQUFPLENBQW9CO1FBQVMsYUFBUSxHQUFSLFFBQVEsQ0FBVTtRQUFTLGVBQVUsR0FBVixVQUFVLENBQWdCO1FBTTVHLGFBQVEsR0FBRyxDQUFDLENBQUM7UUFFYixnQkFBVyxHQUFHLENBQUMsQ0FBQztRQUNoQixVQUFLLEdBQUcsRUFBRSxDQUFDO1FBR1gsVUFBSyxHQUFlLEVBQUUsQ0FBQztRQUV2QixTQUFJLEdBQUcsSUFBSSxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDbkYsWUFBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQW1CLEVBQUUsQ0FBQSxDQUFDLENBQUM7UUFpSnRELGdCQUFXLEdBQUcsVUFBVSxFQUFFLEVBQUUsRUFBRTtZQUM1QixJQUFJLENBQUMsRUFBRSxJQUFJLENBQUMsRUFBRSxFQUFFO2dCQUNkLE9BQU8sSUFBSSxDQUFDO2FBQ2I7WUFDRCxJQUFJLENBQUMsRUFBRSxFQUFFO2dCQUNQLE9BQU8sS0FBSyxDQUFDO2FBQ2Q7WUFDRCxPQUFPLEVBQUUsQ0FBQyxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FBQztRQUN6QixDQUFDLENBQUM7UUF0S0EsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFrQixlQUFlLENBQUMsQ0FBQztRQUM1RSxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFXLFFBQVEsQ0FBQyxDQUFDO0lBQ3hELENBQUM7SUFhRCxXQUFXLENBQUMsT0FBc0I7UUFDaEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN2QixDQUFDO0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUNsQixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQztRQUNqRCxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFDMUIsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLGFBQWEsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFO29CQUN4QyxLQUFLLENBQUMsZUFBZSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7b0JBQ3RDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztpQkFDekQ7cUJBQU07b0JBQ0wsS0FBSyxDQUFDLGVBQWUsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO29CQUN2QyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7aUJBQ3hEO2dCQUNELElBQUksS0FBSyxDQUFDLElBQUksSUFBSSxRQUFRLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLFFBQVEsRUFBRTtvQkFDN0Qsc0VBQXNFO29CQUN0RSxJQUFJLENBQUMsa0JBQWtCLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ2hDO2dCQUNELElBQUksS0FBSyxDQUFDLElBQUksSUFBSSxRQUFRLElBQUksS0FBSyxDQUFDLGVBQWUsQ0FBQyxRQUFRLEVBQUU7b0JBQzVELHNFQUFzRTtvQkFDdEUsSUFBSSxDQUFDLDBCQUEwQixDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUN4QztnQkFDRCxJQUFJLEtBQUssQ0FBQyxJQUFJLElBQUksY0FBYyxFQUFFO29CQUNoQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ3RDO1lBRUgsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUM3QixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3hGLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBbUIsRUFBRSxDQUFBLENBQUMsQ0FBQztRQUMzRCxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksYUFBYSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUU7WUFDeEMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO1NBQzFEO0lBRUgsQ0FBQztJQUdELDBCQUEwQixDQUFDLEtBQVU7UUFDbkMsTUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQU0sSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNuSCxJQUFJLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLE9BQU8sRUFBRTtZQUNuRCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDakI7UUFDRCxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQzdCLEtBQUssQ0FBQyxlQUFlLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3RFLEtBQUssQ0FBQyxlQUFlLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7WUFDckQsSUFBSSxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxPQUFPLEVBQUU7Z0JBQ25ELElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUNqQjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELHdCQUF3QixDQUFDLEtBQVU7UUFDakMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUNyRCxLQUFLLENBQUMsVUFBVSxHQUFFO1lBQ2hCLEVBQUUsRUFBRTtnQkFDRixVQUFVLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFDN0IsT0FBTyxFQUFFLENBQUMsS0FBSyxFQUFFLEtBQXdCLEVBQUUsRUFBRSxDQUFDLElBQUksS0FBSyxDQUFDLFdBQVcsQ0FBQyxLQUFLLGdDQUFnQzthQUMxRztTQUNGLENBQUM7UUFDRixJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQ3pCLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFFBQVEsR0FBRyxjQUFjLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQSxDQUFDLENBQUMsQ0FBQTtTQUMvRjtRQUNELEtBQUssQ0FBQyxlQUFlLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztJQUNwSCxDQUFDO0lBRUQsa0JBQWtCLENBQUMsS0FBVTtRQUMzQixNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBTSxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ25ILElBQUksS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksT0FBTyxFQUFFO1lBQ25ELElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztTQUNqQjtRQUNELE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDN0IsS0FBSyxDQUFDLGVBQWUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDdEUsS0FBSyxDQUFDLGVBQWUsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztZQUNyRCxJQUFJLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLE9BQU8sRUFBRTtnQkFDbkQsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ2pCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsVUFBVTtRQUNSLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsR0FBRyxJQUFJLEtBQUssRUFBRTtZQUN2RixJQUFJLENBQUMsSUFBSSxHQUFHLGFBQWEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO1lBQ3BDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUNoQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNwRCxPQUFPO1NBQ1I7UUFDRCxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUU7WUFDdEMsSUFBSSxDQUFDLElBQUksR0FBRyxhQUFhLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztZQUN2QyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDaEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDcEQsT0FBTztTQUNSO1FBQ0QsSUFBSSxDQUFDLElBQUksR0FBRyxhQUFhLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztJQUV4QyxDQUFDO0lBQ0QsVUFBVSxDQUFDLEVBQU87UUFDaEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQ2pDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRXZDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUVsQixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDRCxpQkFBaUIsQ0FBQyxDQUFNO1FBQ3RCLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUMxQixJQUFJLEtBQUssQ0FBQyxJQUFJLElBQUksY0FBYyxFQUFFO29CQUNoQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFFBQVEsR0FBRyxjQUFjLE9BQU8sQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBLENBQUMsQ0FBQyxDQUFBO2lCQUM3RTtZQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSCxPQUFPLENBQUMsQ0FBQztJQUNYLENBQUM7SUFDRCxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsTUFBTTtRQUN6QixNQUFNLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDcEIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDakMsTUFBTSxPQUFPLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3JCLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQzNEO1FBQ0QsT0FBTyxRQUFRLENBQUM7SUFDbEIsQ0FBQztJQUVELFFBQVEsQ0FBQyxJQUFJO1FBQ1gsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxDQUFDO0lBQzlCLENBQUM7SUFFRCxRQUFRLENBQUMsSUFBSTtRQUNYLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxHQUFHLENBQUMsQ0FBQztJQUM5QixDQUFDO0lBRUQsTUFBTTtRQUNKLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFNLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDaEQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLElBQUksY0FBYyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDdkcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUN2QixDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUU7WUFDTCxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2pCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQVlELFFBQVEsQ0FBQyxLQUFLO1FBQ1osSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbEMsSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNWLE9BQU8sRUFBRSxDQUFDO1NBQ1g7UUFDRCxJQUFJLEtBQUssQ0FBQyxJQUFJLElBQUksUUFBUSxFQUFFO1lBQzFCLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzNCO1FBQ0QsSUFBSSxLQUFLLENBQUMsSUFBSSxJQUFJLGNBQWMsRUFBRTtZQUNoQyxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDaEM7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNmLENBQUM7SUFFRCxRQUFRLENBQUMsS0FBSztRQUNaLE9BQU8sS0FBSyxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUM7SUFDckMsQ0FBQztJQUlEOzs7S0FHQztJQUNELE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxFQUFFLEdBQUc7UUFDL0IsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxRQUFRLENBQUM7WUFDL0UseUJBQXlCLENBQUMsQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLDhCQUE4QixDQUFDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDaEgsQ0FBQztJQUNELDhCQUE4QixDQUFDLEtBQUssRUFBRSxHQUFXO1FBQy9DLElBQUksWUFBWSxHQUFHLEVBQUUsQ0FBQztRQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ25CLEtBQUssSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFLEtBQUssR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsRUFBRTtZQUN6RCxJQUFJLE9BQU8sR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ25DLE9BQU8sQ0FBQyxRQUFRLEdBQUcsY0FBYyxPQUFPLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQSxDQUFDLENBQUMsQ0FBQztZQUN2RCxZQUFZLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQzVCO1FBQ0QsT0FBTyxZQUFZLENBQUM7SUFDdEIsQ0FBQztDQU9GLENBQUE7O1lBdE42QixlQUFlO1lBQXNCLFFBQVE7WUFBcUIsY0FBYzs7QUFIakcsZUFBZTtJQUwzQixTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsWUFBWTtRQUN0QiwydERBQW9DOztLQUVyQyxDQUFDO0dBQ1csZUFBZSxDQXlOM0I7U0F6TlksZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbmplY3RvciwgT25DaGFuZ2VzLCBTaW1wbGVDaGFuZ2VzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSwgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IEZvcm1seUZpZWxkQ29uZmlnLCBGb3JtbHlGb3JtT3B0aW9ucyB9IGZyb20gJ0BuZ3gtZm9ybWx5L2NvcmUnO1xuaW1wb3J0IHsgRm9ybUFycmF5LCBGb3JtR3JvdXAsIFZhbGlkYXRvcnMgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBMb2NhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBtYXB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IEdlbmVyaWNvU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2lvcy9nZW5lcmljby5zZXJ2aWNlJztcbmltcG9ydCB7IE1lbnNhamVzU2VydmljZSwgVGlwb01lbnNhamUgfSBmcm9tICcuLi9tZW5zYWplcy9tZW5zYWplcy5zZXJ2aWNlJztcbmltcG9ydCB7IENvbmZpZ3VyYWNpb24gfSBmcm9tICcuLi9jb25maWd1cmFjaW9uJztcbmltcG9ydCB7IEVudGlkYWRNZW5zYWplIH0gZnJvbSAnLi4vbWVuc2FqZXMvZW50aWRhZC1tZW5zYWplJztcblxuZXhwb3J0IGludGVyZmFjZSBTdGVwVHlwZSB7XG4gIGxhYmVsOiBzdHJpbmc7XG4gIGZpZWxkczogRm9ybWx5RmllbGRDb25maWdbXTtcbn1cblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYXBwLWVkaXRhcicsXG4gIHRlbXBsYXRlVXJsOiAnZWRpdGFyLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJ2VkaXRhci5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIEVkaXRhckNvbXBvbmVudDxUPiBpbXBsZW1lbnRzIE9uSW5pdCwgT25DaGFuZ2VzIHtcbiAgc2VydmljaW9zQ29uZmlnOiBhbnk7XG5cbiAgY29uc3RydWN0b3IocHVibGljIHNlcnZpY2U6IEdlbmVyaWNvU2VydmljZTxUPiwgcHVibGljIGluamVjdG9yOiBJbmplY3RvciwgcHVibGljIHJ1dGFBY3RpdmE6IEFjdGl2YXRlZFJvdXRlKSB7XG5cbiAgICB0aGlzLnNlcnZpY2lvTWVuc2FqZXMgPSB0aGlzLmluamVjdG9yLmdldDxNZW5zYWplc1NlcnZpY2U+KE1lbnNhamVzU2VydmljZSk7XG4gICAgdGhpcy5sb2NhdGlvbiA9IHRoaXMuaW5qZWN0b3IuZ2V0PExvY2F0aW9uPihMb2NhdGlvbik7XG4gIH1cbiAganNvbkVkaXRhcjtcbiAgY2FyZ2FuZG8gPSAwO1xuICBtb2RvOiBudW1iZXI7XG4gIGFjdGl2ZWRTdGVwID0gMDtcbiAgbW9kZWwgPSB7fTtcbiAgc2VydmljaW9NZW5zYWplczogTWVuc2FqZXNTZXJ2aWNlO1xuICBsb2NhdGlvbjogTG9jYXRpb247XG4gIHN0ZXBzOiBTdGVwVHlwZVtdID0gW107XG5cbiAgZm9ybSA9IG5ldyBGb3JtQXJyYXkodGhpcy5zdGVwcy5tYXAoKCkgPT4gbmV3IEZvcm1Hcm91cCh7fSkpLCBWYWxpZGF0b3JzLnJlcXVpcmVkKTtcbiAgb3B0aW9ucyA9IHRoaXMuc3RlcHMubWFwKCgpID0+IDxGb3JtbHlGb3JtT3B0aW9ucz57fSk7XG5cbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcyk6IHZvaWQge1xuICAgIGNvbnNvbGUubG9nKGNoYW5nZXMpO1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy5zZXRlYXJNb2RvKCk7XG4gICAgdGhpcy5qc29uRWRpdGFyID0gdGhpcy5zZXJ2aWNlLmpzb24uY2FtcG9zRWRpdGFyO1xuICAgIHRoaXMuanNvbkVkaXRhci5mb3JFYWNoKHN0ZXAgPT4ge1xuICAgICAgc3RlcC5maWVsZHMuZm9yRWFjaChmaWVsZCA9PiB7XG4gICAgICAgIGlmICh0aGlzLm1vZG8gPT0gQ29uZmlndXJhY2lvbi5NT0RPUy5WRVIpIHtcbiAgICAgICAgICBmaWVsZC50ZW1wbGF0ZU9wdGlvbnMuZGlzYWJsZWQgPSB0cnVlO1xuICAgICAgICAgIHRoaXMuZm9ybS5kaXNhYmxlKHsgb25seVNlbGY6IGZhbHNlLCBlbWl0RXZlbnQ6IHRydWUgfSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgZmllbGQudGVtcGxhdGVPcHRpb25zLmRpc2FibGVkID0gZmFsc2U7XG4gICAgICAgICAgdGhpcy5mb3JtLmRpc2FibGUoeyBvbmx5U2VsZjogdHJ1ZSwgZW1pdEV2ZW50OiB0cnVlIH0pO1xuICAgICAgICB9XG4gICAgICAgIGlmIChmaWVsZC50eXBlID09ICdzZWxlY3QnICYmICFmaWVsZC50ZW1wbGF0ZU9wdGlvbnMubXVsdGlwbGUpIHtcbiAgICAgICAgICAvLyBlbCBub21icmUgZGVsIGtleSBkZWJlIHNlciBpZ3VhbCBhbCBkZWwgc2VydmljaW8gaW55ZWN0YWRvICAgICAgICAgXG4gICAgICAgICAgdGhpcy5wcm9jZXNhclRpcG9TZWxlY3QoZmllbGQpO1xuICAgICAgICB9XG4gICAgICAgIGlmIChmaWVsZC50eXBlID09ICdzZWxlY3QnICYmIGZpZWxkLnRlbXBsYXRlT3B0aW9ucy5tdWx0aXBsZSkge1xuICAgICAgICAgIC8vIGVsIG5vbWJyZSBkZWwga2V5IGRlYmUgc2VyIGlndWFsIGFsIGRlbCBzZXJ2aWNpbyBpbnllY3RhZG8gICAgICAgICBcbiAgICAgICAgICB0aGlzLnByb2Nlc2FyVGlwb1NlbGVjdE11bHRpcGxlKGZpZWxkKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAoZmllbGQudHlwZSA9PSAnYXV0b2NvbXBsZXRlJykge1xuICAgICAgICAgIHRoaXMucHJvY2VzYXJUaXBvQXV0b2NvbXBsZXRlKGZpZWxkKTtcbiAgICAgICAgfVxuXG4gICAgICB9KTtcbiAgICB9KTtcbiAgICB0aGlzLnN0ZXBzID0gdGhpcy5qc29uRWRpdGFyO1xuICAgIHRoaXMuZm9ybSA9IG5ldyBGb3JtQXJyYXkodGhpcy5zdGVwcy5tYXAoKCkgPT4gbmV3IEZvcm1Hcm91cCh7fSkpLCBWYWxpZGF0b3JzLnJlcXVpcmVkKTtcbiAgICB0aGlzLm9wdGlvbnMgPSB0aGlzLnN0ZXBzLm1hcCgoKSA9PiA8Rm9ybWx5Rm9ybU9wdGlvbnM+e30pO1xuICAgIGlmICh0aGlzLm1vZG8gPT0gQ29uZmlndXJhY2lvbi5NT0RPUy5WRVIpIHtcbiAgICAgIHRoaXMuZm9ybS5kaXNhYmxlKHsgb25seVNlbGY6IGZhbHNlLCBlbWl0RXZlbnQ6IGZhbHNlIH0pO1xuICAgIH1cblxuICB9XG5cblxuICBwcm9jZXNhclRpcG9TZWxlY3RNdWx0aXBsZShmaWVsZDogYW55KSB7XG4gICAgY29uc3Qgc2VydmljZSA9IHRoaXMuaW5qZWN0b3IuZ2V0PGFueT4odGhpcy5zZXJ2aWNpb3NDb25maWcuc2VydmljaW9zW2ZpZWxkLmRhdGEuc2VydmljaW8udG9Mb3dlckNhc2UoKV0uc2VydmljaW8pO1xuICAgIGlmIChmaWVsZC5kYXRhLmxpc3RhICYmIGZpZWxkLmRhdGEubGlzdGEgPT0gJ2VhZ2VyJykge1xuICAgICAgdGhpcy5jYXJnYW5kbysrO1xuICAgIH1cbiAgICBzZXJ2aWNlLmdldEFsbCgpLnN1YnNjcmliZShyID0+IHtcbiAgICAgIGZpZWxkLnRlbXBsYXRlT3B0aW9ucy5vcHRpb25zID0gdGhpcy5jb252ZXJ0aXJPcGNpb25lcyhyLCBmaWVsZC5wcm9wKTtcbiAgICAgIGZpZWxkLnRlbXBsYXRlT3B0aW9ucy5jb21wYXJlV2l0aCA9IHRoaXMuY29tcGFyZVdpdGg7XG4gICAgICBpZiAoZmllbGQuZGF0YS5saXN0YSAmJiBmaWVsZC5kYXRhLmxpc3RhID09ICdlYWdlcicpIHtcbiAgICAgICAgdGhpcy5jYXJnYW5kby0tO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgcHJvY2VzYXJUaXBvQXV0b2NvbXBsZXRlKGZpZWxkOiBhbnkpIHtcbiAgICBmaWVsZC50ZW1wbGF0ZU9wdGlvbnMuY29tcGFyZVdpdGggPSB0aGlzLmNvbXBhcmVXaXRoO1xuICAgIGZpZWxkLnZhbGlkYXRvcnM9IHtcbiAgICAgIGlwOiB7XG4gICAgICAgIGV4cHJlc3Npb246IChjKSA9PiBjLnZhbHVlLmlkLFxuICAgICAgICBtZXNzYWdlOiAoZXJyb3IsIGZpZWxkOiBGb3JtbHlGaWVsZENvbmZpZykgPT4gYFwiJHtmaWVsZC5mb3JtQ29udHJvbC52YWx1ZX1cIiwgaW5ncmVzZSB1biB2YWxvciBleGlzdGVudGUuYCxcbiAgICAgIH1cbiAgICB9O1xuICAgIGlmICh0aGlzLm1vZGVsW2ZpZWxkLmtleV0pIHtcbiAgICAgIHRoaXMubW9kZWxbZmllbGQua2V5XS50b1N0cmluZyA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIHRoaXMubW9kZWxbZmllbGQua2V5XVtmaWVsZC5kYXRhLnByb3BdIH1cbiAgICB9XG4gICAgZmllbGQudGVtcGxhdGVPcHRpb25zLmZpbHRlciA9ICgodGVybSkgPT4gdGhpcy5nZXRUZXJtKGZpZWxkLmRhdGEuc2VydmljaW8udG9Mb3dlckNhc2UoKSwgdGVybSwgZmllbGQuZGF0YS5wcm9wKSk7XG4gIH1cblxuICBwcm9jZXNhclRpcG9TZWxlY3QoZmllbGQ6IGFueSkge1xuICAgIGNvbnN0IHNlcnZpY2UgPSB0aGlzLmluamVjdG9yLmdldDxhbnk+KHRoaXMuc2VydmljaW9zQ29uZmlnLnNlcnZpY2lvc1tmaWVsZC5kYXRhLnNlcnZpY2lvLnRvTG93ZXJDYXNlKCldLnNlcnZpY2lvKTtcbiAgICBpZiAoZmllbGQuZGF0YS5saXN0YSAmJiBmaWVsZC5kYXRhLmxpc3RhID09ICdlYWdlcicpIHtcbiAgICAgIHRoaXMuY2FyZ2FuZG8rKztcbiAgICB9XG4gICAgc2VydmljZS5nZXRBbGwoKS5zdWJzY3JpYmUociA9PiB7XG4gICAgICBmaWVsZC50ZW1wbGF0ZU9wdGlvbnMub3B0aW9ucyA9IHRoaXMuY29udmVydGlyT3BjaW9uZXMociwgZmllbGQucHJvcCk7XG4gICAgICBmaWVsZC50ZW1wbGF0ZU9wdGlvbnMuY29tcGFyZVdpdGggPSB0aGlzLmNvbXBhcmVXaXRoO1xuICAgICAgaWYgKGZpZWxkLmRhdGEubGlzdGEgJiYgZmllbGQuZGF0YS5saXN0YSA9PSAnZWFnZXInKSB7XG4gICAgICAgIHRoaXMuY2FyZ2FuZG8tLTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIHNldGVhck1vZG8oKSB7XG4gICAgaWYgKHRoaXMucnV0YUFjdGl2YS5zbmFwc2hvdC5wYXJhbXMudmVyICYmIHRoaXMucnV0YUFjdGl2YS5zbmFwc2hvdC5wYXJhbXMudmVyID09IFwidmVyXCIpIHtcbiAgICAgIHRoaXMubW9kbyA9IENvbmZpZ3VyYWNpb24uTU9ET1MuVkVSO1xuICAgICAgdGhpcy5jYXJnYW5kbysrO1xuICAgICAgdGhpcy5tb2RvRWRpdGFyKHRoaXMucnV0YUFjdGl2YS5zbmFwc2hvdC5wYXJhbXMuaWQpO1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBpZiAodGhpcy5ydXRhQWN0aXZhLnNuYXBzaG90LnBhcmFtcy5pZCkge1xuICAgICAgdGhpcy5tb2RvID0gQ29uZmlndXJhY2lvbi5NT0RPUy5FRElUQVI7XG4gICAgICB0aGlzLmNhcmdhbmRvKys7XG4gICAgICB0aGlzLm1vZG9FZGl0YXIodGhpcy5ydXRhQWN0aXZhLnNuYXBzaG90LnBhcmFtcy5pZCk7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIHRoaXMubW9kbyA9IENvbmZpZ3VyYWNpb24uTU9ET1MuTlVFVk87XG5cbiAgfVxuICBtb2RvRWRpdGFyKGlkOiBhbnkpIHtcbiAgICB0aGlzLnNlcnZpY2UuZ2V0KGlkKS5zdWJzY3JpYmUociA9PiB7XG4gICAgICB0aGlzLm1vZGVsID0gdGhpcy5jb252ZXJ0aXJUb1N0cmluZyhyKTtcblxuICAgICAgdGhpcy5jYXJnYW5kby0tO1xuXG4gICAgfSk7XG4gIH1cbiAgY29udmVydGlyVG9TdHJpbmcocjogYW55KSB7XG4gICAgdGhpcy5qc29uRWRpdGFyLmZvckVhY2goc3RlcCA9PiB7XG4gICAgICBzdGVwLmZpZWxkcy5mb3JFYWNoKGZpZWxkID0+IHtcbiAgICAgICAgaWYgKGZpZWxkLnR5cGUgPT0gJ2F1dG9jb21wbGV0ZScpIHtcbiAgICAgICAgICByW2ZpZWxkLmtleV0udG9TdHJpbmcgPSBmdW5jdGlvbiAoKSB7IHJldHVybiByW2ZpZWxkLmtleV1bZmllbGQuZGF0YS5wcm9wXSB9XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH0pO1xuICAgIHJldHVybiByO1xuICB9XG4gIGNvbnZlcnRpck9wY2lvbmVzKHIsIG9wY2lvbik6IGFueSB7XG4gICAgY29uc3Qgb3BjaW9uZXMgPSBbXTtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHIubGVuZ3RoOyBpKyspIHtcbiAgICAgIGNvbnN0IGVsZW1lbnQgPSByW2ldO1xuICAgICAgb3BjaW9uZXMucHVzaCh7IHZhbHVlOiBlbGVtZW50LCBsYWJlbDogZWxlbWVudFtvcGNpb25dIH0pO1xuICAgIH1cbiAgICByZXR1cm4gb3BjaW9uZXM7XG4gIH1cblxuICBwcmV2U3RlcChzdGVwKSB7XG4gICAgdGhpcy5hY3RpdmVkU3RlcCA9IHN0ZXAgLSAxO1xuICB9XG5cbiAgbmV4dFN0ZXAoc3RlcCkge1xuICAgIHRoaXMuYWN0aXZlZFN0ZXAgPSBzdGVwICsgMTtcbiAgfVxuXG4gIHN1Ym1pdCgpIHtcbiAgICBjb25zb2xlLmxvZyh0aGlzLm1vZGVsKTtcbiAgICB0aGlzLnNlcnZpY2UubnVldm8oPGFueT50aGlzLm1vZGVsKS5zdWJzY3JpYmUociA9PiB7XG4gICAgICB0aGlzLnNlcnZpY2lvTWVuc2FqZXMubW9zdHJhck1lbnNhamUoVGlwb01lbnNhamUuRVhJVE8sIG5ldyBFbnRpZGFkTWVuc2FqZSh0aGlzLnNlcnZpY2UuanNvbi5tZW5zYWplKSk7XG4gICAgICB0aGlzLmxvY2F0aW9uLmJhY2soKTtcbiAgICB9LCBlID0+IHtcbiAgICAgIGNvbnNvbGUubG9nKGUpO1xuICAgIH0pO1xuICB9XG5cbiAgY29tcGFyZVdpdGggPSBmdW5jdGlvbiAobzEsIG8yKSB7XG4gICAgaWYgKCFvMiAmJiAhbzEpIHtcbiAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cbiAgICBpZiAoIW8yKSB7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIHJldHVybiBvMS5pZCA9PT0gbzIuaWQ7XG4gIH07XG5cbiAgZ2V0VGV4dG8oZmllbGQpIHtcbiAgICBsZXQgdGV4dG8gPSB0aGlzLm1vZGVsW2ZpZWxkLmtleV07XG4gICAgaWYgKCF0ZXh0bykge1xuICAgICAgcmV0dXJuIFwiXCI7XG4gICAgfVxuICAgIGlmIChmaWVsZC50eXBlID09IFwic2VsZWN0XCIpIHtcbiAgICAgIHRleHRvID0gdGV4dG9bZmllbGQucHJvcF07XG4gICAgfVxuICAgIGlmIChmaWVsZC50eXBlID09IFwiYXV0b2NvbXBsZXRlXCIpIHtcbiAgICAgIHRleHRvID0gdGV4dG9bZmllbGQuZGF0YS5wcm9wXTtcbiAgICB9XG4gICAgcmV0dXJuIHRleHRvO1xuICB9XG5cbiAgZ2V0TGFiZWwoZmllbGQpIHtcbiAgICByZXR1cm4gZmllbGQudGVtcGxhdGVPcHRpb25zLmxhYmVsO1xuICB9XG5cblxuXG4gIC8qIGdldFRlcm0odGVybTogYW55KSB7XG4gICAgIHJldHVybiB0aGlzLmluamVjdG9yLmdldChTZXJ2aWNpb3NDb25maWcuc2VydmljaW9zLnpvbmEuc2VydmljaW8pLmdldEFsbCgpO1xuICAgfVxuICovXG4gIGdldFRlcm0oc2VydmljaW9TdHJpbmcsIHRlcm0sIGFyZykge1xuICAgIHJldHVybiB0aGlzLmluamVjdG9yLmdldCh0aGlzLnNlcnZpY2lvc0NvbmZpZy5zZXJ2aWNpb3Nbc2VydmljaW9TdHJpbmddLnNlcnZpY2lvKS5cbiAgICAgIGdldFBhZ2luYVNlcnZpZG9yQnVzcXVlZGEoMCwgMjAsIFtbYXJnLCB0ZXJtXV0pLnBpcGUobWFwKG4gPT4gdGhpcy5jb252ZXJ0aXJSZXNwdWVzdGFBdXRvY29tcGxldGUobiwgYXJnKSkpO1xuICB9XG4gIGNvbnZlcnRpclJlc3B1ZXN0YUF1dG9jb21wbGV0ZShhcnJheSwgYXJnOiBzdHJpbmcpOiBhbnkge1xuICAgIGxldCBhcnJheUNvbnZlcnQgPSBbXTtcbiAgICBjb25zb2xlLmxvZyhhcnJheSk7XG4gICAgZm9yIChsZXQgaW5kZXggPSAwOyBpbmRleCA8IGFycmF5LmNvbnRlbnQubGVuZ3RoOyBpbmRleCsrKSB7XG4gICAgICBsZXQgZWxlbWVudCA9IGFycmF5LmNvbnRlbnRbaW5kZXhdO1xuICAgICAgZWxlbWVudC50b1N0cmluZyA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIGVsZW1lbnRbYXJnXSB9O1xuICAgICAgYXJyYXlDb252ZXJ0LnB1c2goZWxlbWVudCk7XG4gICAgfVxuICAgIHJldHVybiBhcnJheUNvbnZlcnQ7XG4gIH1cblxuXG5cblxuXG5cbn1cbiJdfQ==