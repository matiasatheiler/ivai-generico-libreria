import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
let GenericoService = class GenericoService {
    constructor(http) {
        this.http = http;
        this.size = 10;
        this.urlObjeto = '';
        this.json = {};
    }
    get(id) {
        return this.http.get(this.getUrl() + "/" + id);
    }
    getAll() {
        return this.http.get(this.getUrl());
    }
    getPagina() {
        return this.getPaginaServidor(0, this.size);
    }
    getPaginaServidor(pageIndex, pageSize) {
        const params = new HttpParams()
            .set('page', pageIndex.toString())
            .set('pagination', 'true')
            .set('size', pageSize.toString())
            .set('sort', 'id,asc');
        return this.http.get(this.getUrl(), { params });
    }
    getPaginaServidorBusqueda(pageIndex, pageSize, parametros) {
        let params = new HttpParams()
            .set('page', pageIndex.toString())
            .set('pagination', 'true')
            .set('size', pageSize.toString())
            .set('sort', 'id,asc');
        for (let i = 0; i < parametros.length; i++) {
            const p = parametros[i];
            params = params.append(p[0], p[1]);
        }
        return this.http.get(this.getUrl(), { params });
    }
    getUrl() {
        return this.url + this.urlObjeto;
    }
    getCampos() {
        return this.json;
    }
    nuevo(objeto) {
        return this.http.post(this.getUrl(), objeto);
    }
    delete(id) {
        return this.http.delete(this.getUrl() + "/" + id);
    }
};
GenericoService.ctorParameters = () => [
    { type: HttpClient }
];
GenericoService.ɵprov = i0.ɵɵdefineInjectable({ factory: function GenericoService_Factory() { return new GenericoService(i0.ɵɵinject(i1.HttpClient)); }, token: GenericoService, providedIn: "root" });
GenericoService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], GenericoService);
export { GenericoService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2VuZXJpY28uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2l2YWktZ2VuZXJpY28tbGlicmVyaWEvIiwic291cmNlcyI6WyJsaWIvZ2VuZXJpY28vc2VydmljaW9zL2dlbmVyaWNvLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQzs7O0FBVzlELElBQWEsZUFBZSxHQUE1QixNQUFhLGVBQWU7SUFRMUIsWUFBbUIsSUFBZ0I7UUFBaEIsU0FBSSxHQUFKLElBQUksQ0FBWTtRQUxuQyxTQUFJLEdBQUcsRUFBRSxDQUFDO1FBRVYsY0FBUyxHQUFHLEVBQUUsQ0FBQztRQUNmLFNBQUksR0FBSyxFQUFFLENBQUM7SUFFMkIsQ0FBQztJQUV4QyxHQUFHLENBQUMsRUFBRTtRQUNKLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFDLEdBQUcsR0FBQyxFQUFFLENBQUMsQ0FBQztJQUM3QyxDQUFDO0lBRUQsTUFBTTtRQUNKLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQUVELFNBQVM7UUFDUixPQUFPLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxTQUFpQixFQUFFLFFBQWdCO1FBQ25ELE1BQU0sTUFBTSxHQUFHLElBQUksVUFBVSxFQUFFO2FBQ2hDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsU0FBUyxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ2pDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsTUFBTSxDQUFDO2FBQ3pCLEdBQUcsQ0FBQyxNQUFNLEVBQUUsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFFO2FBQ2pDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDckIsT0FBK0IsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxFQUFFLEVBQUMsTUFBTSxFQUFDLENBQUMsQ0FBQztJQUN4RSxDQUFDO0lBRUQseUJBQXlCLENBQUMsU0FBaUIsRUFBRSxRQUFnQixFQUFFLFVBQVU7UUFDdkUsSUFBSSxNQUFNLEdBQUcsSUFBSSxVQUFVLEVBQUU7YUFDOUIsR0FBRyxDQUFDLE1BQU0sRUFBRSxTQUFTLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDakMsR0FBRyxDQUFDLFlBQVksRUFBRSxNQUFNLENBQUM7YUFDekIsR0FBRyxDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUU7YUFDakMsR0FBRyxDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUMsQ0FBQztRQUN2QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsVUFBVSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUMxQyxNQUFNLENBQUMsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDeEIsTUFBTSxHQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ2pDO1FBQ0MsT0FBK0IsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxFQUFFLEVBQUMsTUFBTSxFQUFDLENBQUMsQ0FBQztJQUN4RSxDQUFDO0lBQ0QsTUFBTTtRQUNKLE9BQU8sSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQ25DLENBQUM7SUFFRCxTQUFTO1FBQ1AsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDO0lBQ25CLENBQUM7SUFFRCxLQUFLLENBQUMsTUFBUTtRQUNaLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxFQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFFRCxNQUFNLENBQUMsRUFBTztRQUNaLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFDLEdBQUcsR0FBQyxFQUFFLENBQUMsQ0FBQztJQUNoRCxDQUFDO0NBRUYsQ0FBQTs7WUFuRDBCLFVBQVU7OztBQVJ4QixlQUFlO0lBSDNCLFVBQVUsQ0FBQztRQUNWLFVBQVUsRUFBRSxNQUFNO0tBQ25CLENBQUM7R0FDVyxlQUFlLENBMkQzQjtTQTNEWSxlQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cENsaWVudCwgSHR0cFBhcmFtcyB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IENvbmZpZ3VyYWNpb24gfSBmcm9tICcuLi9jb25maWd1cmFjaW9uJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcblxuaW1wb3J0IHsgU2VydmljaW8gfSBmcm9tICcuL3NlcnZpY2lvJztcbmltcG9ydCB7IFBhZ2luYSB9IGZyb20gJy4uL3BhZ2luYWNpb24vcGFnaW5hJztcblxuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBHZW5lcmljb1NlcnZpY2U8VD4gaW1wbGVtZW50cyBTZXJ2aWNpbzxUPiB7XG4gIFxuIFxuICBzaXplID0gMTA7XG4gIHVybDogc3RyaW5nO1xuICB1cmxPYmpldG8gPSAnJztcbiAganNvbjphbnk9e307XG5cbiAgY29uc3RydWN0b3IocHVibGljIGh0dHA6IEh0dHBDbGllbnQpIHsgfVxuXG4gIGdldChpZCk6T2JzZXJ2YWJsZTxhbnk+e1xuICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0KHRoaXMuZ2V0VXJsKCkrXCIvXCIraWQpO1xuICB9XG5cbiAgZ2V0QWxsKCl7XG4gICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQodGhpcy5nZXRVcmwoKSk7XG4gIH1cblxuICBnZXRQYWdpbmEoKTogT2JzZXJ2YWJsZTxQYWdpbmE8VD4+IHtcbiAgIHJldHVybiB0aGlzLmdldFBhZ2luYVNlcnZpZG9yKDAsdGhpcy5zaXplKTtcbiAgfVxuXG4gIGdldFBhZ2luYVNlcnZpZG9yKHBhZ2VJbmRleDogbnVtYmVyLCBwYWdlU2l6ZTogbnVtYmVyKTogT2JzZXJ2YWJsZTxQYWdpbmE8VD4+IHtcbiAgICBjb25zdCBwYXJhbXMgPSBuZXcgSHR0cFBhcmFtcygpXG4gIC5zZXQoJ3BhZ2UnLCBwYWdlSW5kZXgudG9TdHJpbmcoKSlcbiAgLnNldCgncGFnaW5hdGlvbicsICd0cnVlJylcbiAgLnNldCgnc2l6ZScsIHBhZ2VTaXplLnRvU3RyaW5nKCkgKVxuICAuc2V0KCdzb3J0JywgJ2lkLGFzYycpO1xuICAgIHJldHVybiA8T2JzZXJ2YWJsZTxQYWdpbmE8VD4+PiB0aGlzLmh0dHAuZ2V0KHRoaXMuZ2V0VXJsKCksIHtwYXJhbXN9KTtcbiAgfVxuXG4gIGdldFBhZ2luYVNlcnZpZG9yQnVzcXVlZGEocGFnZUluZGV4OiBudW1iZXIsIHBhZ2VTaXplOiBudW1iZXIsIHBhcmFtZXRyb3MpIHtcbiAgICBsZXQgcGFyYW1zID0gbmV3IEh0dHBQYXJhbXMoKVxuICAuc2V0KCdwYWdlJywgcGFnZUluZGV4LnRvU3RyaW5nKCkpXG4gIC5zZXQoJ3BhZ2luYXRpb24nLCAndHJ1ZScpXG4gIC5zZXQoJ3NpemUnLCBwYWdlU2l6ZS50b1N0cmluZygpIClcbiAgLnNldCgnc29ydCcsICdpZCxhc2MnKTtcbiAgZm9yIChsZXQgaSA9IDA7IGkgPCBwYXJhbWV0cm9zLmxlbmd0aDsgaSsrKSB7XG4gICAgY29uc3QgcCA9IHBhcmFtZXRyb3NbaV07XG4gICAgcGFyYW1zPXBhcmFtcy5hcHBlbmQocFswXSxwWzFdKTsgICAgXG4gIH1cbiAgICByZXR1cm4gPE9ic2VydmFibGU8UGFnaW5hPFQ+Pj4gdGhpcy5odHRwLmdldCh0aGlzLmdldFVybCgpLCB7cGFyYW1zfSk7XG4gIH1cbiAgZ2V0VXJsKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMudXJsICsgdGhpcy51cmxPYmpldG87XG4gIH1cblxuICBnZXRDYW1wb3MoKTphbnl7XG4gICAgcmV0dXJuIHRoaXMuanNvbjtcbiAgfVxuXG4gIG51ZXZvKG9iamV0bzpUKXtcbiAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QodGhpcy5nZXRVcmwoKSxvYmpldG8pO1xuICB9XG5cbiAgZGVsZXRlKGlkOiBhbnkpIHtcbiAgICByZXR1cm4gdGhpcy5odHRwLmRlbGV0ZSh0aGlzLmdldFVybCgpK1wiL1wiK2lkKTtcbiAgfVxuXG59XG4iXX0=