import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
let BusquedaComponent = class BusquedaComponent {
    constructor() {
        this.texto = "";
        this.habilitado = true;
    }
    ngOnInit() {
    }
    limpiar() {
        this.texto = "";
        this.buscar(this.texto);
        this.habilitado = true;
    }
    buscar(texto) {
        this.habilitado = false;
        this.buscador.buscar(texto);
    }
};
__decorate([
    Input()
], BusquedaComponent.prototype, "buscador", void 0);
__decorate([
    Input()
], BusquedaComponent.prototype, "texto", void 0);
BusquedaComponent = __decorate([
    Component({
        selector: 'app-busqueda',
        template: "<div class=\"input-group mb-3\">  \n  <input [disabled]=\"!habilitado\" [(ngModel)]=\"texto\" type=\"text\" class=\"form-control\" placeholder=\"Buscar...\" aria-label=\"Realizar b\u00FAsqueda\" aria-describedby=\"basic-addon2\">\n  <div class=\"input-group-append\" *ngIf=\"!habilitado\">\n    <button  (click)=\"limpiar()\" class=\"btn btn-outline-danger\" type=\"button\"><i class=\"fa fa-times-circle\"></i></button>\n  </div>\n  <div class=\"input-group-append\"  *ngIf=\"habilitado\">\n    <button  (click)=\"buscar(texto)\" class=\"btn btn-outline-primary\" type=\"button\"><i class=\"fa fa-search\"></i></button>\n  </div>\n</div> ",
        styles: [""]
    })
], BusquedaComponent);
export { BusquedaComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnVzcXVlZGEuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaXZhaS1nZW5lcmljby1saWJyZXJpYS8iLCJzb3VyY2VzIjpbImxpYi9nZW5lcmljby9mb3JtbHlzL2J1c3F1ZWRhL2J1c3F1ZWRhLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFRekQsSUFBYSxpQkFBaUIsR0FBOUIsTUFBYSxpQkFBaUI7SUFNNUI7UUFGQSxVQUFLLEdBQVEsRUFBRSxDQUFDO1FBQ2hCLGVBQVUsR0FBQyxJQUFJLENBQUM7SUFDQSxDQUFDO0lBRWpCLFFBQVE7SUFDUixDQUFDO0lBRUQsT0FBTztRQUNMLElBQUksQ0FBQyxLQUFLLEdBQUMsRUFBRSxDQUFDO1FBQ2QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBQyxJQUFJLENBQUM7SUFDdkIsQ0FBQztJQUVELE1BQU0sQ0FBQyxLQUFLO1FBQ1YsSUFBSSxDQUFDLFVBQVUsR0FBQyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDOUIsQ0FBQztDQUVGLENBQUE7QUFwQkM7SUFEQyxLQUFLLEVBQUU7bURBQ1U7QUFFbEI7SUFEQyxLQUFLLEVBQUU7Z0RBQ1E7QUFKTCxpQkFBaUI7SUFMN0IsU0FBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLGNBQWM7UUFDeEIsMm9CQUF3Qzs7S0FFekMsQ0FBQztHQUNXLGlCQUFpQixDQXNCN0I7U0F0QlksaUJBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBCdXNjYWRvciB9IGZyb20gJy4vYnVzY2Fkb3InO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhcHAtYnVzcXVlZGEnLFxuICB0ZW1wbGF0ZVVybDogJy4vYnVzcXVlZGEuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9idXNxdWVkYS5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIEJ1c3F1ZWRhQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHsgIFxuICBASW5wdXQoKVxuICBidXNjYWRvcjpCdXNjYWRvcjsgXG4gIEBJbnB1dCgpXG4gIHRleHRvOnN0cmluZz1cIlwiO1xuICBoYWJpbGl0YWRvPXRydWU7XG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuICBsaW1waWFyKCl7XG4gICAgdGhpcy50ZXh0bz1cIlwiO1xuICAgIHRoaXMuYnVzY2FyKHRoaXMudGV4dG8pO1xuICAgIHRoaXMuaGFiaWxpdGFkbz10cnVlO1xuICB9XG5cbiAgYnVzY2FyKHRleHRvKXtcbiAgICB0aGlzLmhhYmlsaXRhZG89ZmFsc2U7XG4gICAgdGhpcy5idXNjYWRvci5idXNjYXIodGV4dG8pOyAgICBcbiAgfVxuXG59XG4iXX0=