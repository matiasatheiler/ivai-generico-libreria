import { __decorate } from "tslib";
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { ElementoTablaComponent } from '../elemento-tabla/elemento-tabla.component';
import { BotonIconoEstilo } from './boton-icono-estilo';
import { Configuracion } from '../../configuracion';
let BotonIconoComponent = class BotonIconoComponent extends ElementoTablaComponent {
    constructor(cd) {
        super(cd);
        this.cd = cd;
    }
    ngOnInit() {
    }
    getClase() {
        if (this.col.estilo != null) {
            return Configuracion.obtenerClaseIcono(this.col.estilo);
        }
        else {
            return new BotonIconoEstilo(this.col.icon, this.col.clase);
        }
    }
    click() {
        if (this.col.estilo) {
            this.ejecutarFuncionGenerico();
        }
        else {
            this.ejecutarFuncion(this.elemento);
        }
    }
    ejecutarFuncion(item) {
        throw new Error("Method not implemented.");
    }
    ejecutarFuncionGenerico() {
        switch (this.col.estilo) {
            case "editar":
                this.observador.editar(this.elemento.id);
                break;
            case "eliminar":
                this.observador.eliminar(this.elemento.id);
                break;
            case "ver":
                this.observador.ver(this.elemento.id);
                break;
            default:
                console.log("no esta implementado el metodo " + this.col.estilo);
                break;
        }
    }
};
BotonIconoComponent.ctorParameters = () => [
    { type: ChangeDetectorRef }
];
BotonIconoComponent = __decorate([
    Component({
        selector: 'app-boton-icono',
        template: "<button type=\"button\" (click)=\"click()\" [ngClass]=\"[getClase().clase]\"><i [ngClass]=\"[getClase().icono]\"></i></button>",
        styles: [""]
    })
], BotonIconoComponent);
export { BotonIconoComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm90b24taWNvbm8uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaXZhaS1nZW5lcmljby1saWJyZXJpYS8iLCJzb3VyY2VzIjpbImxpYi9nZW5lcmljby9mb3JtbHlzL2JvdG9uLWljb25vL2JvdG9uLWljb25vLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsaUJBQWlCLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzVFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQ3BGLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBRXhELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQU9wRCxJQUFhLG1CQUFtQixHQUFoQyxNQUFhLG1CQUFvQixTQUFRLHNCQUFzQjtJQUU3RCxZQUFtQixFQUFvQjtRQUNyQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7UUFETyxPQUFFLEdBQUYsRUFBRSxDQUFrQjtJQUV2QyxDQUFDO0lBRUQsUUFBUTtJQUNSLENBQUM7SUFFRCxRQUFRO1FBQ04sSUFBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sSUFBRSxJQUFJLEVBQUM7WUFDdkIsT0FBTyxhQUFhLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUN6RDthQUFJO1lBQ0gsT0FBTyxJQUFJLGdCQUFnQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDM0Q7SUFDSCxDQUFDO0lBRUQsS0FBSztRQUNILElBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUM7WUFDakIsSUFBSSxDQUFDLHVCQUF1QixFQUFFLENBQUM7U0FDaEM7YUFBSTtZQUNILElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQ3JDO0lBQ0gsQ0FBQztJQUVELGVBQWUsQ0FBQyxJQUFTO1FBQ3ZCLE1BQU0sSUFBSSxLQUFLLENBQUMseUJBQXlCLENBQUMsQ0FBQztJQUM3QyxDQUFDO0lBRUQsdUJBQXVCO1FBQ25CLFFBQVEsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUU7WUFDckIsS0FBSyxRQUFRO2dCQUNULElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFPLElBQUksQ0FBQyxRQUFTLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ2hELE1BQU07WUFDVixLQUFLLFVBQVU7Z0JBQ2IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQU8sSUFBSSxDQUFDLFFBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDbEQsTUFBTTtZQUNSLEtBQUssS0FBSztnQkFDUixJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBTyxJQUFJLENBQUMsUUFBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUM1QyxNQUFNO1lBQ1Q7Z0JBQ0ksT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQ0FBaUMsR0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUMvRCxNQUFNO1NBQ2pCO0lBQ0QsQ0FBQztDQUVGLENBQUE7O1lBNUN1QixpQkFBaUI7O0FBRjVCLG1CQUFtQjtJQUwvQixTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsaUJBQWlCO1FBQzNCLDBJQUEyQzs7S0FFNUMsQ0FBQztHQUNXLG1CQUFtQixDQThDL0I7U0E5Q1ksbUJBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIENoYW5nZURldGVjdG9yUmVmLCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRWxlbWVudG9UYWJsYUNvbXBvbmVudCB9IGZyb20gJy4uL2VsZW1lbnRvLXRhYmxhL2VsZW1lbnRvLXRhYmxhLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBCb3Rvbkljb25vRXN0aWxvIH0gZnJvbSAnLi9ib3Rvbi1pY29uby1lc3RpbG8nO1xuaW1wb3J0IHsgQWNjaW9uZXNHZW5lcmFsZXMgfSBmcm9tICcuLi9hY2Npb25lcy9hY2Npb25lcy1nZW5lcmFsZXMnO1xuaW1wb3J0IHsgQ29uZmlndXJhY2lvbiB9IGZyb20gJy4uLy4uL2NvbmZpZ3VyYWNpb24nO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhcHAtYm90b24taWNvbm8nLFxuICB0ZW1wbGF0ZVVybDogJy4vYm90b24taWNvbm8uY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9ib3Rvbi1pY29uby5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIEJvdG9uSWNvbm9Db21wb25lbnQgZXh0ZW5kcyBFbGVtZW50b1RhYmxhQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgXG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBjZDpDaGFuZ2VEZXRlY3RvclJlZikge1xuICAgIHN1cGVyKGNkKTtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cbiAgZ2V0Q2xhc2UoKXtcbiAgICBpZih0aGlzLmNvbC5lc3RpbG8hPW51bGwpe1xuICAgICAgcmV0dXJuIENvbmZpZ3VyYWNpb24ub2J0ZW5lckNsYXNlSWNvbm8odGhpcy5jb2wuZXN0aWxvKTtcbiAgICB9ZWxzZXtcbiAgICAgIHJldHVybiBuZXcgQm90b25JY29ub0VzdGlsbyh0aGlzLmNvbC5pY29uLHRoaXMuY29sLmNsYXNlKTtcbiAgICB9XG4gIH1cblxuICBjbGljaygpe1xuICAgIGlmKHRoaXMuY29sLmVzdGlsbyl7XG4gICAgICB0aGlzLmVqZWN1dGFyRnVuY2lvbkdlbmVyaWNvKCk7XG4gICAgfWVsc2V7XG4gICAgICB0aGlzLmVqZWN1dGFyRnVuY2lvbih0aGlzLmVsZW1lbnRvKTtcbiAgICB9XG4gIH1cblxuICBlamVjdXRhckZ1bmNpb24oaXRlbTogYW55KSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKFwiTWV0aG9kIG5vdCBpbXBsZW1lbnRlZC5cIik7XG4gIH1cbiAgXG4gIGVqZWN1dGFyRnVuY2lvbkdlbmVyaWNvKCkgeyAgICBcbiAgICAgIHN3aXRjaCAodGhpcy5jb2wuZXN0aWxvKSB7XG4gICAgICAgICAgY2FzZSBcImVkaXRhclwiOlxuICAgICAgICAgICAgICB0aGlzLm9ic2VydmFkb3IuZWRpdGFyKCg8YW55PnRoaXMuZWxlbWVudG8pLmlkKTtcbiAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgY2FzZSBcImVsaW1pbmFyXCI6XG4gICAgICAgICAgICB0aGlzLm9ic2VydmFkb3IuZWxpbWluYXIoKDxhbnk+dGhpcy5lbGVtZW50bykuaWQpO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgY2FzZSBcInZlclwiOlxuICAgICAgICAgICAgdGhpcy5vYnNlcnZhZG9yLnZlcigoPGFueT50aGlzLmVsZW1lbnRvKS5pZCk7XG4gICAgICAgICAgICAgYnJlYWs7ICAgICAgICAgIFxuICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwibm8gZXN0YSBpbXBsZW1lbnRhZG8gZWwgbWV0b2RvIFwiK3RoaXMuY29sLmVzdGlsbyk7XG4gICAgICAgICAgICAgIGJyZWFrO1xuICB9XG4gIH1cblxufVxuIl19