import { __decorate } from "tslib";
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
let ElementoTablaComponent = class ElementoTablaComponent {
    constructor(cd) {
        this.cd = cd;
        this.eBoolean = false;
        this._col = { prop: "dummy.", clase: "false", icon: null, tipo: null, estilo: null };
        this._elemento = { dummy: null };
    }
    ngOnInit() {
    }
    get elemento() {
        return this._elemento;
    }
    set elemento(value) {
        this._elemento = value;
        this.setPropiedadChange();
        this.cd.detectChanges();
    }
    get col() {
        return this._col;
    }
    set col(value) {
        this._col = value;
        this.cd.detectChanges();
    }
    setPropiedadChange() {
        if (!this.col.prop) {
            return;
        }
        const props = this.col.prop.split(".");
        var e = this.elemento;
        for (let i = 0; i < props.length; i++) {
            const p = props[i];
            e = e[p];
            if (!e) {
                this.propiedad = "";
                return;
            }
        }
        this.propiedad = e;
    }
};
ElementoTablaComponent.ctorParameters = () => [
    { type: ChangeDetectorRef }
];
ElementoTablaComponent = __decorate([
    Component({
        selector: 'app-elemento-tabla',
        template: "<p>\n  elemento-tabla works!\n</p>\n",
        styles: [""]
    })
], ElementoTablaComponent);
export { ElementoTablaComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWxlbWVudG8tdGFibGEuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaXZhaS1nZW5lcmljby1saWJyZXJpYS8iLCJzb3VyY2VzIjpbImxpYi9nZW5lcmljby9mb3JtbHlzL2VsZW1lbnRvLXRhYmxhL2VsZW1lbnRvLXRhYmxhLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFRckUsSUFBYSxzQkFBc0IsR0FBbkMsTUFBYSxzQkFBc0I7SUFJakMsWUFBbUIsRUFBb0I7UUFBcEIsT0FBRSxHQUFGLEVBQUUsQ0FBa0I7UUFGdkMsYUFBUSxHQUFDLEtBQUssQ0FBQztRQVNQLFNBQUksR0FBRyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFDLE9BQU8sRUFBQyxJQUFJLEVBQUMsSUFBSSxFQUFDLElBQUksRUFBQyxJQUFJLEVBQUMsTUFBTSxFQUFDLElBQUksRUFBRSxDQUFDO1FBRXpFLGNBQVMsR0FBRyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsQ0FBQztJQVJwQyxDQUFDO0lBRUQsUUFBUTtJQUVSLENBQUM7SUFNRCxJQUFXLFFBQVE7UUFDakIsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQ3hCLENBQUM7SUFDRCxJQUFXLFFBQVEsQ0FBQyxLQUFLO1FBQ3ZCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDMUIsQ0FBQztJQUVELElBQVcsR0FBRztRQUNaLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQztJQUNuQixDQUFDO0lBQ0QsSUFBVyxHQUFHLENBQUMsS0FBSztRQUNsQixJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQztRQUNsQixJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQzFCLENBQUM7SUFFRCxrQkFBa0I7UUFDaEIsSUFBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFDO1lBQUMsT0FBTztTQUFDO1FBQzNCLE1BQU0sS0FBSyxHQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNyQyxJQUFJLENBQUMsR0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQ3BCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3JDLE1BQU0sQ0FBQyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNuQixDQUFDLEdBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ1AsSUFBRyxDQUFDLENBQUMsRUFBQztnQkFDTixJQUFJLENBQUMsU0FBUyxHQUFDLEVBQUUsQ0FBQztnQkFDbEIsT0FBTzthQUFDO1NBQ1Q7UUFDRCxJQUFJLENBQUMsU0FBUyxHQUFFLENBQUMsQ0FBQztJQUNwQixDQUFDO0NBRUYsQ0FBQTs7WUExQ3VCLGlCQUFpQjs7QUFKNUIsc0JBQXNCO0lBTGxDLFNBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSxvQkFBb0I7UUFDOUIsZ0RBQThDOztLQUUvQyxDQUFDO0dBQ1csc0JBQXNCLENBOENsQztTQTlDWSxzQkFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgQ2hhbmdlRGV0ZWN0b3JSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEFjY2lvbmVzR2VuZXJhbGVzIH0gZnJvbSAnLi4vYWNjaW9uZXMvYWNjaW9uZXMtZ2VuZXJhbGVzJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYXBwLWVsZW1lbnRvLXRhYmxhJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2VsZW1lbnRvLXRhYmxhLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vZWxlbWVudG8tdGFibGEuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBFbGVtZW50b1RhYmxhQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgXG4gIGVCb29sZWFuPWZhbHNlO1xuICBwcm9waWVkYWQ7XG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBjZDpDaGFuZ2VEZXRlY3RvclJlZikgeyAgICBcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgXG4gIH1cbiAgb2JzZXJ2YWRvcjpBY2Npb25lc0dlbmVyYWxlcztcbiAgcHJpdmF0ZSBfY29sID0geyBwcm9wOiBcImR1bW15LlwiICxjbGFzZTpcImZhbHNlXCIsaWNvbjpudWxsLHRpcG86bnVsbCxlc3RpbG86bnVsbCB9OyAgXG4gIFxuICBwcml2YXRlIF9lbGVtZW50byA9IHsgZHVtbXk6IG51bGwgfTtcblxuICBwdWJsaWMgZ2V0IGVsZW1lbnRvKCkge1xuICAgIHJldHVybiB0aGlzLl9lbGVtZW50bztcbiAgfVxuICBwdWJsaWMgc2V0IGVsZW1lbnRvKHZhbHVlKSB7ICAgXG4gICAgdGhpcy5fZWxlbWVudG8gPSB2YWx1ZTtcbiAgICB0aGlzLnNldFByb3BpZWRhZENoYW5nZSgpO1xuICAgIHRoaXMuY2QuZGV0ZWN0Q2hhbmdlcygpOyAgXG4gIH1cblxuICBwdWJsaWMgZ2V0IGNvbCgpIHtcbiAgICByZXR1cm4gdGhpcy5fY29sO1xuICB9XG4gIHB1YmxpYyBzZXQgY29sKHZhbHVlKSB7ICAgXG4gICAgdGhpcy5fY29sID0gdmFsdWU7XG4gICAgdGhpcy5jZC5kZXRlY3RDaGFuZ2VzKCk7XG4gIH0gXG5cbiAgc2V0UHJvcGllZGFkQ2hhbmdlKCkge1xuICAgIGlmKCF0aGlzLmNvbC5wcm9wKXtyZXR1cm47fVxuICAgIGNvbnN0IHByb3BzPXRoaXMuY29sLnByb3Auc3BsaXQoXCIuXCIpO1xuICAgIHZhciBlPXRoaXMuZWxlbWVudG87XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykge1xuICAgICAgY29uc3QgcCA9IHByb3BzW2ldO1xuICAgICAgZT1lW3BdO1xuICAgICAgaWYoIWUpe1xuICAgICAgdGhpcy5wcm9waWVkYWQ9XCJcIjtcbiAgICAgIHJldHVybjt9XG4gICAgfVxuICAgIHRoaXMucHJvcGllZGFkPSBlO1xuICB9XG5cbn1cbiJdfQ==