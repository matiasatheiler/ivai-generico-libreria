import { __decorate } from "tslib";
import { Component, OnInit, ViewChild, ViewContainerRef, Type, Input, AfterViewInit, ComponentFactoryResolver } from '@angular/core';
import { TextoComponent } from '../texto/texto.component';
import { BotonComponent } from '../boton/boton.component';
import { BotonIconoComponent } from '../boton-icono/boton-icono.component';
let FieldGenericoComponent = class FieldGenericoComponent {
    constructor(cfr) {
        this.cfr = cfr;
        this.col = { prop: '' };
    }
    ngOnInit() {
    }
    ngAfterViewInit() {
        let compFactory = this.cfr.resolveComponentFactory(this.getComponent(this.col.tipo));
        let ref = this.viewContainerRef.createComponent(compFactory);
        ref.instance.col = this.col;
        ref.instance.elemento = this.elemento;
        ref.instance.observador = this.observador;
    }
    getComponent(prop) {
        switch (prop) {
            case 'texto':
                return TextoComponent;
                break;
            case 'boton':
                return BotonComponent;
                break;
            case 'boton-icono':
                return BotonIconoComponent;
                break;
            default:
                return TextoComponent;
                break;
        }
    }
};
FieldGenericoComponent.ctorParameters = () => [
    { type: ComponentFactoryResolver }
];
__decorate([
    Input()
], FieldGenericoComponent.prototype, "col", void 0);
__decorate([
    Input()
], FieldGenericoComponent.prototype, "elemento", void 0);
__decorate([
    Input()
], FieldGenericoComponent.prototype, "observador", void 0);
__decorate([
    ViewChild('dynamic', { read: ViewContainerRef })
], FieldGenericoComponent.prototype, "viewContainerRef", void 0);
FieldGenericoComponent = __decorate([
    Component({
        selector: 'app-field-generico',
        template: " <ng-template #dynamic></ng-template>\n",
        styles: [""]
    })
], FieldGenericoComponent);
export { FieldGenericoComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGQtZ2VuZXJpY28uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaXZhaS1nZW5lcmljby1saWJyZXJpYS8iLCJzb3VyY2VzIjpbImxpYi9nZW5lcmljby9mb3JtbHlzL2ZpZWxkLWdlbmVyaWNvL2ZpZWxkLWdlbmVyaWNvLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLGdCQUFnQixFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsYUFBYSxFQUFFLHdCQUF3QixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRXJJLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUUxRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDMUQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFRM0UsSUFBYSxzQkFBc0IsR0FBbkMsTUFBYSxzQkFBc0I7SUFRakMsWUFBb0IsR0FBNkI7UUFBN0IsUUFBRyxHQUFILEdBQUcsQ0FBMEI7UUFQeEMsUUFBRyxHQUFRLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxDQUFDO0lBUWpDLENBQUM7SUFDRCxRQUFRO0lBQ1IsQ0FBQztJQUVELGVBQWU7UUFDYixJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQ3JGLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDN0QsR0FBRyxDQUFDLFFBQVEsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQztRQUM1QixHQUFHLENBQUMsUUFBUSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQ3RDLEdBQUcsQ0FBQyxRQUFRLENBQUMsVUFBVSxHQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDMUMsQ0FBQztJQUVELFlBQVksQ0FBQyxJQUFZO1FBQ3ZCLFFBQVEsSUFBSSxFQUFFO1lBQ1osS0FBSyxPQUFPO2dCQUNWLE9BQU8sY0FBYyxDQUFDO2dCQUN0QixNQUFNO1lBQ1IsS0FBSyxPQUFPO2dCQUNWLE9BQU8sY0FBYyxDQUFDO2dCQUN0QixNQUFNO1lBQ04sS0FBSyxhQUFhO2dCQUNsQixPQUFPLG1CQUFtQixDQUFDO2dCQUMzQixNQUFNO1lBQ1I7Z0JBQ0UsT0FBTyxjQUFjLENBQUM7Z0JBQ3RCLE1BQU07U0FDVDtJQUNILENBQUM7Q0FLRixDQUFBOztZQWpDMEIsd0JBQXdCOztBQVB4QztJQUFSLEtBQUssRUFBRTttREFBeUI7QUFDeEI7SUFBUixLQUFLLEVBQUU7d0RBQWU7QUFDZDtJQUFSLEtBQUssRUFBRTswREFBOEI7QUFHdEM7SUFEQyxTQUFTLENBQUMsU0FBUyxFQUFFLEVBQUMsSUFBSSxFQUFFLGdCQUFnQixFQUFDLENBQUM7Z0VBQ1o7QUFOeEIsc0JBQXNCO0lBTGxDLFNBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSxvQkFBb0I7UUFDOUIsbURBQThDOztLQUUvQyxDQUFDO0dBQ1csc0JBQXNCLENBeUNsQztTQXpDWSxzQkFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0NoaWxkLCBWaWV3Q29udGFpbmVyUmVmLCBUeXBlLCBJbnB1dCwgQWZ0ZXJWaWV3SW5pdCwgQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IFRleHRvQ29tcG9uZW50IH0gZnJvbSAnLi4vdGV4dG8vdGV4dG8uY29tcG9uZW50JztcbmltcG9ydCB7IEVsZW1lbnRvVGFibGFDb21wb25lbnQgfSBmcm9tICcuLi9lbGVtZW50by10YWJsYS9lbGVtZW50by10YWJsYS5jb21wb25lbnQnO1xuaW1wb3J0IHsgQm90b25Db21wb25lbnQgfSBmcm9tICcuLi9ib3Rvbi9ib3Rvbi5jb21wb25lbnQnO1xuaW1wb3J0IHsgQm90b25JY29ub0NvbXBvbmVudCB9IGZyb20gJy4uL2JvdG9uLWljb25vL2JvdG9uLWljb25vLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBBY2Npb25lc0dlbmVyYWxlcyB9IGZyb20gJy4uL2FjY2lvbmVzL2FjY2lvbmVzLWdlbmVyYWxlcyc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2FwcC1maWVsZC1nZW5lcmljbycsXG4gIHRlbXBsYXRlVXJsOiAnLi9maWVsZC1nZW5lcmljby5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2ZpZWxkLWdlbmVyaWNvLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgRmllbGRHZW5lcmljb0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCB7XG4gIEBJbnB1dCgpIGNvbDogYW55ID0geyBwcm9wOiAnJyB9O1xuICBASW5wdXQoKSBlbGVtZW50bzogYW55O1xuICBASW5wdXQoKSBvYnNlcnZhZG9yOkFjY2lvbmVzR2VuZXJhbGVzO1xuXG4gIEBWaWV3Q2hpbGQoJ2R5bmFtaWMnLCB7cmVhZDogVmlld0NvbnRhaW5lclJlZn0pIFxuICB2aWV3Q29udGFpbmVyUmVmOiBWaWV3Q29udGFpbmVyUmVmO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgY2ZyOiBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIpIHtcbiAgfVxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpOiB2b2lkIHtcbiAgICBsZXQgY29tcEZhY3RvcnkgPSB0aGlzLmNmci5yZXNvbHZlQ29tcG9uZW50RmFjdG9yeSh0aGlzLmdldENvbXBvbmVudCh0aGlzLmNvbC50aXBvKSk7XG4gICAgbGV0IHJlZiA9IHRoaXMudmlld0NvbnRhaW5lclJlZi5jcmVhdGVDb21wb25lbnQoY29tcEZhY3RvcnkpO1xuICAgIHJlZi5pbnN0YW5jZS5jb2wgPSB0aGlzLmNvbDtcbiAgICByZWYuaW5zdGFuY2UuZWxlbWVudG8gPSB0aGlzLmVsZW1lbnRvO1xuICAgIHJlZi5pbnN0YW5jZS5vYnNlcnZhZG9yPXRoaXMub2JzZXJ2YWRvcjtcbiAgfVxuXG4gIGdldENvbXBvbmVudChwcm9wOiBzdHJpbmcpOiBUeXBlPEVsZW1lbnRvVGFibGFDb21wb25lbnQ+IHtcbiAgICBzd2l0Y2ggKHByb3ApIHtcbiAgICAgIGNhc2UgJ3RleHRvJzpcbiAgICAgICAgcmV0dXJuIFRleHRvQ29tcG9uZW50O1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ2JvdG9uJzpcbiAgICAgICAgcmV0dXJuIEJvdG9uQ29tcG9uZW50O1xuICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSAnYm90b24taWNvbm8nOlxuICAgICAgICByZXR1cm4gQm90b25JY29ub0NvbXBvbmVudDtcbiAgICAgICAgYnJlYWs7XG4gICAgICBkZWZhdWx0OlxuICAgICAgICByZXR1cm4gVGV4dG9Db21wb25lbnQ7XG4gICAgICAgIGJyZWFrO1xuICAgIH1cbiAgfVxuXG5cblxuXG59XG4iXX0=