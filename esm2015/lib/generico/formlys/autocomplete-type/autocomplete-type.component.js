import { __decorate } from "tslib";
import { Component, ViewChild } from '@angular/core';
import { FieldType } from '@ngx-formly/material';
import { MatInput } from '@angular/material/input';
import { MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { startWith, switchMap } from 'rxjs/operators';
let AutocompleteTypeComponent = class AutocompleteTypeComponent extends FieldType {
    ngOnInit() {
        super.ngOnInit();
        this.filter = this.formControl.valueChanges
            .pipe(startWith(''), switchMap(term => this.to.filter(term)));
    }
    ngAfterViewInit() {
        super.ngAfterViewInit();
        // temporary fix for https://github.com/angular/material2/issues/6728
        this.autocomplete._formField = this.formField;
    }
    borrar() {
        this.value = "";
    }
    seleccionar(value) {
    }
};
__decorate([
    ViewChild(MatInput)
], AutocompleteTypeComponent.prototype, "formFieldControl", void 0);
__decorate([
    ViewChild(MatAutocompleteTrigger)
], AutocompleteTypeComponent.prototype, "autocomplete", void 0);
AutocompleteTypeComponent = __decorate([
    Component({
        selector: 'formly-autocomplete-type',
        template: "  <div class=\"row\">\n    \n    \n     <input class=\"col\" style=\"padding-left: 1rem;\" matInput \n      [matAutocomplete]=\"auto\"\n      [formControl]=\"formControl\"\n      [formlyAttributes]=\"field\"\n      [placeholder]=\"to.placeholder\"\n      [errorStateMatcher]=\"errorStateMatcher\"\n      >\n      <div class=\"col-1\" style=\"color: red;\">\n        <button  style=\"padding-left: 0px;\" class=\"btn btn-link \" *ngIf=\"value\" aria-label=\"Clear\" (click)=\"borrar()\">\n          <i class=\"fa fa-trash\"></i>\n         </button>\n      </div>\n      <mat-autocomplete  #auto=\"matAutocomplete\">\n      \n        <mat-option  (click)=\"seleccionar(value)\" *ngFor=\"let value of filter | async\" [value]=\"value\">\n          {{ value }}\n        </mat-option>      \n      </mat-autocomplete>\n  </div>\n  \n\n \n  \n  \n    \n"
    })
], AutocompleteTypeComponent);
export { AutocompleteTypeComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0b2NvbXBsZXRlLXR5cGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaXZhaS1nZW5lcmljby1saWJyZXJpYS8iLCJzb3VyY2VzIjpbImxpYi9nZW5lcmljby9mb3JtbHlzL2F1dG9jb21wbGV0ZS10eXBlL2F1dG9jb21wbGV0ZS10eXBlLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQXlCLE1BQU0sZUFBZSxDQUFDO0FBQzVFLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDbkQsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFFeEUsT0FBTyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQU10RCxJQUFhLHlCQUF5QixHQUF0QyxNQUFhLHlCQUEwQixTQUFRLFNBQVM7SUFNdEQsUUFBUTtRQUNOLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNqQixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWTthQUN4QyxJQUFJLENBQ0gsU0FBUyxDQUFDLEVBQUUsQ0FBQyxFQUNiLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQ3hDLENBQUM7SUFDTixDQUFDO0lBRUQsZUFBZTtRQUNiLEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN4QixxRUFBcUU7UUFDOUQsSUFBSSxDQUFDLFlBQWEsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUV4RCxDQUFDO0lBSUEsTUFBTTtRQUVKLElBQUksQ0FBQyxLQUFLLEdBQUMsRUFBRSxDQUFDO0lBRWhCLENBQUM7SUFFRCxXQUFXLENBQUMsS0FBSztJQUVqQixDQUFDO0NBSUgsQ0FBQTtBQW5Dc0I7SUFBcEIsU0FBUyxDQUFDLFFBQVEsQ0FBQzttRUFBNEI7QUFDYjtJQUFsQyxTQUFTLENBQUMsc0JBQXNCLENBQUM7K0RBQXNDO0FBRjdELHlCQUF5QjtJQUpyQyxTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsMEJBQTBCO1FBQ3BDLDIxQkFBOEM7S0FDL0MsQ0FBQztHQUNXLHlCQUF5QixDQW9DckM7U0FwQ1kseUJBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBWaWV3Q2hpbGQsIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRmllbGRUeXBlIH0gZnJvbSAnQG5neC1mb3JtbHkvbWF0ZXJpYWwnO1xuaW1wb3J0IHsgTWF0SW5wdXQgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9pbnB1dCc7XG5pbXBvcnQgeyBNYXRBdXRvY29tcGxldGVUcmlnZ2VyIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvYXV0b2NvbXBsZXRlJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IHN0YXJ0V2l0aCwgc3dpdGNoTWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdmb3JtbHktYXV0b2NvbXBsZXRlLXR5cGUnLFxuICB0ZW1wbGF0ZVVybDonYXV0b2NvbXBsZXRlLXR5cGUuY29tcG9uZW50Lmh0bWwnXG59KVxuZXhwb3J0IGNsYXNzIEF1dG9jb21wbGV0ZVR5cGVDb21wb25lbnQgZXh0ZW5kcyBGaWVsZFR5cGUgaW1wbGVtZW50cyBPbkluaXQsIEFmdGVyVmlld0luaXQge1xuICBAVmlld0NoaWxkKE1hdElucHV0KSBmb3JtRmllbGRDb250cm9sOiBNYXRJbnB1dDtcbiAgQFZpZXdDaGlsZChNYXRBdXRvY29tcGxldGVUcmlnZ2VyKSBhdXRvY29tcGxldGU6IE1hdEF1dG9jb21wbGV0ZVRyaWdnZXI7XG5cbiAgZmlsdGVyOiBPYnNlcnZhYmxlPGFueT47XG4gIFxuICBuZ09uSW5pdCgpIHtcbiAgICBzdXBlci5uZ09uSW5pdCgpO1xuICAgIHRoaXMuZmlsdGVyID0gdGhpcy5mb3JtQ29udHJvbC52YWx1ZUNoYW5nZXNcbiAgICAgIC5waXBlKFxuICAgICAgICBzdGFydFdpdGgoJycpLFxuICAgICAgICBzd2l0Y2hNYXAodGVybSA9PiB0aGlzLnRvLmZpbHRlcih0ZXJtKSksXG4gICAgICApO1xuICB9XG5cbiAgbmdBZnRlclZpZXdJbml0KCkge1xuICAgIHN1cGVyLm5nQWZ0ZXJWaWV3SW5pdCgpO1xuICAgIC8vIHRlbXBvcmFyeSBmaXggZm9yIGh0dHBzOi8vZ2l0aHViLmNvbS9hbmd1bGFyL21hdGVyaWFsMi9pc3N1ZXMvNjcyOFxuICAgICg8YW55PiB0aGlzLmF1dG9jb21wbGV0ZSkuX2Zvcm1GaWVsZCA9IHRoaXMuZm9ybUZpZWxkO1xuICAgIFxuICB9XG5cbiAgXG5cbiAgIGJvcnJhcigpe1xuICAgIFxuICAgICB0aGlzLnZhbHVlPVwiXCI7XG4gICAgIFxuICAgfVxuXG4gICBzZWxlY2Npb25hcih2YWx1ZSl7XG4gICAgIFxuICAgfVxuXG4gICBcbiAgXG59XG4iXX0=