import { __decorate } from "tslib";
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ElementoTablaComponent } from '../elemento-tabla/elemento-tabla.component';
let BotonComponent = class BotonComponent extends ElementoTablaComponent {
    constructor(cd) {
        super(cd);
        this.cd = cd;
    }
    ngOnInit() {
    }
};
BotonComponent.ctorParameters = () => [
    { type: ChangeDetectorRef }
];
BotonComponent = __decorate([
    Component({
        selector: 'app-boton',
        template: "<button type=\"button\" [ngClass]=\"[col.clase]\">{{elemento[col.prop]}}</button>",
        styles: [""]
    })
], BotonComponent);
export { BotonComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm90b24uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaXZhaS1nZW5lcmljby1saWJyZXJpYS8iLCJzb3VyY2VzIjpbImxpYi9nZW5lcmljby9mb3JtbHlzL2JvdG9uL2JvdG9uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDckUsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFPcEYsSUFBYSxjQUFjLEdBQTNCLE1BQWEsY0FBZSxTQUFRLHNCQUFzQjtJQUV4RCxZQUFtQixFQUFvQjtRQUNyQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7UUFETyxPQUFFLEdBQUYsRUFBRSxDQUFrQjtJQUV2QyxDQUFDO0lBRUQsUUFBUTtJQUNSLENBQUM7Q0FFRixDQUFBOztZQVB1QixpQkFBaUI7O0FBRjVCLGNBQWM7SUFMMUIsU0FBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLFdBQVc7UUFDckIsNkZBQXFDOztLQUV0QyxDQUFDO0dBQ1csY0FBYyxDQVMxQjtTQVRZLGNBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgQ2hhbmdlRGV0ZWN0b3JSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEVsZW1lbnRvVGFibGFDb21wb25lbnQgfSBmcm9tICcuLi9lbGVtZW50by10YWJsYS9lbGVtZW50by10YWJsYS5jb21wb25lbnQnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhcHAtYm90b24nLFxuICB0ZW1wbGF0ZVVybDogJy4vYm90b24uY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9ib3Rvbi5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIEJvdG9uQ29tcG9uZW50IGV4dGVuZHMgRWxlbWVudG9UYWJsYUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgY29uc3RydWN0b3IocHVibGljIGNkOkNoYW5nZURldGVjdG9yUmVmKSB7XG4gICAgc3VwZXIoY2QpO1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxufVxuIl19