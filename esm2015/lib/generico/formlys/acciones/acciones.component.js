import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
let AccionesComponent = class AccionesComponent {
    constructor() { }
    ngOnInit() {
    }
    nuevo() {
        this.componente.nuevo();
    }
};
__decorate([
    Input()
], AccionesComponent.prototype, "componente", void 0);
AccionesComponent = __decorate([
    Component({
        selector: 'app-acciones',
        template: "<div class=\"input-group mb-3\">  \n  <button  (click)=\"nuevo()\" class=\"btn btn-outline-success\" type=\"button\">\n    <i class=\"fa fa-plus-circle\"></i>\n    <span> Nuevo</span>\n  </button>\n</div>\n",
        styles: [""]
    })
], AccionesComponent);
export { AccionesComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjaW9uZXMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaXZhaS1nZW5lcmljby1saWJyZXJpYS8iLCJzb3VyY2VzIjpbImxpYi9nZW5lcmljby9mb3JtbHlzL2FjY2lvbmVzL2FjY2lvbmVzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFRekQsSUFBYSxpQkFBaUIsR0FBOUIsTUFBYSxpQkFBaUI7SUFHNUIsZ0JBQWdCLENBQUM7SUFFakIsUUFBUTtJQUNSLENBQUM7SUFFRCxLQUFLO1FBQ0gsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUMxQixDQUFDO0NBQ0YsQ0FBQTtBQVRDO0lBREMsS0FBSyxFQUFFO3FEQUNxQjtBQUZsQixpQkFBaUI7SUFMN0IsU0FBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLGNBQWM7UUFDeEIsME5BQXdDOztLQUV6QyxDQUFDO0dBQ1csaUJBQWlCLENBVzdCO1NBWFksaUJBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBBY2Npb25lc0dlbmVyYWxlcyB9IGZyb20gJy4vYWNjaW9uZXMtZ2VuZXJhbGVzJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYXBwLWFjY2lvbmVzJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2FjY2lvbmVzLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vYWNjaW9uZXMuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBBY2Npb25lc0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIEBJbnB1dCgpXG4gIGNvbXBvbmVudGU6QWNjaW9uZXNHZW5lcmFsZXM7IFxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cbiAgbnVldm8oKXtcbiAgICB0aGlzLmNvbXBvbmVudGUubnVldm8oKTtcbiAgfVxufVxuIl19