import { __decorate } from "tslib";
import { Component, OnInit, ViewChild, AfterViewInit, TemplateRef, Injector, EventEmitter, ChangeDetectorRef, InjectionToken } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ModalTablaComponent } from './modal-tabla/modal-tabla.component';
import { Router } from '@angular/router';
import { BusquedaComponent } from '../formlys/busqueda/busqueda.component';
import { MensajesService, TipoMensaje } from '../mensajes/mensajes.service';
import { GenericoService } from '../servicios/generico.service';
import { LogisoftStorage } from '../storage/logisoft-storage';
import { EntidadMensaje } from '../mensajes/entidad-mensaje';
let TablaComponent = class TablaComponent {
    constructor(injector, service, modalService, router) {
        this.injector = injector;
        this.service = service;
        this.modalService = modalService;
        this.router = router;
        this.cargando = true;
        this.data = [];
        this.titulo = '';
        this.columnas = [];
        this.acciones = [];
        this.opcionesPaginacion = {
            pageSizeOptions: [5, 10, 25, 100],
            length: 0,
            pageSize: 10
        };
        this.textoBuscar = null;
        this.columnas = this.service.getCampos().camposListado;
        this.acciones = this.service.getCampos().accionesGenerales;
        this.inicioConstructor();
        this.servicioMensajes = this.injector.get(MensajesService);
        this.cd = this.injector.get(ChangeDetectorRef);
    }
    inicioConstructor() {
        if (this.verificarPantallaAtras()) {
            return;
        }
        this.service.getPaginaServidor(0, this.opcionesPaginacion.pageSize).subscribe((r) => {
            this.data = r.content;
            this.opcionesPaginacion.length = r.totalElements;
            this.cargando = false;
        }, error => {
            console.log(error);
        });
    }
    verificarPantallaAtras() {
        if (LogisoftStorage.isAtras()) {
            if (LogisoftStorage.getPantallaAtras().url == this.router.url) {
                return true;
            }
        }
        return false;
    }
    ngOnInit() {
    }
    ngAfterViewInit() {
        if (this.verificarPantallaAtras()) {
            this.getParametrosAtras();
        }
        this.paginador.page.subscribe(this.cambioDePagina());
    }
    cambioDePagina() {
        return (r) => {
            this.paginacionConBusqueda(r);
        };
    }
    paginacionConBusqueda(r) {
        this.cargando = true;
        let parametros = this.getParametros();
        this.service.getPaginaServidorBusqueda(r.pageIndex, r.pageSize, parametros).subscribe((r) => {
            this.opcionesPaginacion.length = r.totalElements;
            this.data = r.content;
            this.cargando = false;
        }, error => {
            console.log(error);
        });
    }
    getParametros() {
        let parametros = [];
        if (!this.textoBuscar || this.textoBuscar.length == 0) {
            return parametros;
        }
        this.columnas.forEach(col => {
            if (!col.propBuscar) {
                col.propBuscar = col.prop;
            }
            parametros.push([col.propBuscar, this.textoBuscar]);
        });
        return parametros;
    }
    //para "suscribirse como observador"
    getThis() {
        return this;
    }
    //interface buscar
    buscar(texto) {
        this.textoBuscar = texto;
        this.paginador.pageIndex = 0;
        this.paginacionConBusqueda(this.paginador);
    }
    abrirInfo(item) {
        this.modalRef = this.modalService.show(ModalTablaComponent);
        this.modalRef.content.item = item;
        this.modalRef.content.columnas = this.columnas;
        this.modalRef.content.acciones = this.acciones;
        this.modalRef.content.observador = this;
    }
    //interface acciones generales
    nuevo() {
        this.setParametrosAtras();
        this.router.navigate([this.router.url + "/editar"]);
    }
    editar(id) {
        this.setParametrosAtras();
        this.router.navigate([this.router.url + "/editar/" + id]);
    }
    eliminar(id) {
        this.servicioMensajes.mostrarMensaje(TipoMensaje.ELIMINAR, new EntidadMensaje(this.service.getCampos()
            .mensaje)).then((result) => {
            if (result.value) {
                this.service.delete(id).subscribe(r => {
                    this.servicioMensajes.mostrarMensaje(TipoMensaje.EXITO_ELIMINAR, new EntidadMensaje(this.service.getCampos()
                        .mensaje));
                    this.paginacionConBusqueda(this.paginador);
                }, e => {
                    this.servicioMensajes.mostrarMensaje(TipoMensaje.ERROR, e);
                });
            }
        });
    }
    ver(id) {
        this.setParametrosAtras();
        this.router.navigate([this.router.url + "/editar/" + id + "/ver"]);
    }
    //fin interface acciones generales
    //interface pantallaAtras
    setParametrosAtras() {
        let parametros = {
            paginador: this.convertirPaginador(this.paginador),
            busqueda: this.textoBuscar
        };
        LogisoftStorage.setPantallaAtras({
            url: this.router.url,
            parametros: parametros
        });
    }
    convertirPaginador(paginador) {
        return {
            length: paginador.length,
            pageIndex: paginador.pageIndex,
            pageSize: paginador.pageSize,
            previousPageIndex: 0
        };
    }
    getParametrosAtras() {
        let pantallaAtras = LogisoftStorage.getPantallaAtras();
        this.textoBuscar = pantallaAtras.parametros.busqueda;
        if (this.textoBuscar && this.textoBuscar.length > 0) {
            //seteo el texto en el buscador
            this.busqueda.texto = this.textoBuscar;
            this.busqueda.habilitado = false;
        }
        let paginador = pantallaAtras.parametros.paginador;
        this.paginador.length = paginador.length;
        this.paginador.pageIndex = paginador.pageIndex;
        this.paginador.pageSize = paginador.pageSize;
        this.cd.detectChanges();
        this.paginacionConBusqueda(this.paginador);
        LogisoftStorage.borrarPantallaAtras();
    }
};
TablaComponent.ctorParameters = () => [
    { type: Injector },
    { type: GenericoService },
    { type: BsModalService },
    { type: Router }
];
__decorate([
    ViewChild(MatPaginator)
], TablaComponent.prototype, "paginador", void 0);
__decorate([
    ViewChild(BusquedaComponent)
], TablaComponent.prototype, "busqueda", void 0);
TablaComponent = __decorate([
    Component({
        selector: 'app-tabla',
        template: "<h4 class=\"text-muted mb-4\">{{titulo}} </h4>\n<div>\n  <div class=\"card border-0 rounded-0 col-md-12\">\n    <div class=\"card-body cuerpo\" style=\"padding-left: 0px;\n    padding-right: 0px;\">\n    <div class=\"row\">\n      <div class=\"col-12 col-md-6\">\n        <app-busqueda [buscador]=\"getThis()\" ></app-busqueda>  \n      </div>\n      <div class=\"col-12 col-md-6\">\n        <app-acciones [componente]=\"getThis()\"></app-acciones>\n      </div>\n    </div> \n     \n      <div  class=\"table-responsive-md\">\n        <table class=\"table table-hover\">\n          <thead>\n            <tr>\n              <th scope=\"col\" *ngFor=\"let columna of columnas\" [ngClass]=\"{'d-none d-sm-none d-md-block':columna.ocultarMovil}\">{{columna.nombre}}</th>\n              <th scope=\"col\" [ngClass]=\"{'d-block d-sm-block d-md-none':true}\">Info</th>\n              <th scope=\"col\" [ngClass]=\"{'d-none d-sm-none d-md-block':true}\"  *ngIf=\"acciones.length>0\">Acciones</th>\n            </tr>\n          </thead>\n          <mat-progress-bar mode=\"indeterminate\" [hidden]=\"!cargando\"></mat-progress-bar>\n          <tbody [hidden]=\"cargando\">            \n            <tr *ngFor=\"let item of data\">\n              <td *ngFor=\"let columna of columnas\" [ngClass]=\"{'d-none d-sm-none d-md-block':columna.ocultarMovil}\">\n                <app-field-generico [col]=\"columna\" [elemento]=\"item\">\n                </app-field-generico>\n              </td>              \n              <td  [ngClass]=\"{'d-block d-sm-block d-md-none':true}\">\n                <button (click)=\"abrirInfo(item)\" type=\"button\" class=\"btn btn-outline-success\"><i class=\"fa fa-info-circle\"></i></button>\n              </td>\n              <td [ngClass]=\"{'d-none d-sm-none d-md-block':true}\" *ngIf=\"acciones.length>0\">                \n                  <app-field-generico style=\"padding: 0.1rem;\" *ngFor=\"let accion of acciones\" [col]=\"accion\" [elemento]=\"item\" [observador]=\"getThis()\" ></app-field-generico>\n              </td>\n            </tr>\n          </tbody>\n        </table>\n      </div>\n    </div>\n  </div>\n</div>\n<mat-paginator [length]=\"opcionesPaginacion.length\" [pageSize]=\"opcionesPaginacion.pageSize\"\n  [pageSizeOptions]=\"opcionesPaginacion.pageSizeOptions\">\n</mat-paginator>\n\n",
        styles: [".cuerpo{padding-left:0!important;padding-right:0!important}"]
    })
], TablaComponent);
export { TablaComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGEuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaXZhaS1nZW5lcmljby1saWJyZXJpYS8iLCJzb3VyY2VzIjpbImxpYi9nZW5lcmljby90YWJsYS90YWJsYS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxhQUFhLEVBQUUsV0FBVyxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsaUJBQWlCLEVBQUUsY0FBYyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3BKLE9BQU8sRUFBRSxZQUFZLEVBQWEsTUFBTSw2QkFBNkIsQ0FBQztBQUl0RSxPQUFPLEVBQUUsY0FBYyxFQUFFLFVBQVUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBRWpFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBRTFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUl6QyxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUczRSxPQUFPLEVBQUUsZUFBZSxFQUFFLFdBQVcsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQzVFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUNoRSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDOUQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBTzdELElBQWEsY0FBYyxHQUEzQixNQUFhLGNBQWM7SUFJekIsWUFBbUIsUUFBa0IsRUFBUSxPQUEyQixFQUFTLFlBQTRCLEVBQVEsTUFBYTtRQUEvRyxhQUFRLEdBQVIsUUFBUSxDQUFVO1FBQVEsWUFBTyxHQUFQLE9BQU8sQ0FBb0I7UUFBUyxpQkFBWSxHQUFaLFlBQVksQ0FBZ0I7UUFBUSxXQUFNLEdBQU4sTUFBTSxDQUFPO1FBV2xJLGFBQVEsR0FBRyxJQUFJLENBQUM7UUFDaEIsU0FBSSxHQUFRLEVBQUUsQ0FBQztRQUNmLFdBQU0sR0FBRyxFQUFFLENBQUM7UUFDWixhQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ2QsYUFBUSxHQUFDLEVBQUUsQ0FBQztRQUNaLHVCQUFrQixHQUFHO1lBQ25CLGVBQWUsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEdBQUcsQ0FBQztZQUNqQyxNQUFNLEVBQUUsQ0FBQztZQUNULFFBQVEsRUFBRSxFQUFFO1NBQ2IsQ0FBQztRQUNGLGdCQUFXLEdBQVcsSUFBSSxDQUFDO1FBcEJ6QixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLENBQUMsYUFBYSxDQUFDO1FBQ3ZELElBQUksQ0FBQyxRQUFRLEdBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQztRQUN6RCxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUN6QixJQUFJLENBQUMsZ0JBQWdCLEdBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQWtCLGVBQWUsQ0FBQyxDQUFDO1FBQzFFLElBQUksQ0FBQyxFQUFFLEdBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUMsQ0FBQztJQUMvQyxDQUFDO0lBcUJELGlCQUFpQjtRQUNmLElBQUcsSUFBSSxDQUFDLHNCQUFzQixFQUFFLEVBQUM7WUFBQyxPQUFPO1NBQUM7UUFDMUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQVksRUFBRSxFQUFFO1lBQzdGLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQztZQUN0QixJQUFJLENBQUMsa0JBQWtCLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7WUFDakQsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO1lBQ1QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNyQixDQUFDLENBQUMsQ0FBQztJQUVMLENBQUM7SUFDRCxzQkFBc0I7UUFDcEIsSUFBRyxlQUFlLENBQUMsT0FBTyxFQUFFLEVBQUM7WUFDM0IsSUFBRyxlQUFlLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxHQUFHLElBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUM7Z0JBQ3pELE9BQU8sSUFBSSxDQUFDO2FBQ2I7U0FDRjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQztJQUVELFFBQVE7SUFDUixDQUFDO0lBQ0QsZUFBZTtRQUNiLElBQUcsSUFBSSxDQUFDLHNCQUFzQixFQUFFLEVBQUM7WUFDL0IsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7U0FDM0I7UUFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDLENBQUM7SUFHdkQsQ0FBQztJQUNELGNBQWM7UUFDWixPQUFPLENBQUMsQ0FBWSxFQUFFLEVBQUU7WUFDdEIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2hDLENBQUMsQ0FBQztJQUNKLENBQUM7SUFJRCxxQkFBcUIsQ0FBQyxDQUFZO1FBQ2hDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ3JCLElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUN0QyxJQUFJLENBQUMsT0FBTyxDQUFDLHlCQUF5QixDQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFZLEVBQUUsRUFBRTtZQUNyRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7WUFDakQsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDO1lBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtZQUNULE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDckIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsYUFBYTtRQUNYLElBQUksVUFBVSxHQUFHLEVBQUUsQ0FBQztRQUNwQixJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7WUFBRSxPQUFPLFVBQVUsQ0FBQztTQUFFO1FBQzdFLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQzFCLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFO2dCQUFFLEdBQUcsQ0FBQyxVQUFVLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQzthQUFFO1lBQ25ELFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1FBQ3RELENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxVQUFVLENBQUM7SUFDcEIsQ0FBQztJQUdELG9DQUFvQztJQUNwQyxPQUFPO1FBQ0wsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUQsa0JBQWtCO0lBQ2xCLE1BQU0sQ0FBQyxLQUFhO1FBQ2xCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQztRQUM3QixJQUFJLENBQUMscUJBQXFCLENBQXFCLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUNqRSxDQUFDO0lBR0QsU0FBUyxDQUFDLElBQUk7UUFDWixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDNUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFDLElBQUksQ0FBQztRQUNoQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEdBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUM3QyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEdBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUM3QyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEdBQUMsSUFBSSxDQUFDO0lBQ3hDLENBQUM7SUFFSCw4QkFBOEI7SUFDNUIsS0FBSztRQUNILElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEdBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRUQsTUFBTSxDQUFDLEVBQU87UUFDWixJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxHQUFDLFVBQVUsR0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ3hELENBQUM7SUFDRCxRQUFRLENBQUMsRUFBRTtRQUNULElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBQyxJQUFJLGNBQWMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRTthQUNwRyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sRUFBRSxFQUFFO1lBQ3pCLElBQUksTUFBTSxDQUFDLEtBQUssRUFBRTtnQkFDaEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQSxFQUFFO29CQUNuQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxjQUFjLEVBQUMsSUFBSSxjQUFjLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUU7eUJBQzFHLE9BQU8sQ0FBQyxDQUFDLENBQUM7b0JBQ1gsSUFBSSxDQUFDLHFCQUFxQixDQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDbEQsQ0FBQyxFQUFDLENBQUMsQ0FBQSxFQUFFO29CQUNILElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBQyxDQUFDLENBQUMsQ0FBQztnQkFDNUQsQ0FBQyxDQUFDLENBQUM7YUFDSjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELEdBQUcsQ0FBQyxFQUFFO1FBQ0osSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsR0FBQyxVQUFVLEdBQUMsRUFBRSxHQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVELGtDQUFrQztJQUdsQyx5QkFBeUI7SUFDekIsa0JBQWtCO1FBQ2hCLElBQUksVUFBVSxHQUFDO1lBQ2IsU0FBUyxFQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQ2pELFFBQVEsRUFBQyxJQUFJLENBQUMsV0FBVztTQUFDLENBQUE7UUFDMUIsZUFBZSxDQUFDLGdCQUFnQixDQUFDO1lBQy9CLEdBQUcsRUFBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUc7WUFDbkIsVUFBVSxFQUFDLFVBQVU7U0FDdEIsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUNELGtCQUFrQixDQUFDLFNBQXNCO1FBQ3ZDLE9BQU87WUFDTCxNQUFNLEVBQUUsU0FBUyxDQUFDLE1BQU07WUFDeEIsU0FBUyxFQUFFLFNBQVMsQ0FBQyxTQUFTO1lBQzlCLFFBQVEsRUFBRSxTQUFTLENBQUMsUUFBUTtZQUM1QixpQkFBaUIsRUFBRSxDQUFDO1NBQ3JCLENBQUM7SUFDSixDQUFDO0lBQ0Qsa0JBQWtCO1FBQ2hCLElBQUksYUFBYSxHQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQ3JELElBQUksQ0FBQyxXQUFXLEdBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUM7UUFDbkQsSUFBRyxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFDLENBQUMsRUFBQztZQUMvQywrQkFBK0I7WUFDL0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEdBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztZQUNyQyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsR0FBQyxLQUFLLENBQUM7U0FDaEM7UUFDRCxJQUFJLFNBQVMsR0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQztRQUNqRCxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxHQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUM7UUFDN0MsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQztRQUMzQyxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxxQkFBcUIsQ0FBTSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDaEQsZUFBZSxDQUFDLG1CQUFtQixFQUFFLENBQUM7SUFDeEMsQ0FBQztDQUVGLENBQUE7O1lBakw4QixRQUFRO1lBQWlCLGVBQWU7WUFBMEIsY0FBYztZQUFlLE1BQU07O0FBdUJsSTtJQURDLFNBQVMsQ0FBQyxZQUFZLENBQUM7aURBQ0E7QUFFeEI7SUFEQyxTQUFTLENBQUMsaUJBQWlCLENBQUM7Z0RBQ0Q7QUE3QmpCLGNBQWM7SUFMMUIsU0FBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLFdBQVc7UUFDckIsaXpFQUFtQzs7S0FFcEMsQ0FBQztHQUNXLGNBQWMsQ0FxTDFCO1NBckxZLGNBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0NoaWxkLCBBZnRlclZpZXdJbml0LCBUZW1wbGF0ZVJlZiwgSW5qZWN0b3IsIEV2ZW50RW1pdHRlciwgQ2hhbmdlRGV0ZWN0b3JSZWYsIEluamVjdGlvblRva2VuIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBNYXRQYWdpbmF0b3IsIFBhZ2VFdmVudCB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3BhZ2luYXRvcic7XG5cblxuaW1wb3J0IHsgQnVzY2Fkb3IgfSBmcm9tICcuLi9mb3JtbHlzL2J1c3F1ZWRhL2J1c2NhZG9yJztcbmltcG9ydCB7IEJzTW9kYWxTZXJ2aWNlLCBCc01vZGFsUmVmIH0gZnJvbSAnbmd4LWJvb3RzdHJhcC9tb2RhbCc7XG5pbXBvcnQgeyBMaXN0YUNvbXBvbmVudCB9IGZyb20gJy4uL2xpc3RhL2xpc3RhLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBNb2RhbFRhYmxhQ29tcG9uZW50IH0gZnJvbSAnLi9tb2RhbC10YWJsYS9tb2RhbC10YWJsYS5jb21wb25lbnQnO1xuaW1wb3J0IHsgQWNjaW9uZXNHZW5lcmFsZXMgfSBmcm9tICcuLi9mb3JtbHlzL2FjY2lvbmVzL2FjY2lvbmVzLWdlbmVyYWxlcyc7XG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuXG5pbXBvcnQgeyBQYW50YWxsYUF0cmFzIH0gZnJvbSAnLi4vYm90b24tYXRyYXMvcGFudGFsbGEtYXRyYXMnO1xuXG5pbXBvcnQgeyBCdXNxdWVkYUNvbXBvbmVudCB9IGZyb20gJy4uL2Zvcm1seXMvYnVzcXVlZGEvYnVzcXVlZGEuY29tcG9uZW50JztcbmltcG9ydCB7IFBhZ2luYSB9IGZyb20gJy4uL3BhZ2luYWNpb24vcGFnaW5hJztcbmltcG9ydCB7IFBhZ2luYWRvciB9IGZyb20gJy4uL3BhZ2luYWNpb24vcGFnaW5hZG9yJztcbmltcG9ydCB7IE1lbnNhamVzU2VydmljZSwgVGlwb01lbnNhamUgfSBmcm9tICcuLi9tZW5zYWplcy9tZW5zYWplcy5zZXJ2aWNlJztcbmltcG9ydCB7IEdlbmVyaWNvU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2lvcy9nZW5lcmljby5zZXJ2aWNlJztcbmltcG9ydCB7IExvZ2lzb2Z0U3RvcmFnZSB9IGZyb20gJy4uL3N0b3JhZ2UvbG9naXNvZnQtc3RvcmFnZSc7XG5pbXBvcnQgeyBFbnRpZGFkTWVuc2FqZSB9IGZyb20gJy4uL21lbnNhamVzL2VudGlkYWQtbWVuc2FqZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2FwcC10YWJsYScsXG4gIHRlbXBsYXRlVXJsOiAndGFibGEuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsndGFibGEuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBUYWJsYUNvbXBvbmVudDxUPiBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCwgQnVzY2Fkb3IsQWNjaW9uZXNHZW5lcmFsZXMsUGFudGFsbGFBdHJhcyB7XG4gIG1vZGFsUmVmOiBCc01vZGFsUmVmO1xuICBzZXJ2aWNpb01lbnNhamVzOk1lbnNhamVzU2VydmljZTtcbiAgY2Q6Q2hhbmdlRGV0ZWN0b3JSZWY7XG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBpbmplY3RvcjogSW5qZWN0b3IscHVibGljIHNlcnZpY2U6IEdlbmVyaWNvU2VydmljZTxUPiwgcHVibGljIG1vZGFsU2VydmljZTogQnNNb2RhbFNlcnZpY2UscHVibGljIHJvdXRlcjpSb3V0ZXIpIHtcbiAgICB0aGlzLmNvbHVtbmFzID0gdGhpcy5zZXJ2aWNlLmdldENhbXBvcygpLmNhbXBvc0xpc3RhZG87XG4gICAgdGhpcy5hY2Npb25lcz10aGlzLnNlcnZpY2UuZ2V0Q2FtcG9zKCkuYWNjaW9uZXNHZW5lcmFsZXM7XG4gICAgdGhpcy5pbmljaW9Db25zdHJ1Y3RvcigpO1xuICAgIHRoaXMuc2VydmljaW9NZW5zYWplcz10aGlzLmluamVjdG9yLmdldDxNZW5zYWplc1NlcnZpY2U+KE1lbnNhamVzU2VydmljZSk7XG4gICAgdGhpcy5jZD10aGlzLmluamVjdG9yLmdldChDaGFuZ2VEZXRlY3RvclJlZik7XG4gIH1cbiBcbiBcbiAgXG4gIFxuICBjYXJnYW5kbyA9IHRydWU7XG4gIGRhdGE6IFRbXSA9IFtdO1xuICB0aXR1bG8gPSAnJztcbiAgY29sdW1uYXMgPSBbXTtcbiAgYWNjaW9uZXM9W107XG4gIG9wY2lvbmVzUGFnaW5hY2lvbiA9IHtcbiAgICBwYWdlU2l6ZU9wdGlvbnM6IFs1LCAxMCwgMjUsIDEwMF0sXG4gICAgbGVuZ3RoOiAwLFxuICAgIHBhZ2VTaXplOiAxMFxuICB9O1xuICB0ZXh0b0J1c2Nhcjogc3RyaW5nID0gbnVsbDtcbiAgQFZpZXdDaGlsZChNYXRQYWdpbmF0b3IpXG4gIHBhZ2luYWRvcjogTWF0UGFnaW5hdG9yO1xuICBAVmlld0NoaWxkKEJ1c3F1ZWRhQ29tcG9uZW50KVxuICBidXNxdWVkYSA6QnVzcXVlZGFDb21wb25lbnQ7XG5cbiAgaW5pY2lvQ29uc3RydWN0b3IoKSB7XG4gICAgaWYodGhpcy52ZXJpZmljYXJQYW50YWxsYUF0cmFzKCkpe3JldHVybjt9XG4gICAgdGhpcy5zZXJ2aWNlLmdldFBhZ2luYVNlcnZpZG9yKDAsIHRoaXMub3BjaW9uZXNQYWdpbmFjaW9uLnBhZ2VTaXplKS5zdWJzY3JpYmUoKHI6IFBhZ2luYTxUPikgPT4ge1xuICAgICAgdGhpcy5kYXRhID0gci5jb250ZW50O1xuICAgICAgdGhpcy5vcGNpb25lc1BhZ2luYWNpb24ubGVuZ3RoID0gci50b3RhbEVsZW1lbnRzO1xuICAgICAgdGhpcy5jYXJnYW5kbyA9IGZhbHNlO1xuICAgIH0sIGVycm9yID0+IHtcbiAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICB9KTtcbiAgIFxuICB9XG4gIHZlcmlmaWNhclBhbnRhbGxhQXRyYXMoKSA6Ym9vbGVhbntcbiAgICBpZihMb2dpc29mdFN0b3JhZ2UuaXNBdHJhcygpKXsgXG4gICAgICBpZihMb2dpc29mdFN0b3JhZ2UuZ2V0UGFudGFsbGFBdHJhcygpLnVybD09dGhpcy5yb3V0ZXIudXJsKXtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICB9XG4gIG5nQWZ0ZXJWaWV3SW5pdCgpOiB2b2lkIHtcbiAgICBpZih0aGlzLnZlcmlmaWNhclBhbnRhbGxhQXRyYXMoKSl7XG4gICAgICB0aGlzLmdldFBhcmFtZXRyb3NBdHJhcygpOyAgICBcbiAgICB9ICAgIFxuICAgIHRoaXMucGFnaW5hZG9yLnBhZ2Uuc3Vic2NyaWJlKHRoaXMuY2FtYmlvRGVQYWdpbmEoKSk7XG5cbiAgIFxuICB9XG4gIGNhbWJpb0RlUGFnaW5hKCk6IGFueSB7XG4gICAgcmV0dXJuIChyOiBQYWdpbmFkb3IpID0+IHtcbiAgICAgIHRoaXMucGFnaW5hY2lvbkNvbkJ1c3F1ZWRhKHIpO1xuICAgIH07XG4gIH1cblxuXG5cbiAgcGFnaW5hY2lvbkNvbkJ1c3F1ZWRhKHI6IFBhZ2luYWRvcikge1xuICAgIHRoaXMuY2FyZ2FuZG8gPSB0cnVlO1xuICAgIGxldCBwYXJhbWV0cm9zID0gdGhpcy5nZXRQYXJhbWV0cm9zKCk7XG4gICAgdGhpcy5zZXJ2aWNlLmdldFBhZ2luYVNlcnZpZG9yQnVzcXVlZGEoci5wYWdlSW5kZXgsIHIucGFnZVNpemUsIHBhcmFtZXRyb3MpLnN1YnNjcmliZSgocjogUGFnaW5hPFQ+KSA9PiB7XG4gICAgICB0aGlzLm9wY2lvbmVzUGFnaW5hY2lvbi5sZW5ndGggPSByLnRvdGFsRWxlbWVudHM7XG4gICAgICB0aGlzLmRhdGEgPSByLmNvbnRlbnQ7XG4gICAgICB0aGlzLmNhcmdhbmRvID0gZmFsc2U7XG4gICAgfSwgZXJyb3IgPT4ge1xuICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgIH0pO1xuICB9XG5cbiAgZ2V0UGFyYW1ldHJvcygpIHtcbiAgICBsZXQgcGFyYW1ldHJvcyA9IFtdO1xuICAgIGlmICghdGhpcy50ZXh0b0J1c2NhciB8fCB0aGlzLnRleHRvQnVzY2FyLmxlbmd0aCA9PSAwKSB7IHJldHVybiBwYXJhbWV0cm9zOyB9XG4gICAgdGhpcy5jb2x1bW5hcy5mb3JFYWNoKGNvbCA9PiB7XG4gICAgICBpZiAoIWNvbC5wcm9wQnVzY2FyKSB7IGNvbC5wcm9wQnVzY2FyID0gY29sLnByb3A7IH1cbiAgICAgIHBhcmFtZXRyb3MucHVzaChbY29sLnByb3BCdXNjYXIsIHRoaXMudGV4dG9CdXNjYXJdKTtcbiAgICB9KTtcbiAgICByZXR1cm4gcGFyYW1ldHJvcztcbiAgfVxuXG5cbiAgLy9wYXJhIFwic3VzY3JpYmlyc2UgY29tbyBvYnNlcnZhZG9yXCJcbiAgZ2V0VGhpcygpIHtcbiAgICByZXR1cm4gdGhpcztcbiAgfVxuXG4gIC8vaW50ZXJmYWNlIGJ1c2NhclxuICBidXNjYXIodGV4dG86IHN0cmluZykge1xuICAgIHRoaXMudGV4dG9CdXNjYXIgPSB0ZXh0bztcbiAgICB0aGlzLnBhZ2luYWRvci5wYWdlSW5kZXggPSAwO1xuICAgIHRoaXMucGFnaW5hY2lvbkNvbkJ1c3F1ZWRhKDxQYWdpbmFkb3I+PHVua25vd24+dGhpcy5wYWdpbmFkb3IpO1xuICB9XG5cblxuICBhYnJpckluZm8oaXRlbSkge1xuICAgIHRoaXMubW9kYWxSZWYgPSB0aGlzLm1vZGFsU2VydmljZS5zaG93KE1vZGFsVGFibGFDb21wb25lbnQpOyAgICBcbiAgICB0aGlzLm1vZGFsUmVmLmNvbnRlbnQuaXRlbT1pdGVtO1xuICAgIHRoaXMubW9kYWxSZWYuY29udGVudC5jb2x1bW5hcz10aGlzLmNvbHVtbmFzO1xuICAgIHRoaXMubW9kYWxSZWYuY29udGVudC5hY2Npb25lcz10aGlzLmFjY2lvbmVzO1xuICAgIHRoaXMubW9kYWxSZWYuY29udGVudC5vYnNlcnZhZG9yPXRoaXM7XG4gIH1cblxuLy9pbnRlcmZhY2UgYWNjaW9uZXMgZ2VuZXJhbGVzXG4gIG51ZXZvKCkge1xuICAgIHRoaXMuc2V0UGFyYW1ldHJvc0F0cmFzKCk7XG4gICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW3RoaXMucm91dGVyLnVybCtcIi9lZGl0YXJcIl0pO1xuICB9XG5cbiAgZWRpdGFyKGlkOiBhbnkpIHtcbiAgICB0aGlzLnNldFBhcmFtZXRyb3NBdHJhcygpO1xuICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFt0aGlzLnJvdXRlci51cmwrXCIvZWRpdGFyL1wiK2lkXSk7XG4gIH1cbiAgZWxpbWluYXIoaWQpIHtcbiAgICB0aGlzLnNlcnZpY2lvTWVuc2FqZXMubW9zdHJhck1lbnNhamUoVGlwb01lbnNhamUuRUxJTUlOQVIsbmV3IEVudGlkYWRNZW5zYWplKHRoaXMuc2VydmljZS5nZXRDYW1wb3MoKVxuICAgIC5tZW5zYWplKSkudGhlbigocmVzdWx0KSA9PiB7XG4gICAgICBpZiAocmVzdWx0LnZhbHVlKSB7XG4gICAgICAgIHRoaXMuc2VydmljZS5kZWxldGUoaWQpLnN1YnNjcmliZShyPT57XG4gICAgICAgICAgdGhpcy5zZXJ2aWNpb01lbnNhamVzLm1vc3RyYXJNZW5zYWplKFRpcG9NZW5zYWplLkVYSVRPX0VMSU1JTkFSLG5ldyBFbnRpZGFkTWVuc2FqZSh0aGlzLnNlcnZpY2UuZ2V0Q2FtcG9zKClcbiAgICAgICAgICAubWVuc2FqZSkpO1xuICAgICAgICAgIHRoaXMucGFnaW5hY2lvbkNvbkJ1c3F1ZWRhKDxhbnk+dGhpcy5wYWdpbmFkb3IpO1xuICAgICAgICB9LGU9PntcbiAgICAgICAgICB0aGlzLnNlcnZpY2lvTWVuc2FqZXMubW9zdHJhck1lbnNhamUoVGlwb01lbnNhamUuRVJST1IsZSk7XG4gICAgICAgIH0pOyAgICAgICAgXG4gICAgICB9XG4gICAgfSk7IFxuICB9XG5cbiAgdmVyKGlkKSB7XG4gICAgdGhpcy5zZXRQYXJhbWV0cm9zQXRyYXMoKTtcbiAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbdGhpcy5yb3V0ZXIudXJsK1wiL2VkaXRhci9cIitpZCtcIi92ZXJcIl0pO1xuICB9XG5cbiAgLy9maW4gaW50ZXJmYWNlIGFjY2lvbmVzIGdlbmVyYWxlc1xuXG5cbiAgLy9pbnRlcmZhY2UgcGFudGFsbGFBdHJhc1xuICBzZXRQYXJhbWV0cm9zQXRyYXMoKSB7XG4gICAgbGV0IHBhcmFtZXRyb3M9e1xuICAgICAgcGFnaW5hZG9yOnRoaXMuY29udmVydGlyUGFnaW5hZG9yKHRoaXMucGFnaW5hZG9yKSwgICAgICBcbiAgICAgIGJ1c3F1ZWRhOnRoaXMudGV4dG9CdXNjYXJ9ICAgICAgXG4gICAgICBMb2dpc29mdFN0b3JhZ2Uuc2V0UGFudGFsbGFBdHJhcyh7XG4gICAgICAgIHVybDp0aGlzLnJvdXRlci51cmwsXG4gICAgICAgIHBhcmFtZXRyb3M6cGFyYW1ldHJvc1xuICAgICAgfSk7XG4gIH1cbiAgY29udmVydGlyUGFnaW5hZG9yKHBhZ2luYWRvcjpNYXRQYWdpbmF0b3IpOlBhZ2luYWRvciB7XG4gICAgcmV0dXJuIHtcbiAgICAgIGxlbmd0aDogcGFnaW5hZG9yLmxlbmd0aCxcbiAgICAgIHBhZ2VJbmRleDogcGFnaW5hZG9yLnBhZ2VJbmRleCxcbiAgICAgIHBhZ2VTaXplOiBwYWdpbmFkb3IucGFnZVNpemUsXG4gICAgICBwcmV2aW91c1BhZ2VJbmRleDogMFxuICAgIH07XG4gIH1cbiAgZ2V0UGFyYW1ldHJvc0F0cmFzKCkgeyAgICBcbiAgICBsZXQgcGFudGFsbGFBdHJhcz1Mb2dpc29mdFN0b3JhZ2UuZ2V0UGFudGFsbGFBdHJhcygpO1xuICAgIHRoaXMudGV4dG9CdXNjYXI9cGFudGFsbGFBdHJhcy5wYXJhbWV0cm9zLmJ1c3F1ZWRhO1xuICAgIGlmKHRoaXMudGV4dG9CdXNjYXIgJiYgdGhpcy50ZXh0b0J1c2Nhci5sZW5ndGg+MCl7XG4gICAgICAvL3NldGVvIGVsIHRleHRvIGVuIGVsIGJ1c2NhZG9yXG4gICAgICB0aGlzLmJ1c3F1ZWRhLnRleHRvPXRoaXMudGV4dG9CdXNjYXI7XG4gICAgICB0aGlzLmJ1c3F1ZWRhLmhhYmlsaXRhZG89ZmFsc2U7XG4gICAgfSAgIFxuICAgIGxldCBwYWdpbmFkb3I9cGFudGFsbGFBdHJhcy5wYXJhbWV0cm9zLnBhZ2luYWRvcjsgICAgXG4gICAgdGhpcy5wYWdpbmFkb3IubGVuZ3RoPXBhZ2luYWRvci5sZW5ndGg7XG4gICAgdGhpcy5wYWdpbmFkb3IucGFnZUluZGV4PXBhZ2luYWRvci5wYWdlSW5kZXg7XG4gICAgdGhpcy5wYWdpbmFkb3IucGFnZVNpemU9cGFnaW5hZG9yLnBhZ2VTaXplO1xuICAgIHRoaXMuY2QuZGV0ZWN0Q2hhbmdlcygpO1xuICAgIHRoaXMucGFnaW5hY2lvbkNvbkJ1c3F1ZWRhKDxhbnk+dGhpcy5wYWdpbmFkb3IpO1xuICAgIExvZ2lzb2Z0U3RvcmFnZS5ib3JyYXJQYW50YWxsYUF0cmFzKCk7XG4gIH1cbiAgLy9maW4gaW50ZXJmYWNlIHBhbnRhbGxhQXRyYXNcbn1cbiJdfQ==