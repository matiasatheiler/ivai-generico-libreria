import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/public_api';
let ModalTablaComponent = class ModalTablaComponent {
    constructor(modalRef) {
        this.modalRef = modalRef;
        this.item = {};
        this.columnas = [];
        this.acciones = [];
    }
    getThis() {
        return this;
    }
    nuevo() {
        this.modalRef.hide();
        this.observador.nuevo();
    }
    editar(id) {
        this.modalRef.hide();
        this.observador.editar(id);
    }
    eliminar(id) {
        this.modalRef.hide();
        this.observador.eliminar(id);
    }
    ver(id) {
        this.modalRef.hide();
        this.observador.ver(id);
    }
    ngOnInit() {
    }
    getTitulo() {
        const titulo = this.columnas.find(col => col.titulo);
        if (titulo) {
            return this.item[titulo.prop];
        }
        return "Logisoft";
    }
};
ModalTablaComponent.ctorParameters = () => [
    { type: BsModalRef }
];
ModalTablaComponent = __decorate([
    Component({
        selector: 'app-modal-tabla',
        template: "<div class=\"modal-header\">\n  <h4 class=\"modal-title pull-left\" id=\"my-modal-title\">{{getTitulo()}}</h4>\n  <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"modalRef.hide()\">\n    <span aria-hidden=\"true\">&times;</span>\n  </button>   \n  \n</div>\n\n<div class=\"modal-body\">\n  <div class=\"col-12 \">\n    <app-field-generico class=\"float-right\" style=\"padding: 0.1rem;\" *ngFor=\"let accion of acciones\" [col]=\"accion\" [elemento]=\"item\" [observador]=\"getThis()\" ></app-field-generico>\n  </div>\n  <br>\n  \n    <ul class=\"list-group col-12\">    \n      <li *ngFor=\"let columna of columnas\" class=\"list-group-item disabled\">\n        <div class=\"row \"><h6>{{columna.nombre}}</h6></div>      \n        <div class=\"row justify-content-center align-items-center\">\n          <app-field-generico  [col]=\"columna\" [elemento]=\"item\">\n          </app-field-generico>\n        </div>\n       \n      </li>\n      \n    </ul>\n  \n  \n  \n</div>",
        styles: [""]
    })
], ModalTablaComponent);
export { ModalTablaComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwtdGFibGEuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vaXZhaS1nZW5lcmljby1saWJyZXJpYS8iLCJzb3VyY2VzIjpbImxpYi9nZW5lcmljby90YWJsYS9tb2RhbC10YWJsYS9tb2RhbC10YWJsYS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFDbEQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBUzVELElBQWEsbUJBQW1CLEdBQWhDLE1BQWEsbUJBQW1CO0lBSzlCLFlBQW1CLFFBQW1CO1FBQW5CLGFBQVEsR0FBUixRQUFRLENBQVc7UUFKckMsU0FBSSxHQUFDLEVBQUUsQ0FBQztRQUNSLGFBQVEsR0FBQyxFQUFFLENBQUM7UUFDWixhQUFRLEdBQUMsRUFBRSxDQUFDO0lBRTZCLENBQUM7SUFFM0MsT0FBTztRQUNMLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUNELEtBQUs7UUFDSCxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDMUIsQ0FBQztJQUNELE1BQU0sQ0FBQyxFQUFPO1FBQ1osSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUM3QixDQUFDO0lBQ0QsUUFBUSxDQUFDLEVBQU87UUFDZCxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFDRCxHQUFHLENBQUMsRUFBTztRQUNULElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDMUIsQ0FBQztJQUVELFFBQVE7SUFDUixDQUFDO0lBR0QsU0FBUztRQUNQLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3JELElBQUcsTUFBTSxFQUFDO1lBQUMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUFDO1FBQzFDLE9BQU8sVUFBVSxDQUFDO0lBQ3BCLENBQUM7Q0FFRixDQUFBOztZQWhDNkIsVUFBVTs7QUFMM0IsbUJBQW1CO0lBTC9CLFNBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSxpQkFBaUI7UUFDM0IsNC9CQUEyQzs7S0FFNUMsQ0FBQztHQUNXLG1CQUFtQixDQXFDL0I7U0FyQ1ksbUJBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEJzTW9kYWxSZWYgfSBmcm9tICduZ3gtYm9vdHN0cmFwL21vZGFsL3B1YmxpY19hcGknO1xuaW1wb3J0IHsgQWNjaW9uZXNHZW5lcmFsZXMgfSBmcm9tICcuLi8uLi9mb3JtbHlzL2FjY2lvbmVzL2FjY2lvbmVzLWdlbmVyYWxlcyc7XG5pbXBvcnQgeyBUYWJsYUNvbXBvbmVudCB9IGZyb20gJy4uL3RhYmxhLmNvbXBvbmVudCc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2FwcC1tb2RhbC10YWJsYScsXG4gIHRlbXBsYXRlVXJsOiAnLi9tb2RhbC10YWJsYS5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL21vZGFsLXRhYmxhLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgTW9kYWxUYWJsYUNvbXBvbmVudDxUPiBpbXBsZW1lbnRzIE9uSW5pdCxBY2Npb25lc0dlbmVyYWxlcyB7XG4gICBpdGVtPXt9O1xuICAgY29sdW1uYXM9W107XG4gICBhY2Npb25lcz1bXTtcbiAgIG9ic2VydmFkb3I6IFRhYmxhQ29tcG9uZW50PFQ+O1xuICBjb25zdHJ1Y3RvcihwdWJsaWMgbW9kYWxSZWY6QnNNb2RhbFJlZikgeyB9XG5cbiAgZ2V0VGhpcygpe1xuICAgIHJldHVybiB0aGlzO1xuICB9XG4gIG51ZXZvKCkge1xuICAgIHRoaXMubW9kYWxSZWYuaGlkZSgpO1xuICAgIHRoaXMub2JzZXJ2YWRvci5udWV2bygpO1xuICB9XG4gIGVkaXRhcihpZDogYW55KSB7XG4gICAgdGhpcy5tb2RhbFJlZi5oaWRlKCk7XG4gICAgdGhpcy5vYnNlcnZhZG9yLmVkaXRhcihpZCk7XG4gIH1cbiAgZWxpbWluYXIoaWQ6IGFueSkge1xuICAgIHRoaXMubW9kYWxSZWYuaGlkZSgpO1xuICAgIHRoaXMub2JzZXJ2YWRvci5lbGltaW5hcihpZCk7XG4gIH1cbiAgdmVyKGlkOiBhbnkpIHtcbiAgICB0aGlzLm1vZGFsUmVmLmhpZGUoKTtcbiAgICB0aGlzLm9ic2VydmFkb3IudmVyKGlkKTtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cblxuICBnZXRUaXR1bG8oKTpzdHJpbmd7XG4gICAgY29uc3QgdGl0dWxvID0gdGhpcy5jb2x1bW5hcy5maW5kKGNvbCA9PiBjb2wudGl0dWxvKTtcbiAgICBpZih0aXR1bG8pe3JldHVybiB0aGlzLml0ZW1bdGl0dWxvLnByb3BdO31cbiAgICByZXR1cm4gXCJMb2dpc29mdFwiO1xuICB9XG5cbn1cbiJdfQ==