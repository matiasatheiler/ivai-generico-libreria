/**
 * Generated bundle index. Do not edit.
 */
export * from './public-api';
export { AutocompleteTypeComponent as ɵd } from './lib/generico/formlys/autocomplete-type/autocomplete-type.component';
export { ElementoTablaComponent as ɵa } from './lib/generico/formlys/elemento-tabla/elemento-tabla.component';
export { MostrarTextoComponent as ɵc } from './lib/generico/formlys/mostrar-texto/mostrar-texto.component';
export { ListaComponent as ɵb } from './lib/generico/lista/lista.component';
