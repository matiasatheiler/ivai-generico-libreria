import { __decorate } from 'tslib';
import { ɵɵdefineInjectable, ɵɵinject, Injectable, ChangeDetectorRef, Component, Injector, ComponentFactoryResolver, Input, ViewChild, ViewContainerRef, NgModule } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
import { FormArray, FormGroup, Validators, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { Location, CommonModule } from '@angular/common';
import { map, startWith, switchMap } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal/public_api';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { BsModalService } from 'ngx-bootstrap/modal';
import { FormlyModule } from '@ngx-formly/core';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatStepperModule } from '@angular/material/stepper';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule, MatInput } from '@angular/material/input';
import { FormlyMaterialModule, FieldType } from '@ngx-formly/material';
import { FormlyMatToggleModule } from '@ngx-formly/material/toggle';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { MatAutocompleteModule, MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { MatIconModule } from '@angular/material/icon';

class LogisoftStorage {
    static borrarPantallaAtras() {
        localStorage.removeItem('atras');
    }
    static getPantallaAtras() {
        return JSON.parse(localStorage.getItem('atras'));
    }
    static isAtras() {
        if (localStorage.getItem('atras')) {
            return true;
        }
        return false;
    }
    static setPantallaAtras(pantalla) {
        localStorage.setItem('atras', JSON.stringify(pantalla));
    }
    static isUsuario() {
        if (localStorage.getItem('currentUser')) {
            return true;
        }
        return false;
    }
    static cerrarSesion() {
        localStorage.removeItem('currentUser');
    }
    static getNombreUsuario() {
        if (!localStorage.getItem('currentUser')) {
            return '';
        }
        return JSON.parse(localStorage.getItem('currentUser')).nombre;
    }
}

class BotonIconoEstilo {
    constructor(icono, clase) {
        this.icono = icono;
        this.clase = clase;
    }
}

class Configuracion {
    static obtenerClaseIcono(tipo) {
        switch (tipo) {
            case "editar":
                return new BotonIconoEstilo("fa fa-edit", "btn btn-primary");
            case "eliminar":
                return new BotonIconoEstilo("fa fa-trash", "btn btn-danger");
            case "ver":
                return new BotonIconoEstilo("fa fa-search", "btn btn-info");
            default:
                return new BotonIconoEstilo("fa fa-fingerprint", "btn btn-warning");
        }
    }
}
Configuracion.MODOS = { VER: 0, EDITAR: 1, NUEVO: 2 };

let GenericoService = class GenericoService {
    constructor(http) {
        this.http = http;
        this.size = 10;
        this.urlObjeto = '';
        this.json = {};
    }
    get(id) {
        return this.http.get(this.getUrl() + "/" + id);
    }
    getAll() {
        return this.http.get(this.getUrl());
    }
    getPagina() {
        return this.getPaginaServidor(0, this.size);
    }
    getPaginaServidor(pageIndex, pageSize) {
        const params = new HttpParams()
            .set('page', pageIndex.toString())
            .set('pagination', 'true')
            .set('size', pageSize.toString())
            .set('sort', 'id,asc');
        return this.http.get(this.getUrl(), { params });
    }
    getPaginaServidorBusqueda(pageIndex, pageSize, parametros) {
        let params = new HttpParams()
            .set('page', pageIndex.toString())
            .set('pagination', 'true')
            .set('size', pageSize.toString())
            .set('sort', 'id,asc');
        for (let i = 0; i < parametros.length; i++) {
            const p = parametros[i];
            params = params.append(p[0], p[1]);
        }
        return this.http.get(this.getUrl(), { params });
    }
    getUrl() {
        return this.url + this.urlObjeto;
    }
    getCampos() {
        return this.json;
    }
    nuevo(objeto) {
        return this.http.post(this.getUrl(), objeto);
    }
    delete(id) {
        return this.http.delete(this.getUrl() + "/" + id);
    }
};
GenericoService.ctorParameters = () => [
    { type: HttpClient }
];
GenericoService.ɵprov = ɵɵdefineInjectable({ factory: function GenericoService_Factory() { return new GenericoService(ɵɵinject(HttpClient)); }, token: GenericoService, providedIn: "root" });
GenericoService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], GenericoService);

var TipoMensaje;
(function (TipoMensaje) {
    TipoMensaje["EXITO"] = "EXITO";
    TipoMensaje["ERROR"] = "ERROR";
    TipoMensaje["ELIMINAR"] = "ELIMINAR";
    TipoMensaje["EXITO_ELIMINAR"] = "EXITO_ELIMINAR";
})(TipoMensaje || (TipoMensaje = {}));
let MensajesService = class MensajesService {
    constructor() {
        this.jsonMensajes = this.getMensajes();
    }
    mostrarMensaje(tipo, entidadMensaje) {
        switch (tipo) {
            case TipoMensaje.EXITO:
                return this.mensajeExito(entidadMensaje);
            case TipoMensaje.ELIMINAR:
                return this.mensajeEliminar(entidadMensaje);
            case TipoMensaje.EXITO_ELIMINAR:
                return this.mensajeExitoEliminar(entidadMensaje);
            default:
                break;
        }
    }
    mensajeExitoEliminar(entidadMensaje) {
        let mensaje = Object.assign({}, this.jsonMensajes.EXITO_ELIMINAR);
        mensaje.text = entidadMensaje.getSingularConArticulo() + mensaje.text;
        return Swal.fire(mensaje);
    }
    mensajeEliminar(entidadMensaje) {
        let mensaje = Object.assign({}, this.jsonMensajes.ELIMINAR);
        mensaje.text = mensaje.text + entidadMensaje.getSingularConArticulo();
        return Swal.fire(mensaje);
    }
    mensajeExito(entidadMensaje) {
        let mensaje = Object.assign({}, this.jsonMensajes.EXITO);
        mensaje.text = entidadMensaje.getSingularConArticulo() + mensaje.text;
        return Swal.fire(mensaje);
    }
    getMensajes() {
        return {
            "CARGANDO": {
                "title": "Cargando..!!!",
                "width": 600,
                "padding": "3em",
                "background": "#fff",
                "backdrop": "rgba(0,0,0,0.7)"
            },
            "EXITO": {
                "icon": "success",
                "title": "Carga Exitosa",
                "text": " se guardo correctamente"
            },
            "EXITO_ELIMINAR": {
                "icon": "success",
                "title": "Eliminación Exitosa",
                "text": " se ha eliminado correctamente"
            },
            "ERROR": {
                "icon": "error",
                "title": "Error",
                "text": "Algo salió mal!"
            },
            "ELIMINAR": {
                "title": "Eliminar",
                "text": "¿ Esta seguro que desea eliminar ?",
                "icon": "warning",
                "showCancelButton": true,
                "confirmButtonColor": "#3085d6",
                "cancelButtonColor": "#d33",
                "confirmButtonText": "Si"
            }
        };
    }
};
MensajesService.ɵprov = ɵɵdefineInjectable({ factory: function MensajesService_Factory() { return new MensajesService(); }, token: MensajesService, providedIn: "root" });
MensajesService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], MensajesService);

let ElementoTablaComponent = class ElementoTablaComponent {
    constructor(cd) {
        this.cd = cd;
        this.eBoolean = false;
        this._col = { prop: "dummy.", clase: "false", icon: null, tipo: null, estilo: null };
        this._elemento = { dummy: null };
    }
    ngOnInit() {
    }
    get elemento() {
        return this._elemento;
    }
    set elemento(value) {
        this._elemento = value;
        this.setPropiedadChange();
        this.cd.detectChanges();
    }
    get col() {
        return this._col;
    }
    set col(value) {
        this._col = value;
        this.cd.detectChanges();
    }
    setPropiedadChange() {
        if (!this.col.prop) {
            return;
        }
        const props = this.col.prop.split(".");
        var e = this.elemento;
        for (let i = 0; i < props.length; i++) {
            const p = props[i];
            e = e[p];
            if (!e) {
                this.propiedad = "";
                return;
            }
        }
        this.propiedad = e;
    }
};
ElementoTablaComponent.ctorParameters = () => [
    { type: ChangeDetectorRef }
];
ElementoTablaComponent = __decorate([
    Component({
        selector: 'app-elemento-tabla',
        template: "<p>\n  elemento-tabla works!\n</p>\n",
        styles: [""]
    })
], ElementoTablaComponent);

let BotonIconoComponent = class BotonIconoComponent extends ElementoTablaComponent {
    constructor(cd) {
        super(cd);
        this.cd = cd;
    }
    ngOnInit() {
    }
    getClase() {
        if (this.col.estilo != null) {
            return Configuracion.obtenerClaseIcono(this.col.estilo);
        }
        else {
            return new BotonIconoEstilo(this.col.icon, this.col.clase);
        }
    }
    click() {
        if (this.col.estilo) {
            this.ejecutarFuncionGenerico();
        }
        else {
            this.ejecutarFuncion(this.elemento);
        }
    }
    ejecutarFuncion(item) {
        throw new Error("Method not implemented.");
    }
    ejecutarFuncionGenerico() {
        switch (this.col.estilo) {
            case "editar":
                this.observador.editar(this.elemento.id);
                break;
            case "eliminar":
                this.observador.eliminar(this.elemento.id);
                break;
            case "ver":
                this.observador.ver(this.elemento.id);
                break;
            default:
                console.log("no esta implementado el metodo " + this.col.estilo);
                break;
        }
    }
};
BotonIconoComponent.ctorParameters = () => [
    { type: ChangeDetectorRef }
];
BotonIconoComponent = __decorate([
    Component({
        selector: 'app-boton-icono',
        template: "<button type=\"button\" (click)=\"click()\" [ngClass]=\"[getClase().clase]\"><i [ngClass]=\"[getClase().icono]\"></i></button>",
        styles: [""]
    })
], BotonIconoComponent);

class EntidadMensaje {
    constructor(entidadO) {
        this.articulo = null;
        this.singular = null;
        this.articuloPlural = null;
        this.plural = null;
        this.femenino = false;
        //recorro las propiedades del nuevo objeto
        //este metodo se utiliza para sobreescribir las propiedades del objeto        
        Object.keys(this).forEach(k => {
            console.log(Object.keys(entidadO).indexOf(k));
            if (Object.keys(entidadO).includes(k)) {
                this[k] = entidadO[k];
            }
        });
    }
    getSingular() {
        return this.singular;
    }
    getPlural() {
        if (this.plural != null) {
            return this.plural;
        }
        return this.singular + "s";
    }
    getSingularConArticulo() {
        if (this.articulo != null) {
            return this.articulo + " " + this.getSingular();
        }
        this.articulo = "El";
        if (this.femenino) {
            this.articulo = "La";
        }
        return this.articulo + " " + this.getSingular();
    }
    getPluralConArticulo() {
        if (this.articuloPlural != null) {
            return this.articuloPlural + " " + this.getPlural();
        }
        this.articuloPlural = "Los";
        if (this.femenino) {
            this.articulo = "Las";
        }
        return this.articuloPlural + " " + this.getPlural();
    }
}

let EditarComponent = class EditarComponent {
    constructor(service, injector, rutaActiva) {
        this.service = service;
        this.injector = injector;
        this.rutaActiva = rutaActiva;
        this.cargando = 0;
        this.activedStep = 0;
        this.model = {};
        this.steps = [];
        this.form = new FormArray(this.steps.map(() => new FormGroup({})), Validators.required);
        this.options = this.steps.map(() => ({}));
        this.compareWith = function (o1, o2) {
            if (!o2 && !o1) {
                return true;
            }
            if (!o2) {
                return false;
            }
            return o1.id === o2.id;
        };
        this.servicioMensajes = this.injector.get(MensajesService);
        this.location = this.injector.get(Location);
    }
    ngOnChanges(changes) {
        console.log(changes);
    }
    ngOnInit() {
        this.setearModo();
        this.jsonEditar = this.service.json.camposEditar;
        this.jsonEditar.forEach(step => {
            step.fields.forEach(field => {
                if (this.modo == Configuracion.MODOS.VER) {
                    field.templateOptions.disabled = true;
                    this.form.disable({ onlySelf: false, emitEvent: true });
                }
                else {
                    field.templateOptions.disabled = false;
                    this.form.disable({ onlySelf: true, emitEvent: true });
                }
                if (field.type == 'select' && !field.templateOptions.multiple) {
                    // el nombre del key debe ser igual al del servicio inyectado         
                    this.procesarTipoSelect(field);
                }
                if (field.type == 'select' && field.templateOptions.multiple) {
                    // el nombre del key debe ser igual al del servicio inyectado         
                    this.procesarTipoSelectMultiple(field);
                }
                if (field.type == 'autocomplete') {
                    this.procesarTipoAutocomplete(field);
                }
            });
        });
        this.steps = this.jsonEditar;
        this.form = new FormArray(this.steps.map(() => new FormGroup({})), Validators.required);
        this.options = this.steps.map(() => ({}));
        if (this.modo == Configuracion.MODOS.VER) {
            this.form.disable({ onlySelf: false, emitEvent: false });
        }
    }
    procesarTipoSelectMultiple(field) {
        const service = this.injector.get(this.serviciosConfig.servicios[field.data.servicio.toLowerCase()].servicio);
        if (field.data.lista && field.data.lista == 'eager') {
            this.cargando++;
        }
        service.getAll().subscribe(r => {
            field.templateOptions.options = this.convertirOpciones(r, field.prop);
            field.templateOptions.compareWith = this.compareWith;
            if (field.data.lista && field.data.lista == 'eager') {
                this.cargando--;
            }
        });
    }
    procesarTipoAutocomplete(field) {
        field.templateOptions.compareWith = this.compareWith;
        field.validators = {
            ip: {
                expression: (c) => c.value.id,
                message: (error, field) => `"${field.formControl.value}", ingrese un valor existente.`,
            }
        };
        if (this.model[field.key]) {
            this.model[field.key].toString = function () { return this.model[field.key][field.data.prop]; };
        }
        field.templateOptions.filter = ((term) => this.getTerm(field.data.servicio.toLowerCase(), term, field.data.prop));
    }
    procesarTipoSelect(field) {
        const service = this.injector.get(this.serviciosConfig.servicios[field.data.servicio.toLowerCase()].servicio);
        if (field.data.lista && field.data.lista == 'eager') {
            this.cargando++;
        }
        service.getAll().subscribe(r => {
            field.templateOptions.options = this.convertirOpciones(r, field.prop);
            field.templateOptions.compareWith = this.compareWith;
            if (field.data.lista && field.data.lista == 'eager') {
                this.cargando--;
            }
        });
    }
    setearModo() {
        if (this.rutaActiva.snapshot.params.ver && this.rutaActiva.snapshot.params.ver == "ver") {
            this.modo = Configuracion.MODOS.VER;
            this.cargando++;
            this.modoEditar(this.rutaActiva.snapshot.params.id);
            return;
        }
        if (this.rutaActiva.snapshot.params.id) {
            this.modo = Configuracion.MODOS.EDITAR;
            this.cargando++;
            this.modoEditar(this.rutaActiva.snapshot.params.id);
            return;
        }
        this.modo = Configuracion.MODOS.NUEVO;
    }
    modoEditar(id) {
        this.service.get(id).subscribe(r => {
            this.model = this.convertirToString(r);
            this.cargando--;
        });
    }
    convertirToString(r) {
        this.jsonEditar.forEach(step => {
            step.fields.forEach(field => {
                if (field.type == 'autocomplete') {
                    r[field.key].toString = function () { return r[field.key][field.data.prop]; };
                }
            });
        });
        return r;
    }
    convertirOpciones(r, opcion) {
        const opciones = [];
        for (let i = 0; i < r.length; i++) {
            const element = r[i];
            opciones.push({ value: element, label: element[opcion] });
        }
        return opciones;
    }
    prevStep(step) {
        this.activedStep = step - 1;
    }
    nextStep(step) {
        this.activedStep = step + 1;
    }
    submit() {
        console.log(this.model);
        this.service.nuevo(this.model).subscribe(r => {
            this.servicioMensajes.mostrarMensaje(TipoMensaje.EXITO, new EntidadMensaje(this.service.json.mensaje));
            this.location.back();
        }, e => {
            console.log(e);
        });
    }
    getTexto(field) {
        let texto = this.model[field.key];
        if (!texto) {
            return "";
        }
        if (field.type == "select") {
            texto = texto[field.prop];
        }
        if (field.type == "autocomplete") {
            texto = texto[field.data.prop];
        }
        return texto;
    }
    getLabel(field) {
        return field.templateOptions.label;
    }
    /* getTerm(term: any) {
       return this.injector.get(ServiciosConfig.servicios.zona.servicio).getAll();
     }
   */
    getTerm(servicioString, term, arg) {
        return this.injector.get(this.serviciosConfig.servicios[servicioString].servicio).
            getPaginaServidorBusqueda(0, 20, [[arg, term]]).pipe(map(n => this.convertirRespuestaAutocomplete(n, arg)));
    }
    convertirRespuestaAutocomplete(array, arg) {
        let arrayConvert = [];
        console.log(array);
        for (let index = 0; index < array.content.length; index++) {
            let element = array.content[index];
            element.toString = function () { return element[arg]; };
            arrayConvert.push(element);
        }
        return arrayConvert;
    }
};
EditarComponent.ctorParameters = () => [
    { type: GenericoService },
    { type: Injector },
    { type: ActivatedRoute }
];
EditarComponent = __decorate([
    Component({
        selector: 'app-editar',
        template: "<mat-progress-bar mode=\"indeterminate\"   [hidden]=\"0==cargando\"></mat-progress-bar>\n<form [formGroup]=\"form\" (ngSubmit)=\"submit()\" [hidden]=\"cargando>0\">\n\n  <mat-horizontal-stepper>\n    \n    <mat-step *ngFor=\"let step of steps; let index = index; let last = last;\"   >\n      <mat-list *ngIf=\"modo==0\" class=\"row\">\n        <mat-list-item *ngFor=\"let field of step.fields\" >\n          <mat-form-field class=\"example-full-width\">\n            <mat-label>{{getLabel(field)}}</mat-label>\n            <input matInput readonly [value]=\"getTexto(field)\">\n          </mat-form-field>\n           </mat-list-item>\n        \n       </mat-list>\n      <ng-template matStepLabel>{{ step.label }}</ng-template>\n      <formly-form *ngIf=\"modo!=0\"\n        [form]=\"form.at(index)\"\n        [model]=\"model\"\n        [fields]=\"step.fields\"\n        [options]=\"options[index]\">\n      </formly-form>\n      \n      <div>\n        <button *ngIf=\"index !== 0\" matStepperPrevious class=\"btn btn-primary\" type=\"button\" (click)=\"prevStep(index)\"><i class=\"fa fa-angle-double-left\"></i></button>\n        <button *ngIf=\"!last && modo!=0\" matStepperNext class=\"btn btn-primary\" type=\"button\" [disabled]=\"!form.at(index).valid\" (click)=\"nextStep(index)\"><i class=\"fa fa-angle-double-right\"></i></button>\n        <button *ngIf=\"!last && modo==0\" matStepperNext class=\"btn btn-primary\" type=\"button\"  (click)=\"nextStep(index)\"><i class=\"fa fa-angle-double-right\"></i></button>\n        <button *ngIf=\"last\" class=\"btn btn-primary\" [disabled]=\"!form.valid\" type=\"submit\" style=\"margin-left: 1em;\">Aceptar</button>\n      </div>\n    </mat-step>\n  </mat-horizontal-stepper>\n</form>\n\n",
        styles: [".editar-item input:disabled{color:#000!important}"]
    })
], EditarComponent);

let TextoComponent = class TextoComponent extends ElementoTablaComponent {
    constructor(cd) {
        super(cd);
        this.cd = cd;
    }
    ngAfterViewInit() {
    }
    ngOnInit() {
    }
};
TextoComponent.ctorParameters = () => [
    { type: ChangeDetectorRef }
];
TextoComponent = __decorate([
    Component({
        selector: 'app-texto',
        template: "\n{{propiedad}}\n",
        styles: [""]
    })
], TextoComponent);

let BotonComponent = class BotonComponent extends ElementoTablaComponent {
    constructor(cd) {
        super(cd);
        this.cd = cd;
    }
    ngOnInit() {
    }
};
BotonComponent.ctorParameters = () => [
    { type: ChangeDetectorRef }
];
BotonComponent = __decorate([
    Component({
        selector: 'app-boton',
        template: "<button type=\"button\" [ngClass]=\"[col.clase]\">{{elemento[col.prop]}}</button>",
        styles: [""]
    })
], BotonComponent);

let FieldGenericoComponent = class FieldGenericoComponent {
    constructor(cfr) {
        this.cfr = cfr;
        this.col = { prop: '' };
    }
    ngOnInit() {
    }
    ngAfterViewInit() {
        let compFactory = this.cfr.resolveComponentFactory(this.getComponent(this.col.tipo));
        let ref = this.viewContainerRef.createComponent(compFactory);
        ref.instance.col = this.col;
        ref.instance.elemento = this.elemento;
        ref.instance.observador = this.observador;
    }
    getComponent(prop) {
        switch (prop) {
            case 'texto':
                return TextoComponent;
                break;
            case 'boton':
                return BotonComponent;
                break;
            case 'boton-icono':
                return BotonIconoComponent;
                break;
            default:
                return TextoComponent;
                break;
        }
    }
};
FieldGenericoComponent.ctorParameters = () => [
    { type: ComponentFactoryResolver }
];
__decorate([
    Input()
], FieldGenericoComponent.prototype, "col", void 0);
__decorate([
    Input()
], FieldGenericoComponent.prototype, "elemento", void 0);
__decorate([
    Input()
], FieldGenericoComponent.prototype, "observador", void 0);
__decorate([
    ViewChild('dynamic', { read: ViewContainerRef })
], FieldGenericoComponent.prototype, "viewContainerRef", void 0);
FieldGenericoComponent = __decorate([
    Component({
        selector: 'app-field-generico',
        template: " <ng-template #dynamic></ng-template>\n",
        styles: [""]
    })
], FieldGenericoComponent);

let BusquedaComponent = class BusquedaComponent {
    constructor() {
        this.texto = "";
        this.habilitado = true;
    }
    ngOnInit() {
    }
    limpiar() {
        this.texto = "";
        this.buscar(this.texto);
        this.habilitado = true;
    }
    buscar(texto) {
        this.habilitado = false;
        this.buscador.buscar(texto);
    }
};
__decorate([
    Input()
], BusquedaComponent.prototype, "buscador", void 0);
__decorate([
    Input()
], BusquedaComponent.prototype, "texto", void 0);
BusquedaComponent = __decorate([
    Component({
        selector: 'app-busqueda',
        template: "<div class=\"input-group mb-3\">  \n  <input [disabled]=\"!habilitado\" [(ngModel)]=\"texto\" type=\"text\" class=\"form-control\" placeholder=\"Buscar...\" aria-label=\"Realizar b\u00FAsqueda\" aria-describedby=\"basic-addon2\">\n  <div class=\"input-group-append\" *ngIf=\"!habilitado\">\n    <button  (click)=\"limpiar()\" class=\"btn btn-outline-danger\" type=\"button\"><i class=\"fa fa-times-circle\"></i></button>\n  </div>\n  <div class=\"input-group-append\"  *ngIf=\"habilitado\">\n    <button  (click)=\"buscar(texto)\" class=\"btn btn-outline-primary\" type=\"button\"><i class=\"fa fa-search\"></i></button>\n  </div>\n</div> ",
        styles: [""]
    })
], BusquedaComponent);

let ModalTablaComponent = class ModalTablaComponent {
    constructor(modalRef) {
        this.modalRef = modalRef;
        this.item = {};
        this.columnas = [];
        this.acciones = [];
    }
    getThis() {
        return this;
    }
    nuevo() {
        this.modalRef.hide();
        this.observador.nuevo();
    }
    editar(id) {
        this.modalRef.hide();
        this.observador.editar(id);
    }
    eliminar(id) {
        this.modalRef.hide();
        this.observador.eliminar(id);
    }
    ver(id) {
        this.modalRef.hide();
        this.observador.ver(id);
    }
    ngOnInit() {
    }
    getTitulo() {
        const titulo = this.columnas.find(col => col.titulo);
        if (titulo) {
            return this.item[titulo.prop];
        }
        return "Logisoft";
    }
};
ModalTablaComponent.ctorParameters = () => [
    { type: BsModalRef }
];
ModalTablaComponent = __decorate([
    Component({
        selector: 'app-modal-tabla',
        template: "<div class=\"modal-header\">\n  <h4 class=\"modal-title pull-left\" id=\"my-modal-title\">{{getTitulo()}}</h4>\n  <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"modalRef.hide()\">\n    <span aria-hidden=\"true\">&times;</span>\n  </button>   \n  \n</div>\n\n<div class=\"modal-body\">\n  <div class=\"col-12 \">\n    <app-field-generico class=\"float-right\" style=\"padding: 0.1rem;\" *ngFor=\"let accion of acciones\" [col]=\"accion\" [elemento]=\"item\" [observador]=\"getThis()\" ></app-field-generico>\n  </div>\n  <br>\n  \n    <ul class=\"list-group col-12\">    \n      <li *ngFor=\"let columna of columnas\" class=\"list-group-item disabled\">\n        <div class=\"row \"><h6>{{columna.nombre}}</h6></div>      \n        <div class=\"row justify-content-center align-items-center\">\n          <app-field-generico  [col]=\"columna\" [elemento]=\"item\">\n          </app-field-generico>\n        </div>\n       \n      </li>\n      \n    </ul>\n  \n  \n  \n</div>",
        styles: [""]
    })
], ModalTablaComponent);

let AccionesComponent = class AccionesComponent {
    constructor() { }
    ngOnInit() {
    }
    nuevo() {
        this.componente.nuevo();
    }
};
__decorate([
    Input()
], AccionesComponent.prototype, "componente", void 0);
AccionesComponent = __decorate([
    Component({
        selector: 'app-acciones',
        template: "<div class=\"input-group mb-3\">  \n  <button  (click)=\"nuevo()\" class=\"btn btn-outline-success\" type=\"button\">\n    <i class=\"fa fa-plus-circle\"></i>\n    <span> Nuevo</span>\n  </button>\n</div>\n",
        styles: [""]
    })
], AccionesComponent);

let PanelBotonAtrasComponent = class PanelBotonAtrasComponent {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
    }
    existePantallaAtras() {
        return LogisoftStorage.isAtras();
    }
    atras() {
        this.router.navigate([LogisoftStorage.getPantallaAtras().url]);
    }
};
PanelBotonAtrasComponent.ctorParameters = () => [
    { type: Router }
];
PanelBotonAtrasComponent = __decorate([
    Component({
        selector: 'app-panel-boton-atras',
        template: "<div class=\"row mb-4 ml-0\">\n \n    <button type=\"button\" class=\"btn btn-info\" *ngIf=\"existePantallaAtras()\" (click)=\"atras()\"><i class=\"fa fa-chevron-circle-left\"></i> Atr\u00E1s</button>\n \n</div>\n",
        styles: [""]
    })
], PanelBotonAtrasComponent);

let TablaComponent = class TablaComponent {
    constructor(injector, service, modalService, router) {
        this.injector = injector;
        this.service = service;
        this.modalService = modalService;
        this.router = router;
        this.cargando = true;
        this.data = [];
        this.titulo = '';
        this.columnas = [];
        this.acciones = [];
        this.opcionesPaginacion = {
            pageSizeOptions: [5, 10, 25, 100],
            length: 0,
            pageSize: 10
        };
        this.textoBuscar = null;
        this.columnas = this.service.getCampos().camposListado;
        this.acciones = this.service.getCampos().accionesGenerales;
        this.inicioConstructor();
        this.servicioMensajes = this.injector.get(MensajesService);
        this.cd = this.injector.get(ChangeDetectorRef);
    }
    inicioConstructor() {
        if (this.verificarPantallaAtras()) {
            return;
        }
        this.service.getPaginaServidor(0, this.opcionesPaginacion.pageSize).subscribe((r) => {
            this.data = r.content;
            this.opcionesPaginacion.length = r.totalElements;
            this.cargando = false;
        }, error => {
            console.log(error);
        });
    }
    verificarPantallaAtras() {
        if (LogisoftStorage.isAtras()) {
            if (LogisoftStorage.getPantallaAtras().url == this.router.url) {
                return true;
            }
        }
        return false;
    }
    ngOnInit() {
    }
    ngAfterViewInit() {
        if (this.verificarPantallaAtras()) {
            this.getParametrosAtras();
        }
        this.paginador.page.subscribe(this.cambioDePagina());
    }
    cambioDePagina() {
        return (r) => {
            this.paginacionConBusqueda(r);
        };
    }
    paginacionConBusqueda(r) {
        this.cargando = true;
        let parametros = this.getParametros();
        this.service.getPaginaServidorBusqueda(r.pageIndex, r.pageSize, parametros).subscribe((r) => {
            this.opcionesPaginacion.length = r.totalElements;
            this.data = r.content;
            this.cargando = false;
        }, error => {
            console.log(error);
        });
    }
    getParametros() {
        let parametros = [];
        if (!this.textoBuscar || this.textoBuscar.length == 0) {
            return parametros;
        }
        this.columnas.forEach(col => {
            if (!col.propBuscar) {
                col.propBuscar = col.prop;
            }
            parametros.push([col.propBuscar, this.textoBuscar]);
        });
        return parametros;
    }
    //para "suscribirse como observador"
    getThis() {
        return this;
    }
    //interface buscar
    buscar(texto) {
        this.textoBuscar = texto;
        this.paginador.pageIndex = 0;
        this.paginacionConBusqueda(this.paginador);
    }
    abrirInfo(item) {
        this.modalRef = this.modalService.show(ModalTablaComponent);
        this.modalRef.content.item = item;
        this.modalRef.content.columnas = this.columnas;
        this.modalRef.content.acciones = this.acciones;
        this.modalRef.content.observador = this;
    }
    //interface acciones generales
    nuevo() {
        this.setParametrosAtras();
        this.router.navigate([this.router.url + "/editar"]);
    }
    editar(id) {
        this.setParametrosAtras();
        this.router.navigate([this.router.url + "/editar/" + id]);
    }
    eliminar(id) {
        this.servicioMensajes.mostrarMensaje(TipoMensaje.ELIMINAR, new EntidadMensaje(this.service.getCampos()
            .mensaje)).then((result) => {
            if (result.value) {
                this.service.delete(id).subscribe(r => {
                    this.servicioMensajes.mostrarMensaje(TipoMensaje.EXITO_ELIMINAR, new EntidadMensaje(this.service.getCampos()
                        .mensaje));
                    this.paginacionConBusqueda(this.paginador);
                }, e => {
                    this.servicioMensajes.mostrarMensaje(TipoMensaje.ERROR, e);
                });
            }
        });
    }
    ver(id) {
        this.setParametrosAtras();
        this.router.navigate([this.router.url + "/editar/" + id + "/ver"]);
    }
    //fin interface acciones generales
    //interface pantallaAtras
    setParametrosAtras() {
        let parametros = {
            paginador: this.convertirPaginador(this.paginador),
            busqueda: this.textoBuscar
        };
        LogisoftStorage.setPantallaAtras({
            url: this.router.url,
            parametros: parametros
        });
    }
    convertirPaginador(paginador) {
        return {
            length: paginador.length,
            pageIndex: paginador.pageIndex,
            pageSize: paginador.pageSize,
            previousPageIndex: 0
        };
    }
    getParametrosAtras() {
        let pantallaAtras = LogisoftStorage.getPantallaAtras();
        this.textoBuscar = pantallaAtras.parametros.busqueda;
        if (this.textoBuscar && this.textoBuscar.length > 0) {
            //seteo el texto en el buscador
            this.busqueda.texto = this.textoBuscar;
            this.busqueda.habilitado = false;
        }
        let paginador = pantallaAtras.parametros.paginador;
        this.paginador.length = paginador.length;
        this.paginador.pageIndex = paginador.pageIndex;
        this.paginador.pageSize = paginador.pageSize;
        this.cd.detectChanges();
        this.paginacionConBusqueda(this.paginador);
        LogisoftStorage.borrarPantallaAtras();
    }
};
TablaComponent.ctorParameters = () => [
    { type: Injector },
    { type: GenericoService },
    { type: BsModalService },
    { type: Router }
];
__decorate([
    ViewChild(MatPaginator)
], TablaComponent.prototype, "paginador", void 0);
__decorate([
    ViewChild(BusquedaComponent)
], TablaComponent.prototype, "busqueda", void 0);
TablaComponent = __decorate([
    Component({
        selector: 'app-tabla',
        template: "<h4 class=\"text-muted mb-4\">{{titulo}} </h4>\n<div>\n  <div class=\"card border-0 rounded-0 col-md-12\">\n    <div class=\"card-body cuerpo\" style=\"padding-left: 0px;\n    padding-right: 0px;\">\n    <div class=\"row\">\n      <div class=\"col-12 col-md-6\">\n        <app-busqueda [buscador]=\"getThis()\" ></app-busqueda>  \n      </div>\n      <div class=\"col-12 col-md-6\">\n        <app-acciones [componente]=\"getThis()\"></app-acciones>\n      </div>\n    </div> \n     \n      <div  class=\"table-responsive-md\">\n        <table class=\"table table-hover\">\n          <thead>\n            <tr>\n              <th scope=\"col\" *ngFor=\"let columna of columnas\" [ngClass]=\"{'d-none d-sm-none d-md-block':columna.ocultarMovil}\">{{columna.nombre}}</th>\n              <th scope=\"col\" [ngClass]=\"{'d-block d-sm-block d-md-none':true}\">Info</th>\n              <th scope=\"col\" [ngClass]=\"{'d-none d-sm-none d-md-block':true}\"  *ngIf=\"acciones.length>0\">Acciones</th>\n            </tr>\n          </thead>\n          <mat-progress-bar mode=\"indeterminate\" [hidden]=\"!cargando\"></mat-progress-bar>\n          <tbody [hidden]=\"cargando\">            \n            <tr *ngFor=\"let item of data\">\n              <td *ngFor=\"let columna of columnas\" [ngClass]=\"{'d-none d-sm-none d-md-block':columna.ocultarMovil}\">\n                <app-field-generico [col]=\"columna\" [elemento]=\"item\">\n                </app-field-generico>\n              </td>              \n              <td  [ngClass]=\"{'d-block d-sm-block d-md-none':true}\">\n                <button (click)=\"abrirInfo(item)\" type=\"button\" class=\"btn btn-outline-success\"><i class=\"fa fa-info-circle\"></i></button>\n              </td>\n              <td [ngClass]=\"{'d-none d-sm-none d-md-block':true}\" *ngIf=\"acciones.length>0\">                \n                  <app-field-generico style=\"padding: 0.1rem;\" *ngFor=\"let accion of acciones\" [col]=\"accion\" [elemento]=\"item\" [observador]=\"getThis()\" ></app-field-generico>\n              </td>\n            </tr>\n          </tbody>\n        </table>\n      </div>\n    </div>\n  </div>\n</div>\n<mat-paginator [length]=\"opcionesPaginacion.length\" [pageSize]=\"opcionesPaginacion.pageSize\"\n  [pageSizeOptions]=\"opcionesPaginacion.pageSizeOptions\">\n</mat-paginator>\n\n",
        styles: [".cuerpo{padding-left:0!important;padding-right:0!important}"]
    })
], TablaComponent);

let ListaComponent = class ListaComponent {
    constructor() { }
    ngOnInit() {
    }
};
ListaComponent = __decorate([
    Component({
        selector: 'app-lista',
        template: "<p>\n  lista works!\n</p>\n",
        styles: [""]
    })
], ListaComponent);

let MostrarTextoComponent = class MostrarTextoComponent {
    constructor() { }
    ngOnInit() {
    }
};
MostrarTextoComponent = __decorate([
    Component({
        selector: 'app-mostrar-texto',
        template: "<p>\n  mostrar-texto works!\n</p>\n",
        styles: [""]
    })
], MostrarTextoComponent);

let MaterialModule = class MaterialModule {
};
MaterialModule = __decorate([
    NgModule({
        declarations: [],
        imports: [
            CommonModule,
            ReactiveFormsModule,
            MatAutocompleteModule,
            FormsModule,
            MatPaginatorModule,
            MatProgressBarModule,
            MatStepperModule,
            MatFormFieldModule,
            MatInputModule,
            FormlyMaterialModule,
            FormlyMatToggleModule,
            MatListModule,
            MatTableModule,
            MatIconModule
        ],
        exports: [
            ReactiveFormsModule,
            MatAutocompleteModule,
            FormsModule,
            MatPaginatorModule,
            MatProgressBarModule,
            MatStepperModule,
            MatFormFieldModule,
            MatInputModule,
            FormlyMaterialModule,
            FormlyMatToggleModule,
            MatListModule,
            MatTableModule,
            MatIconModule
        ]
    })
], MaterialModule);

let AutocompleteTypeComponent = class AutocompleteTypeComponent extends FieldType {
    ngOnInit() {
        super.ngOnInit();
        this.filter = this.formControl.valueChanges
            .pipe(startWith(''), switchMap(term => this.to.filter(term)));
    }
    ngAfterViewInit() {
        super.ngAfterViewInit();
        // temporary fix for https://github.com/angular/material2/issues/6728
        this.autocomplete._formField = this.formField;
    }
    borrar() {
        this.value = "";
    }
    seleccionar(value) {
    }
};
__decorate([
    ViewChild(MatInput)
], AutocompleteTypeComponent.prototype, "formFieldControl", void 0);
__decorate([
    ViewChild(MatAutocompleteTrigger)
], AutocompleteTypeComponent.prototype, "autocomplete", void 0);
AutocompleteTypeComponent = __decorate([
    Component({
        selector: 'formly-autocomplete-type',
        template: "  <div class=\"row\">\n    \n    \n     <input class=\"col\" style=\"padding-left: 1rem;\" matInput \n      [matAutocomplete]=\"auto\"\n      [formControl]=\"formControl\"\n      [formlyAttributes]=\"field\"\n      [placeholder]=\"to.placeholder\"\n      [errorStateMatcher]=\"errorStateMatcher\"\n      >\n      <div class=\"col-1\" style=\"color: red;\">\n        <button  style=\"padding-left: 0px;\" class=\"btn btn-link \" *ngIf=\"value\" aria-label=\"Clear\" (click)=\"borrar()\">\n          <i class=\"fa fa-trash\"></i>\n         </button>\n      </div>\n      <mat-autocomplete  #auto=\"matAutocomplete\">\n      \n        <mat-option  (click)=\"seleccionar(value)\" *ngFor=\"let value of filter | async\" [value]=\"value\">\n          {{ value }}\n        </mat-option>      \n      </mat-autocomplete>\n  </div>\n  \n\n \n  \n  \n    \n"
    })
], AutocompleteTypeComponent);

let GenericoModule = class GenericoModule {
};
GenericoModule = __decorate([
    NgModule({
        declarations: [ListaComponent, TablaComponent, TextoComponent, FieldGenericoComponent, ElementoTablaComponent, BotonComponent, BusquedaComponent, ModalTablaComponent, AccionesComponent, BotonIconoComponent, EditarComponent, MostrarTextoComponent, AutocompleteTypeComponent, PanelBotonAtrasComponent],
        imports: [
            CommonModule,
            ReactiveFormsModule,
            FormlyModule.forRoot({ types: [{
                        name: 'autocomplete',
                        component: AutocompleteTypeComponent,
                        wrappers: ['form-field'],
                    }],
                validationMessages: [
                    { name: 'required', message: 'Campo requerido.' },
                ] }),
            MaterialModule
        ],
        exports: [FormlyModule, TablaComponent, TextoComponent, FieldGenericoComponent, BotonComponent, BusquedaComponent, ModalTablaComponent, AccionesComponent, MaterialModule, PanelBotonAtrasComponent, MaterialModule]
    })
], GenericoModule);

/*
 * Public API Surface of ivai-generico-libreria
 */

/**
 * Generated bundle index. Do not edit.
 */

export { AccionesComponent, BotonComponent, BotonIconoComponent, BusquedaComponent, Configuracion, EditarComponent, FieldGenericoComponent, GenericoModule, GenericoService, LogisoftStorage, MaterialModule, MensajesService, ModalTablaComponent, PanelBotonAtrasComponent, TablaComponent, TextoComponent, ElementoTablaComponent as ɵa, ListaComponent as ɵb, MostrarTextoComponent as ɵc, AutocompleteTypeComponent as ɵd };
//# sourceMappingURL=ivai-generico-libreria.js.map
