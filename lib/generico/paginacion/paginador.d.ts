export declare class Paginador {
    length: number;
    pageIndex: number;
    pageSize: number;
    previousPageIndex: number;
}
