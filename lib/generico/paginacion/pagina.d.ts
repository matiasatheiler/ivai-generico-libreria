export declare class Pagina<T> {
    content: T[];
    totalPages: number;
    totalElements: number;
    last: boolean;
    first: boolean;
    numberOfElements: number;
    sort: any;
    size: number;
    number: number;
}
