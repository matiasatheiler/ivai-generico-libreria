import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Servicio } from './servicio';
import { Pagina } from '../paginacion/pagina';
export declare class GenericoService<T> implements Servicio<T> {
    http: HttpClient;
    size: number;
    url: string;
    urlObjeto: string;
    json: any;
    constructor(http: HttpClient);
    get(id: any): Observable<any>;
    getAll(): Observable<Object>;
    getPagina(): Observable<Pagina<T>>;
    getPaginaServidor(pageIndex: number, pageSize: number): Observable<Pagina<T>>;
    getPaginaServidorBusqueda(pageIndex: number, pageSize: number, parametros: any): Observable<Pagina<T>>;
    getUrl(): string;
    getCampos(): any;
    nuevo(objeto: T): Observable<Object>;
    delete(id: any): Observable<Object>;
}
