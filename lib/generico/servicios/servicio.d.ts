import { Observable } from 'rxjs';
import { Pagina } from '../paginacion/pagina';
export interface Servicio<T> {
    getAll(): any;
    getPagina(): Observable<Pagina<T>>;
    getPaginaServidor(pageIndex: number, pageSize: number): Observable<Pagina<T>>;
    getPaginaServidorBusqueda(pageIndex: number, pageSize: number, parametros: any): Observable<Pagina<T>>;
    getUrl(): string;
    getCampos(): any;
}
