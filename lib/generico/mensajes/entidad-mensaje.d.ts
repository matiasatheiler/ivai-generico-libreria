export declare class EntidadMensaje {
    articulo: string;
    singular: string;
    articuloPlural: string;
    plural: string;
    femenino: boolean;
    constructor(entidadO: any);
    getSingular(): string;
    getPlural(): string;
    getSingularConArticulo(): string;
    getPluralConArticulo(): string;
}
