import { SweetAlertResult } from 'sweetalert2';
import { EntidadMensaje } from './entidad-mensaje';
export declare enum TipoMensaje {
    EXITO = "EXITO",
    ERROR = "ERROR",
    ELIMINAR = "ELIMINAR",
    EXITO_ELIMINAR = "EXITO_ELIMINAR"
}
export declare class MensajesService {
    jsonMensajes: {
        "CARGANDO": {
            "title": string;
            "width": number;
            "padding": string;
            "background": string;
            "backdrop": string;
        };
        "EXITO": {
            "icon": string;
            "title": string;
            "text": string;
        };
        "EXITO_ELIMINAR": {
            "icon": string;
            "title": string;
            "text": string;
        };
        "ERROR": {
            "icon": string;
            "title": string;
            "text": string;
        };
        "ELIMINAR": {
            "title": string;
            "text": string;
            "icon": string;
            "showCancelButton": boolean;
            "confirmButtonColor": string;
            "cancelButtonColor": string;
            "confirmButtonText": string;
        };
    };
    constructor();
    mostrarMensaje(tipo: TipoMensaje, entidadMensaje: EntidadMensaje): Promise<SweetAlertResult>;
    mensajeExitoEliminar(entidadMensaje: EntidadMensaje): Promise<SweetAlertResult>;
    mensajeEliminar(entidadMensaje: EntidadMensaje): Promise<SweetAlertResult>;
    mensajeExito(entidadMensaje: EntidadMensaje): Promise<SweetAlertResult>;
    getMensajes(): {
        "CARGANDO": {
            "title": string;
            "width": number;
            "padding": string;
            "background": string;
            "backdrop": string;
        };
        "EXITO": {
            "icon": string;
            "title": string;
            "text": string;
        };
        "EXITO_ELIMINAR": {
            "icon": string;
            "title": string;
            "text": string;
        };
        "ERROR": {
            "icon": string;
            "title": string;
            "text": string;
        };
        "ELIMINAR": {
            "title": string;
            "text": string;
            "icon": string;
            "showCancelButton": boolean;
            "confirmButtonColor": string;
            "cancelButtonColor": string;
            "confirmButtonText": string;
        };
    };
}
