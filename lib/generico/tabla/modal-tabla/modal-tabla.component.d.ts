import { OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/public_api';
import { AccionesGenerales } from '../../formlys/acciones/acciones-generales';
import { TablaComponent } from '../tabla.component';
export declare class ModalTablaComponent<T> implements OnInit, AccionesGenerales {
    modalRef: BsModalRef;
    item: {};
    columnas: any[];
    acciones: any[];
    observador: TablaComponent<T>;
    constructor(modalRef: BsModalRef);
    getThis(): this;
    nuevo(): void;
    editar(id: any): void;
    eliminar(id: any): void;
    ver(id: any): void;
    ngOnInit(): void;
    getTitulo(): string;
}
