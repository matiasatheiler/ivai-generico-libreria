import { OnInit } from '@angular/core';
import { Router } from '@angular/router';
export declare class PanelBotonAtrasComponent implements OnInit {
    private router;
    constructor(router: Router);
    ngOnInit(): void;
    existePantallaAtras(): boolean;
    atras(): void;
}
