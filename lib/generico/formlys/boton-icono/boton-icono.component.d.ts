import { OnInit, ChangeDetectorRef } from '@angular/core';
import { ElementoTablaComponent } from '../elemento-tabla/elemento-tabla.component';
import { BotonIconoEstilo } from './boton-icono-estilo';
export declare class BotonIconoComponent extends ElementoTablaComponent implements OnInit {
    cd: ChangeDetectorRef;
    constructor(cd: ChangeDetectorRef);
    ngOnInit(): void;
    getClase(): BotonIconoEstilo;
    click(): void;
    ejecutarFuncion(item: any): void;
    ejecutarFuncionGenerico(): void;
}
