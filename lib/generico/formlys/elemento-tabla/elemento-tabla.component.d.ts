import { OnInit, ChangeDetectorRef } from '@angular/core';
import { AccionesGenerales } from '../acciones/acciones-generales';
export declare class ElementoTablaComponent implements OnInit {
    cd: ChangeDetectorRef;
    eBoolean: boolean;
    propiedad: any;
    constructor(cd: ChangeDetectorRef);
    ngOnInit(): void;
    observador: AccionesGenerales;
    private _col;
    private _elemento;
    get elemento(): {
        dummy: any;
    };
    set elemento(value: {
        dummy: any;
    });
    get col(): {
        prop: string;
        clase: string;
        icon: any;
        tipo: any;
        estilo: any;
    };
    set col(value: {
        prop: string;
        clase: string;
        icon: any;
        tipo: any;
        estilo: any;
    });
    setPropiedadChange(): void;
}
