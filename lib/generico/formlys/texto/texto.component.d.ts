import { OnInit, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { ElementoTablaComponent } from '../elemento-tabla/elemento-tabla.component';
export declare class TextoComponent extends ElementoTablaComponent implements OnInit, AfterViewInit {
    cd: ChangeDetectorRef;
    constructor(cd: ChangeDetectorRef);
    ngAfterViewInit(): void;
    ngOnInit(): void;
}
