import { OnInit, ViewContainerRef, Type, AfterViewInit, ComponentFactoryResolver } from '@angular/core';
import { ElementoTablaComponent } from '../elemento-tabla/elemento-tabla.component';
import { AccionesGenerales } from '../acciones/acciones-generales';
export declare class FieldGenericoComponent implements OnInit, AfterViewInit {
    private cfr;
    col: any;
    elemento: any;
    observador: AccionesGenerales;
    viewContainerRef: ViewContainerRef;
    constructor(cfr: ComponentFactoryResolver);
    ngOnInit(): void;
    ngAfterViewInit(): void;
    getComponent(prop: string): Type<ElementoTablaComponent>;
}
