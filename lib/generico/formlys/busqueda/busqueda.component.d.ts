import { OnInit } from '@angular/core';
import { Buscador } from './buscador';
export declare class BusquedaComponent implements OnInit {
    buscador: Buscador;
    texto: string;
    habilitado: boolean;
    constructor();
    ngOnInit(): void;
    limpiar(): void;
    buscar(texto: any): void;
}
