export interface AccionesGenerales {
    nuevo(): any;
    editar(id: any): any;
    eliminar(id: any): any;
    ver(id: any): any;
}
