import { OnInit, AfterViewInit } from '@angular/core';
import { FieldType } from '@ngx-formly/material';
import { MatInput } from '@angular/material/input';
import { MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { Observable } from 'rxjs';
export declare class AutocompleteTypeComponent extends FieldType implements OnInit, AfterViewInit {
    formFieldControl: MatInput;
    autocomplete: MatAutocompleteTrigger;
    filter: Observable<any>;
    ngOnInit(): void;
    ngAfterViewInit(): void;
    borrar(): void;
    seleccionar(value: any): void;
}
