import { OnInit, ChangeDetectorRef } from '@angular/core';
import { ElementoTablaComponent } from '../elemento-tabla/elemento-tabla.component';
export declare class BotonComponent extends ElementoTablaComponent implements OnInit {
    cd: ChangeDetectorRef;
    constructor(cd: ChangeDetectorRef);
    ngOnInit(): void;
}
