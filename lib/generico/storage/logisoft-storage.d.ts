import { PantallaAtrasStorage } from '../boton-atras/pantalla-atras-storage';
export declare class LogisoftStorage {
    static borrarPantallaAtras(): void;
    static getPantallaAtras(): PantallaAtrasStorage;
    static isAtras(): boolean;
    static setPantallaAtras(pantalla: PantallaAtrasStorage): void;
    static isUsuario(): boolean;
    static cerrarSesion(): void;
    static getNombreUsuario(): string;
}
