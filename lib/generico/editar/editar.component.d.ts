import { OnInit, Injector, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { FormArray } from '@angular/forms';
import { Location } from '@angular/common';
import { GenericoService } from '../servicios/generico.service';
import { MensajesService } from '../mensajes/mensajes.service';
export interface StepType {
    label: string;
    fields: FormlyFieldConfig[];
}
export declare class EditarComponent<T> implements OnInit, OnChanges {
    service: GenericoService<T>;
    injector: Injector;
    rutaActiva: ActivatedRoute;
    serviciosConfig: any;
    constructor(service: GenericoService<T>, injector: Injector, rutaActiva: ActivatedRoute);
    jsonEditar: any;
    cargando: number;
    modo: number;
    activedStep: number;
    model: {};
    servicioMensajes: MensajesService;
    location: Location;
    steps: StepType[];
    form: FormArray;
    options: FormlyFormOptions[];
    ngOnChanges(changes: SimpleChanges): void;
    ngOnInit(): void;
    procesarTipoSelectMultiple(field: any): void;
    procesarTipoAutocomplete(field: any): void;
    procesarTipoSelect(field: any): void;
    setearModo(): void;
    modoEditar(id: any): void;
    convertirToString(r: any): any;
    convertirOpciones(r: any, opcion: any): any;
    prevStep(step: any): void;
    nextStep(step: any): void;
    submit(): void;
    compareWith: (o1: any, o2: any) => boolean;
    getTexto(field: any): any;
    getLabel(field: any): any;
    getTerm(servicioString: any, term: any, arg: any): any;
    convertirRespuestaAutocomplete(array: any, arg: string): any;
}
