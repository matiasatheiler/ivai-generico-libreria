import { BotonIconoEstilo } from './formlys/boton-icono/boton-icono-estilo';
export declare class Configuracion {
    static MODOS: {
        VER: number;
        EDITAR: number;
        NUEVO: number;
    };
    static obtenerClaseIcono(tipo: string): BotonIconoEstilo;
}
