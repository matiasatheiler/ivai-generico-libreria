export { LogisoftStorage } from './lib/generico/storage/logisoft-storage';
export { Configuracion } from './lib/generico/configuracion';
export { GenericoService } from './lib/generico/servicios/generico.service';
export { MensajesService } from './lib/generico/mensajes/mensajes.service';
export { BotonIconoComponent } from './lib/generico/formlys/boton-icono/boton-icono.component';
export { EditarComponent } from './lib/generico/editar/editar.component';
export { TextoComponent } from './lib/generico/formlys/texto/texto.component';
export { FieldGenericoComponent } from './lib/generico/formlys/field-generico/field-generico.component';
export { BotonComponent } from './lib/generico/formlys/boton/boton.component';
export { BusquedaComponent } from './lib/generico/formlys/busqueda/busqueda.component';
export { ModalTablaComponent } from './lib/generico/tabla/modal-tabla/modal-tabla.component';
export { AccionesComponent } from './lib/generico/formlys/acciones/acciones.component';
export { PanelBotonAtrasComponent } from './lib/generico/boton-atras/panel-boton-atras/panel-boton-atras.component';
export { TablaComponent } from './lib/generico/tabla/tabla.component';
export * from './lib/generico/generico.module';
export * from './lib/material/material.module';
