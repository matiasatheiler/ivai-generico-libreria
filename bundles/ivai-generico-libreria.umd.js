(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common/http'), require('sweetalert2'), require('@angular/router'), require('@angular/forms'), require('@angular/common'), require('rxjs/operators'), require('ngx-bootstrap/modal/public_api'), require('@angular/material/paginator'), require('ngx-bootstrap/modal'), require('@ngx-formly/core'), require('@angular/material/progress-bar'), require('@angular/material/stepper'), require('@angular/material/form-field'), require('@angular/material/input'), require('@ngx-formly/material'), require('@ngx-formly/material/toggle'), require('@angular/material/list'), require('@angular/material/table'), require('@angular/material/autocomplete'), require('@angular/material/icon')) :
    typeof define === 'function' && define.amd ? define('ivai-generico-libreria', ['exports', '@angular/core', '@angular/common/http', 'sweetalert2', '@angular/router', '@angular/forms', '@angular/common', 'rxjs/operators', 'ngx-bootstrap/modal/public_api', '@angular/material/paginator', 'ngx-bootstrap/modal', '@ngx-formly/core', '@angular/material/progress-bar', '@angular/material/stepper', '@angular/material/form-field', '@angular/material/input', '@ngx-formly/material', '@ngx-formly/material/toggle', '@angular/material/list', '@angular/material/table', '@angular/material/autocomplete', '@angular/material/icon'], factory) :
    (global = global || self, factory(global['ivai-generico-libreria'] = {}, global.ng.core, global.ng.common.http, global.Swal, global.ng.router, global.ng.forms, global.ng.common, global.rxjs.operators, global.public_api, global.ng.material.paginator, global.modal, global.core$1, global.ng.material.progressBar, global.ng.material.stepper, global.ng.material.formField, global.ng.material.input, global.material, global.toggle, global.ng.material.list, global.ng.material.table, global.ng.material.autocomplete, global.ng.material.icon));
}(this, (function (exports, core, http, Swal, router, forms, common, operators, public_api, paginator, modal, core$1, progressBar, stepper, formField, input, material, toggle, list, table, autocomplete, icon) { 'use strict';

    Swal = Swal && Object.prototype.hasOwnProperty.call(Swal, 'default') ? Swal['default'] : Swal;

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __createBinding(o, m, k, k2) {
        if (k2 === undefined) k2 = k;
        o[k2] = m[k];
    }

    function __exportStar(m, exports) {
        for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m) return m.call(o);
        if (o && typeof o.length === "number") return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }

    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    var LogisoftStorage = /** @class */ (function () {
        function LogisoftStorage() {
        }
        LogisoftStorage.borrarPantallaAtras = function () {
            localStorage.removeItem('atras');
        };
        LogisoftStorage.getPantallaAtras = function () {
            return JSON.parse(localStorage.getItem('atras'));
        };
        LogisoftStorage.isAtras = function () {
            if (localStorage.getItem('atras')) {
                return true;
            }
            return false;
        };
        LogisoftStorage.setPantallaAtras = function (pantalla) {
            localStorage.setItem('atras', JSON.stringify(pantalla));
        };
        LogisoftStorage.isUsuario = function () {
            if (localStorage.getItem('currentUser')) {
                return true;
            }
            return false;
        };
        LogisoftStorage.cerrarSesion = function () {
            localStorage.removeItem('currentUser');
        };
        LogisoftStorage.getNombreUsuario = function () {
            if (!localStorage.getItem('currentUser')) {
                return '';
            }
            return JSON.parse(localStorage.getItem('currentUser')).nombre;
        };
        return LogisoftStorage;
    }());

    var BotonIconoEstilo = /** @class */ (function () {
        function BotonIconoEstilo(icono, clase) {
            this.icono = icono;
            this.clase = clase;
        }
        return BotonIconoEstilo;
    }());

    var Configuracion = /** @class */ (function () {
        function Configuracion() {
        }
        Configuracion.obtenerClaseIcono = function (tipo) {
            switch (tipo) {
                case "editar":
                    return new BotonIconoEstilo("fa fa-edit", "btn btn-primary");
                case "eliminar":
                    return new BotonIconoEstilo("fa fa-trash", "btn btn-danger");
                case "ver":
                    return new BotonIconoEstilo("fa fa-search", "btn btn-info");
                default:
                    return new BotonIconoEstilo("fa fa-fingerprint", "btn btn-warning");
            }
        };
        Configuracion.MODOS = { VER: 0, EDITAR: 1, NUEVO: 2 };
        return Configuracion;
    }());

    var GenericoService = /** @class */ (function () {
        function GenericoService(http) {
            this.http = http;
            this.size = 10;
            this.urlObjeto = '';
            this.json = {};
        }
        GenericoService.prototype.get = function (id) {
            return this.http.get(this.getUrl() + "/" + id);
        };
        GenericoService.prototype.getAll = function () {
            return this.http.get(this.getUrl());
        };
        GenericoService.prototype.getPagina = function () {
            return this.getPaginaServidor(0, this.size);
        };
        GenericoService.prototype.getPaginaServidor = function (pageIndex, pageSize) {
            var params = new http.HttpParams()
                .set('page', pageIndex.toString())
                .set('pagination', 'true')
                .set('size', pageSize.toString())
                .set('sort', 'id,asc');
            return this.http.get(this.getUrl(), { params: params });
        };
        GenericoService.prototype.getPaginaServidorBusqueda = function (pageIndex, pageSize, parametros) {
            var params = new http.HttpParams()
                .set('page', pageIndex.toString())
                .set('pagination', 'true')
                .set('size', pageSize.toString())
                .set('sort', 'id,asc');
            for (var i = 0; i < parametros.length; i++) {
                var p = parametros[i];
                params = params.append(p[0], p[1]);
            }
            return this.http.get(this.getUrl(), { params: params });
        };
        GenericoService.prototype.getUrl = function () {
            return this.url + this.urlObjeto;
        };
        GenericoService.prototype.getCampos = function () {
            return this.json;
        };
        GenericoService.prototype.nuevo = function (objeto) {
            return this.http.post(this.getUrl(), objeto);
        };
        GenericoService.prototype.delete = function (id) {
            return this.http.delete(this.getUrl() + "/" + id);
        };
        GenericoService.ctorParameters = function () { return [
            { type: http.HttpClient }
        ]; };
        GenericoService.ɵprov = core.ɵɵdefineInjectable({ factory: function GenericoService_Factory() { return new GenericoService(core.ɵɵinject(http.HttpClient)); }, token: GenericoService, providedIn: "root" });
        GenericoService = __decorate([
            core.Injectable({
                providedIn: 'root'
            })
        ], GenericoService);
        return GenericoService;
    }());

    var TipoMensaje;
    (function (TipoMensaje) {
        TipoMensaje["EXITO"] = "EXITO";
        TipoMensaje["ERROR"] = "ERROR";
        TipoMensaje["ELIMINAR"] = "ELIMINAR";
        TipoMensaje["EXITO_ELIMINAR"] = "EXITO_ELIMINAR";
    })(TipoMensaje || (TipoMensaje = {}));
    var MensajesService = /** @class */ (function () {
        function MensajesService() {
            this.jsonMensajes = this.getMensajes();
        }
        MensajesService.prototype.mostrarMensaje = function (tipo, entidadMensaje) {
            switch (tipo) {
                case TipoMensaje.EXITO:
                    return this.mensajeExito(entidadMensaje);
                case TipoMensaje.ELIMINAR:
                    return this.mensajeEliminar(entidadMensaje);
                case TipoMensaje.EXITO_ELIMINAR:
                    return this.mensajeExitoEliminar(entidadMensaje);
                default:
                    break;
            }
        };
        MensajesService.prototype.mensajeExitoEliminar = function (entidadMensaje) {
            var mensaje = Object.assign({}, this.jsonMensajes.EXITO_ELIMINAR);
            mensaje.text = entidadMensaje.getSingularConArticulo() + mensaje.text;
            return Swal.fire(mensaje);
        };
        MensajesService.prototype.mensajeEliminar = function (entidadMensaje) {
            var mensaje = Object.assign({}, this.jsonMensajes.ELIMINAR);
            mensaje.text = mensaje.text + entidadMensaje.getSingularConArticulo();
            return Swal.fire(mensaje);
        };
        MensajesService.prototype.mensajeExito = function (entidadMensaje) {
            var mensaje = Object.assign({}, this.jsonMensajes.EXITO);
            mensaje.text = entidadMensaje.getSingularConArticulo() + mensaje.text;
            return Swal.fire(mensaje);
        };
        MensajesService.prototype.getMensajes = function () {
            return {
                "CARGANDO": {
                    "title": "Cargando..!!!",
                    "width": 600,
                    "padding": "3em",
                    "background": "#fff",
                    "backdrop": "rgba(0,0,0,0.7)"
                },
                "EXITO": {
                    "icon": "success",
                    "title": "Carga Exitosa",
                    "text": " se guardo correctamente"
                },
                "EXITO_ELIMINAR": {
                    "icon": "success",
                    "title": "Eliminación Exitosa",
                    "text": " se ha eliminado correctamente"
                },
                "ERROR": {
                    "icon": "error",
                    "title": "Error",
                    "text": "Algo salió mal!"
                },
                "ELIMINAR": {
                    "title": "Eliminar",
                    "text": "¿ Esta seguro que desea eliminar ?",
                    "icon": "warning",
                    "showCancelButton": true,
                    "confirmButtonColor": "#3085d6",
                    "cancelButtonColor": "#d33",
                    "confirmButtonText": "Si"
                }
            };
        };
        MensajesService.ɵprov = core.ɵɵdefineInjectable({ factory: function MensajesService_Factory() { return new MensajesService(); }, token: MensajesService, providedIn: "root" });
        MensajesService = __decorate([
            core.Injectable({
                providedIn: 'root'
            })
        ], MensajesService);
        return MensajesService;
    }());

    var ElementoTablaComponent = /** @class */ (function () {
        function ElementoTablaComponent(cd) {
            this.cd = cd;
            this.eBoolean = false;
            this._col = { prop: "dummy.", clase: "false", icon: null, tipo: null, estilo: null };
            this._elemento = { dummy: null };
        }
        ElementoTablaComponent.prototype.ngOnInit = function () {
        };
        Object.defineProperty(ElementoTablaComponent.prototype, "elemento", {
            get: function () {
                return this._elemento;
            },
            set: function (value) {
                this._elemento = value;
                this.setPropiedadChange();
                this.cd.detectChanges();
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ElementoTablaComponent.prototype, "col", {
            get: function () {
                return this._col;
            },
            set: function (value) {
                this._col = value;
                this.cd.detectChanges();
            },
            enumerable: true,
            configurable: true
        });
        ElementoTablaComponent.prototype.setPropiedadChange = function () {
            if (!this.col.prop) {
                return;
            }
            var props = this.col.prop.split(".");
            var e = this.elemento;
            for (var i = 0; i < props.length; i++) {
                var p = props[i];
                e = e[p];
                if (!e) {
                    this.propiedad = "";
                    return;
                }
            }
            this.propiedad = e;
        };
        ElementoTablaComponent.ctorParameters = function () { return [
            { type: core.ChangeDetectorRef }
        ]; };
        ElementoTablaComponent = __decorate([
            core.Component({
                selector: 'app-elemento-tabla',
                template: "<p>\n  elemento-tabla works!\n</p>\n",
                styles: [""]
            })
        ], ElementoTablaComponent);
        return ElementoTablaComponent;
    }());

    var BotonIconoComponent = /** @class */ (function (_super) {
        __extends(BotonIconoComponent, _super);
        function BotonIconoComponent(cd) {
            var _this = _super.call(this, cd) || this;
            _this.cd = cd;
            return _this;
        }
        BotonIconoComponent.prototype.ngOnInit = function () {
        };
        BotonIconoComponent.prototype.getClase = function () {
            if (this.col.estilo != null) {
                return Configuracion.obtenerClaseIcono(this.col.estilo);
            }
            else {
                return new BotonIconoEstilo(this.col.icon, this.col.clase);
            }
        };
        BotonIconoComponent.prototype.click = function () {
            if (this.col.estilo) {
                this.ejecutarFuncionGenerico();
            }
            else {
                this.ejecutarFuncion(this.elemento);
            }
        };
        BotonIconoComponent.prototype.ejecutarFuncion = function (item) {
            throw new Error("Method not implemented.");
        };
        BotonIconoComponent.prototype.ejecutarFuncionGenerico = function () {
            switch (this.col.estilo) {
                case "editar":
                    this.observador.editar(this.elemento.id);
                    break;
                case "eliminar":
                    this.observador.eliminar(this.elemento.id);
                    break;
                case "ver":
                    this.observador.ver(this.elemento.id);
                    break;
                default:
                    console.log("no esta implementado el metodo " + this.col.estilo);
                    break;
            }
        };
        BotonIconoComponent.ctorParameters = function () { return [
            { type: core.ChangeDetectorRef }
        ]; };
        BotonIconoComponent = __decorate([
            core.Component({
                selector: 'app-boton-icono',
                template: "<button type=\"button\" (click)=\"click()\" [ngClass]=\"[getClase().clase]\"><i [ngClass]=\"[getClase().icono]\"></i></button>",
                styles: [""]
            })
        ], BotonIconoComponent);
        return BotonIconoComponent;
    }(ElementoTablaComponent));

    var EntidadMensaje = /** @class */ (function () {
        function EntidadMensaje(entidadO) {
            var _this = this;
            this.articulo = null;
            this.singular = null;
            this.articuloPlural = null;
            this.plural = null;
            this.femenino = false;
            //recorro las propiedades del nuevo objeto
            //este metodo se utiliza para sobreescribir las propiedades del objeto        
            Object.keys(this).forEach(function (k) {
                console.log(Object.keys(entidadO).indexOf(k));
                if (Object.keys(entidadO).includes(k)) {
                    _this[k] = entidadO[k];
                }
            });
        }
        EntidadMensaje.prototype.getSingular = function () {
            return this.singular;
        };
        EntidadMensaje.prototype.getPlural = function () {
            if (this.plural != null) {
                return this.plural;
            }
            return this.singular + "s";
        };
        EntidadMensaje.prototype.getSingularConArticulo = function () {
            if (this.articulo != null) {
                return this.articulo + " " + this.getSingular();
            }
            this.articulo = "El";
            if (this.femenino) {
                this.articulo = "La";
            }
            return this.articulo + " " + this.getSingular();
        };
        EntidadMensaje.prototype.getPluralConArticulo = function () {
            if (this.articuloPlural != null) {
                return this.articuloPlural + " " + this.getPlural();
            }
            this.articuloPlural = "Los";
            if (this.femenino) {
                this.articulo = "Las";
            }
            return this.articuloPlural + " " + this.getPlural();
        };
        return EntidadMensaje;
    }());

    var EditarComponent = /** @class */ (function () {
        function EditarComponent(service, injector, rutaActiva) {
            this.service = service;
            this.injector = injector;
            this.rutaActiva = rutaActiva;
            this.cargando = 0;
            this.activedStep = 0;
            this.model = {};
            this.steps = [];
            this.form = new forms.FormArray(this.steps.map(function () { return new forms.FormGroup({}); }), forms.Validators.required);
            this.options = this.steps.map(function () { return ({}); });
            this.compareWith = function (o1, o2) {
                if (!o2 && !o1) {
                    return true;
                }
                if (!o2) {
                    return false;
                }
                return o1.id === o2.id;
            };
            this.servicioMensajes = this.injector.get(MensajesService);
            this.location = this.injector.get(common.Location);
        }
        EditarComponent.prototype.ngOnChanges = function (changes) {
            console.log(changes);
        };
        EditarComponent.prototype.ngOnInit = function () {
            var _this = this;
            this.setearModo();
            this.jsonEditar = this.service.json.camposEditar;
            this.jsonEditar.forEach(function (step) {
                step.fields.forEach(function (field) {
                    if (_this.modo == Configuracion.MODOS.VER) {
                        field.templateOptions.disabled = true;
                        _this.form.disable({ onlySelf: false, emitEvent: true });
                    }
                    else {
                        field.templateOptions.disabled = false;
                        _this.form.disable({ onlySelf: true, emitEvent: true });
                    }
                    if (field.type == 'select' && !field.templateOptions.multiple) {
                        // el nombre del key debe ser igual al del servicio inyectado         
                        _this.procesarTipoSelect(field);
                    }
                    if (field.type == 'select' && field.templateOptions.multiple) {
                        // el nombre del key debe ser igual al del servicio inyectado         
                        _this.procesarTipoSelectMultiple(field);
                    }
                    if (field.type == 'autocomplete') {
                        _this.procesarTipoAutocomplete(field);
                    }
                });
            });
            this.steps = this.jsonEditar;
            this.form = new forms.FormArray(this.steps.map(function () { return new forms.FormGroup({}); }), forms.Validators.required);
            this.options = this.steps.map(function () { return ({}); });
            if (this.modo == Configuracion.MODOS.VER) {
                this.form.disable({ onlySelf: false, emitEvent: false });
            }
        };
        EditarComponent.prototype.procesarTipoSelectMultiple = function (field) {
            var _this = this;
            var service = this.injector.get(this.serviciosConfig.servicios[field.data.servicio.toLowerCase()].servicio);
            if (field.data.lista && field.data.lista == 'eager') {
                this.cargando++;
            }
            service.getAll().subscribe(function (r) {
                field.templateOptions.options = _this.convertirOpciones(r, field.prop);
                field.templateOptions.compareWith = _this.compareWith;
                if (field.data.lista && field.data.lista == 'eager') {
                    _this.cargando--;
                }
            });
        };
        EditarComponent.prototype.procesarTipoAutocomplete = function (field) {
            var _this = this;
            field.templateOptions.compareWith = this.compareWith;
            field.validators = {
                ip: {
                    expression: function (c) { return c.value.id; },
                    message: function (error, field) { return "\"" + field.formControl.value + "\", ingrese un valor existente."; },
                }
            };
            if (this.model[field.key]) {
                this.model[field.key].toString = function () { return this.model[field.key][field.data.prop]; };
            }
            field.templateOptions.filter = (function (term) { return _this.getTerm(field.data.servicio.toLowerCase(), term, field.data.prop); });
        };
        EditarComponent.prototype.procesarTipoSelect = function (field) {
            var _this = this;
            var service = this.injector.get(this.serviciosConfig.servicios[field.data.servicio.toLowerCase()].servicio);
            if (field.data.lista && field.data.lista == 'eager') {
                this.cargando++;
            }
            service.getAll().subscribe(function (r) {
                field.templateOptions.options = _this.convertirOpciones(r, field.prop);
                field.templateOptions.compareWith = _this.compareWith;
                if (field.data.lista && field.data.lista == 'eager') {
                    _this.cargando--;
                }
            });
        };
        EditarComponent.prototype.setearModo = function () {
            if (this.rutaActiva.snapshot.params.ver && this.rutaActiva.snapshot.params.ver == "ver") {
                this.modo = Configuracion.MODOS.VER;
                this.cargando++;
                this.modoEditar(this.rutaActiva.snapshot.params.id);
                return;
            }
            if (this.rutaActiva.snapshot.params.id) {
                this.modo = Configuracion.MODOS.EDITAR;
                this.cargando++;
                this.modoEditar(this.rutaActiva.snapshot.params.id);
                return;
            }
            this.modo = Configuracion.MODOS.NUEVO;
        };
        EditarComponent.prototype.modoEditar = function (id) {
            var _this = this;
            this.service.get(id).subscribe(function (r) {
                _this.model = _this.convertirToString(r);
                _this.cargando--;
            });
        };
        EditarComponent.prototype.convertirToString = function (r) {
            this.jsonEditar.forEach(function (step) {
                step.fields.forEach(function (field) {
                    if (field.type == 'autocomplete') {
                        r[field.key].toString = function () { return r[field.key][field.data.prop]; };
                    }
                });
            });
            return r;
        };
        EditarComponent.prototype.convertirOpciones = function (r, opcion) {
            var opciones = [];
            for (var i = 0; i < r.length; i++) {
                var element = r[i];
                opciones.push({ value: element, label: element[opcion] });
            }
            return opciones;
        };
        EditarComponent.prototype.prevStep = function (step) {
            this.activedStep = step - 1;
        };
        EditarComponent.prototype.nextStep = function (step) {
            this.activedStep = step + 1;
        };
        EditarComponent.prototype.submit = function () {
            var _this = this;
            console.log(this.model);
            this.service.nuevo(this.model).subscribe(function (r) {
                _this.servicioMensajes.mostrarMensaje(TipoMensaje.EXITO, new EntidadMensaje(_this.service.json.mensaje));
                _this.location.back();
            }, function (e) {
                console.log(e);
            });
        };
        EditarComponent.prototype.getTexto = function (field) {
            var texto = this.model[field.key];
            if (!texto) {
                return "";
            }
            if (field.type == "select") {
                texto = texto[field.prop];
            }
            if (field.type == "autocomplete") {
                texto = texto[field.data.prop];
            }
            return texto;
        };
        EditarComponent.prototype.getLabel = function (field) {
            return field.templateOptions.label;
        };
        /* getTerm(term: any) {
           return this.injector.get(ServiciosConfig.servicios.zona.servicio).getAll();
         }
       */
        EditarComponent.prototype.getTerm = function (servicioString, term, arg) {
            var _this = this;
            return this.injector.get(this.serviciosConfig.servicios[servicioString].servicio).
                getPaginaServidorBusqueda(0, 20, [[arg, term]]).pipe(operators.map(function (n) { return _this.convertirRespuestaAutocomplete(n, arg); }));
        };
        EditarComponent.prototype.convertirRespuestaAutocomplete = function (array, arg) {
            var arrayConvert = [];
            console.log(array);
            var _loop_1 = function (index) {
                var element = array.content[index];
                element.toString = function () { return element[arg]; };
                arrayConvert.push(element);
            };
            for (var index = 0; index < array.content.length; index++) {
                _loop_1(index);
            }
            return arrayConvert;
        };
        EditarComponent.ctorParameters = function () { return [
            { type: GenericoService },
            { type: core.Injector },
            { type: router.ActivatedRoute }
        ]; };
        EditarComponent = __decorate([
            core.Component({
                selector: 'app-editar',
                template: "<mat-progress-bar mode=\"indeterminate\"   [hidden]=\"0==cargando\"></mat-progress-bar>\n<form [formGroup]=\"form\" (ngSubmit)=\"submit()\" [hidden]=\"cargando>0\">\n\n  <mat-horizontal-stepper>\n    \n    <mat-step *ngFor=\"let step of steps; let index = index; let last = last;\"   >\n      <mat-list *ngIf=\"modo==0\" class=\"row\">\n        <mat-list-item *ngFor=\"let field of step.fields\" >\n          <mat-form-field class=\"example-full-width\">\n            <mat-label>{{getLabel(field)}}</mat-label>\n            <input matInput readonly [value]=\"getTexto(field)\">\n          </mat-form-field>\n           </mat-list-item>\n        \n       </mat-list>\n      <ng-template matStepLabel>{{ step.label }}</ng-template>\n      <formly-form *ngIf=\"modo!=0\"\n        [form]=\"form.at(index)\"\n        [model]=\"model\"\n        [fields]=\"step.fields\"\n        [options]=\"options[index]\">\n      </formly-form>\n      \n      <div>\n        <button *ngIf=\"index !== 0\" matStepperPrevious class=\"btn btn-primary\" type=\"button\" (click)=\"prevStep(index)\"><i class=\"fa fa-angle-double-left\"></i></button>\n        <button *ngIf=\"!last && modo!=0\" matStepperNext class=\"btn btn-primary\" type=\"button\" [disabled]=\"!form.at(index).valid\" (click)=\"nextStep(index)\"><i class=\"fa fa-angle-double-right\"></i></button>\n        <button *ngIf=\"!last && modo==0\" matStepperNext class=\"btn btn-primary\" type=\"button\"  (click)=\"nextStep(index)\"><i class=\"fa fa-angle-double-right\"></i></button>\n        <button *ngIf=\"last\" class=\"btn btn-primary\" [disabled]=\"!form.valid\" type=\"submit\" style=\"margin-left: 1em;\">Aceptar</button>\n      </div>\n    </mat-step>\n  </mat-horizontal-stepper>\n</form>\n\n",
                styles: [".editar-item input:disabled{color:#000!important}"]
            })
        ], EditarComponent);
        return EditarComponent;
    }());

    var TextoComponent = /** @class */ (function (_super) {
        __extends(TextoComponent, _super);
        function TextoComponent(cd) {
            var _this = _super.call(this, cd) || this;
            _this.cd = cd;
            return _this;
        }
        TextoComponent.prototype.ngAfterViewInit = function () {
        };
        TextoComponent.prototype.ngOnInit = function () {
        };
        TextoComponent.ctorParameters = function () { return [
            { type: core.ChangeDetectorRef }
        ]; };
        TextoComponent = __decorate([
            core.Component({
                selector: 'app-texto',
                template: "\n{{propiedad}}\n",
                styles: [""]
            })
        ], TextoComponent);
        return TextoComponent;
    }(ElementoTablaComponent));

    var BotonComponent = /** @class */ (function (_super) {
        __extends(BotonComponent, _super);
        function BotonComponent(cd) {
            var _this = _super.call(this, cd) || this;
            _this.cd = cd;
            return _this;
        }
        BotonComponent.prototype.ngOnInit = function () {
        };
        BotonComponent.ctorParameters = function () { return [
            { type: core.ChangeDetectorRef }
        ]; };
        BotonComponent = __decorate([
            core.Component({
                selector: 'app-boton',
                template: "<button type=\"button\" [ngClass]=\"[col.clase]\">{{elemento[col.prop]}}</button>",
                styles: [""]
            })
        ], BotonComponent);
        return BotonComponent;
    }(ElementoTablaComponent));

    var FieldGenericoComponent = /** @class */ (function () {
        function FieldGenericoComponent(cfr) {
            this.cfr = cfr;
            this.col = { prop: '' };
        }
        FieldGenericoComponent.prototype.ngOnInit = function () {
        };
        FieldGenericoComponent.prototype.ngAfterViewInit = function () {
            var compFactory = this.cfr.resolveComponentFactory(this.getComponent(this.col.tipo));
            var ref = this.viewContainerRef.createComponent(compFactory);
            ref.instance.col = this.col;
            ref.instance.elemento = this.elemento;
            ref.instance.observador = this.observador;
        };
        FieldGenericoComponent.prototype.getComponent = function (prop) {
            switch (prop) {
                case 'texto':
                    return TextoComponent;
                    break;
                case 'boton':
                    return BotonComponent;
                    break;
                case 'boton-icono':
                    return BotonIconoComponent;
                    break;
                default:
                    return TextoComponent;
                    break;
            }
        };
        FieldGenericoComponent.ctorParameters = function () { return [
            { type: core.ComponentFactoryResolver }
        ]; };
        __decorate([
            core.Input()
        ], FieldGenericoComponent.prototype, "col", void 0);
        __decorate([
            core.Input()
        ], FieldGenericoComponent.prototype, "elemento", void 0);
        __decorate([
            core.Input()
        ], FieldGenericoComponent.prototype, "observador", void 0);
        __decorate([
            core.ViewChild('dynamic', { read: core.ViewContainerRef })
        ], FieldGenericoComponent.prototype, "viewContainerRef", void 0);
        FieldGenericoComponent = __decorate([
            core.Component({
                selector: 'app-field-generico',
                template: " <ng-template #dynamic></ng-template>\n",
                styles: [""]
            })
        ], FieldGenericoComponent);
        return FieldGenericoComponent;
    }());

    var BusquedaComponent = /** @class */ (function () {
        function BusquedaComponent() {
            this.texto = "";
            this.habilitado = true;
        }
        BusquedaComponent.prototype.ngOnInit = function () {
        };
        BusquedaComponent.prototype.limpiar = function () {
            this.texto = "";
            this.buscar(this.texto);
            this.habilitado = true;
        };
        BusquedaComponent.prototype.buscar = function (texto) {
            this.habilitado = false;
            this.buscador.buscar(texto);
        };
        __decorate([
            core.Input()
        ], BusquedaComponent.prototype, "buscador", void 0);
        __decorate([
            core.Input()
        ], BusquedaComponent.prototype, "texto", void 0);
        BusquedaComponent = __decorate([
            core.Component({
                selector: 'app-busqueda',
                template: "<div class=\"input-group mb-3\">  \n  <input [disabled]=\"!habilitado\" [(ngModel)]=\"texto\" type=\"text\" class=\"form-control\" placeholder=\"Buscar...\" aria-label=\"Realizar b\u00FAsqueda\" aria-describedby=\"basic-addon2\">\n  <div class=\"input-group-append\" *ngIf=\"!habilitado\">\n    <button  (click)=\"limpiar()\" class=\"btn btn-outline-danger\" type=\"button\"><i class=\"fa fa-times-circle\"></i></button>\n  </div>\n  <div class=\"input-group-append\"  *ngIf=\"habilitado\">\n    <button  (click)=\"buscar(texto)\" class=\"btn btn-outline-primary\" type=\"button\"><i class=\"fa fa-search\"></i></button>\n  </div>\n</div> ",
                styles: [""]
            })
        ], BusquedaComponent);
        return BusquedaComponent;
    }());

    var ModalTablaComponent = /** @class */ (function () {
        function ModalTablaComponent(modalRef) {
            this.modalRef = modalRef;
            this.item = {};
            this.columnas = [];
            this.acciones = [];
        }
        ModalTablaComponent.prototype.getThis = function () {
            return this;
        };
        ModalTablaComponent.prototype.nuevo = function () {
            this.modalRef.hide();
            this.observador.nuevo();
        };
        ModalTablaComponent.prototype.editar = function (id) {
            this.modalRef.hide();
            this.observador.editar(id);
        };
        ModalTablaComponent.prototype.eliminar = function (id) {
            this.modalRef.hide();
            this.observador.eliminar(id);
        };
        ModalTablaComponent.prototype.ver = function (id) {
            this.modalRef.hide();
            this.observador.ver(id);
        };
        ModalTablaComponent.prototype.ngOnInit = function () {
        };
        ModalTablaComponent.prototype.getTitulo = function () {
            var titulo = this.columnas.find(function (col) { return col.titulo; });
            if (titulo) {
                return this.item[titulo.prop];
            }
            return "Logisoft";
        };
        ModalTablaComponent.ctorParameters = function () { return [
            { type: public_api.BsModalRef }
        ]; };
        ModalTablaComponent = __decorate([
            core.Component({
                selector: 'app-modal-tabla',
                template: "<div class=\"modal-header\">\n  <h4 class=\"modal-title pull-left\" id=\"my-modal-title\">{{getTitulo()}}</h4>\n  <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"modalRef.hide()\">\n    <span aria-hidden=\"true\">&times;</span>\n  </button>   \n  \n</div>\n\n<div class=\"modal-body\">\n  <div class=\"col-12 \">\n    <app-field-generico class=\"float-right\" style=\"padding: 0.1rem;\" *ngFor=\"let accion of acciones\" [col]=\"accion\" [elemento]=\"item\" [observador]=\"getThis()\" ></app-field-generico>\n  </div>\n  <br>\n  \n    <ul class=\"list-group col-12\">    \n      <li *ngFor=\"let columna of columnas\" class=\"list-group-item disabled\">\n        <div class=\"row \"><h6>{{columna.nombre}}</h6></div>      \n        <div class=\"row justify-content-center align-items-center\">\n          <app-field-generico  [col]=\"columna\" [elemento]=\"item\">\n          </app-field-generico>\n        </div>\n       \n      </li>\n      \n    </ul>\n  \n  \n  \n</div>",
                styles: [""]
            })
        ], ModalTablaComponent);
        return ModalTablaComponent;
    }());

    var AccionesComponent = /** @class */ (function () {
        function AccionesComponent() {
        }
        AccionesComponent.prototype.ngOnInit = function () {
        };
        AccionesComponent.prototype.nuevo = function () {
            this.componente.nuevo();
        };
        __decorate([
            core.Input()
        ], AccionesComponent.prototype, "componente", void 0);
        AccionesComponent = __decorate([
            core.Component({
                selector: 'app-acciones',
                template: "<div class=\"input-group mb-3\">  \n  <button  (click)=\"nuevo()\" class=\"btn btn-outline-success\" type=\"button\">\n    <i class=\"fa fa-plus-circle\"></i>\n    <span> Nuevo</span>\n  </button>\n</div>\n",
                styles: [""]
            })
        ], AccionesComponent);
        return AccionesComponent;
    }());

    var PanelBotonAtrasComponent = /** @class */ (function () {
        function PanelBotonAtrasComponent(router) {
            this.router = router;
        }
        PanelBotonAtrasComponent.prototype.ngOnInit = function () {
        };
        PanelBotonAtrasComponent.prototype.existePantallaAtras = function () {
            return LogisoftStorage.isAtras();
        };
        PanelBotonAtrasComponent.prototype.atras = function () {
            this.router.navigate([LogisoftStorage.getPantallaAtras().url]);
        };
        PanelBotonAtrasComponent.ctorParameters = function () { return [
            { type: router.Router }
        ]; };
        PanelBotonAtrasComponent = __decorate([
            core.Component({
                selector: 'app-panel-boton-atras',
                template: "<div class=\"row mb-4 ml-0\">\n \n    <button type=\"button\" class=\"btn btn-info\" *ngIf=\"existePantallaAtras()\" (click)=\"atras()\"><i class=\"fa fa-chevron-circle-left\"></i> Atr\u00E1s</button>\n \n</div>\n",
                styles: [""]
            })
        ], PanelBotonAtrasComponent);
        return PanelBotonAtrasComponent;
    }());

    var TablaComponent = /** @class */ (function () {
        function TablaComponent(injector, service, modalService, router) {
            this.injector = injector;
            this.service = service;
            this.modalService = modalService;
            this.router = router;
            this.cargando = true;
            this.data = [];
            this.titulo = '';
            this.columnas = [];
            this.acciones = [];
            this.opcionesPaginacion = {
                pageSizeOptions: [5, 10, 25, 100],
                length: 0,
                pageSize: 10
            };
            this.textoBuscar = null;
            this.columnas = this.service.getCampos().camposListado;
            this.acciones = this.service.getCampos().accionesGenerales;
            this.inicioConstructor();
            this.servicioMensajes = this.injector.get(MensajesService);
            this.cd = this.injector.get(core.ChangeDetectorRef);
        }
        TablaComponent.prototype.inicioConstructor = function () {
            var _this = this;
            if (this.verificarPantallaAtras()) {
                return;
            }
            this.service.getPaginaServidor(0, this.opcionesPaginacion.pageSize).subscribe(function (r) {
                _this.data = r.content;
                _this.opcionesPaginacion.length = r.totalElements;
                _this.cargando = false;
            }, function (error) {
                console.log(error);
            });
        };
        TablaComponent.prototype.verificarPantallaAtras = function () {
            if (LogisoftStorage.isAtras()) {
                if (LogisoftStorage.getPantallaAtras().url == this.router.url) {
                    return true;
                }
            }
            return false;
        };
        TablaComponent.prototype.ngOnInit = function () {
        };
        TablaComponent.prototype.ngAfterViewInit = function () {
            if (this.verificarPantallaAtras()) {
                this.getParametrosAtras();
            }
            this.paginador.page.subscribe(this.cambioDePagina());
        };
        TablaComponent.prototype.cambioDePagina = function () {
            var _this = this;
            return function (r) {
                _this.paginacionConBusqueda(r);
            };
        };
        TablaComponent.prototype.paginacionConBusqueda = function (r) {
            var _this = this;
            this.cargando = true;
            var parametros = this.getParametros();
            this.service.getPaginaServidorBusqueda(r.pageIndex, r.pageSize, parametros).subscribe(function (r) {
                _this.opcionesPaginacion.length = r.totalElements;
                _this.data = r.content;
                _this.cargando = false;
            }, function (error) {
                console.log(error);
            });
        };
        TablaComponent.prototype.getParametros = function () {
            var _this = this;
            var parametros = [];
            if (!this.textoBuscar || this.textoBuscar.length == 0) {
                return parametros;
            }
            this.columnas.forEach(function (col) {
                if (!col.propBuscar) {
                    col.propBuscar = col.prop;
                }
                parametros.push([col.propBuscar, _this.textoBuscar]);
            });
            return parametros;
        };
        //para "suscribirse como observador"
        TablaComponent.prototype.getThis = function () {
            return this;
        };
        //interface buscar
        TablaComponent.prototype.buscar = function (texto) {
            this.textoBuscar = texto;
            this.paginador.pageIndex = 0;
            this.paginacionConBusqueda(this.paginador);
        };
        TablaComponent.prototype.abrirInfo = function (item) {
            this.modalRef = this.modalService.show(ModalTablaComponent);
            this.modalRef.content.item = item;
            this.modalRef.content.columnas = this.columnas;
            this.modalRef.content.acciones = this.acciones;
            this.modalRef.content.observador = this;
        };
        //interface acciones generales
        TablaComponent.prototype.nuevo = function () {
            this.setParametrosAtras();
            this.router.navigate([this.router.url + "/editar"]);
        };
        TablaComponent.prototype.editar = function (id) {
            this.setParametrosAtras();
            this.router.navigate([this.router.url + "/editar/" + id]);
        };
        TablaComponent.prototype.eliminar = function (id) {
            var _this = this;
            this.servicioMensajes.mostrarMensaje(TipoMensaje.ELIMINAR, new EntidadMensaje(this.service.getCampos()
                .mensaje)).then(function (result) {
                if (result.value) {
                    _this.service.delete(id).subscribe(function (r) {
                        _this.servicioMensajes.mostrarMensaje(TipoMensaje.EXITO_ELIMINAR, new EntidadMensaje(_this.service.getCampos()
                            .mensaje));
                        _this.paginacionConBusqueda(_this.paginador);
                    }, function (e) {
                        _this.servicioMensajes.mostrarMensaje(TipoMensaje.ERROR, e);
                    });
                }
            });
        };
        TablaComponent.prototype.ver = function (id) {
            this.setParametrosAtras();
            this.router.navigate([this.router.url + "/editar/" + id + "/ver"]);
        };
        //fin interface acciones generales
        //interface pantallaAtras
        TablaComponent.prototype.setParametrosAtras = function () {
            var parametros = {
                paginador: this.convertirPaginador(this.paginador),
                busqueda: this.textoBuscar
            };
            LogisoftStorage.setPantallaAtras({
                url: this.router.url,
                parametros: parametros
            });
        };
        TablaComponent.prototype.convertirPaginador = function (paginador) {
            return {
                length: paginador.length,
                pageIndex: paginador.pageIndex,
                pageSize: paginador.pageSize,
                previousPageIndex: 0
            };
        };
        TablaComponent.prototype.getParametrosAtras = function () {
            var pantallaAtras = LogisoftStorage.getPantallaAtras();
            this.textoBuscar = pantallaAtras.parametros.busqueda;
            if (this.textoBuscar && this.textoBuscar.length > 0) {
                //seteo el texto en el buscador
                this.busqueda.texto = this.textoBuscar;
                this.busqueda.habilitado = false;
            }
            var paginador = pantallaAtras.parametros.paginador;
            this.paginador.length = paginador.length;
            this.paginador.pageIndex = paginador.pageIndex;
            this.paginador.pageSize = paginador.pageSize;
            this.cd.detectChanges();
            this.paginacionConBusqueda(this.paginador);
            LogisoftStorage.borrarPantallaAtras();
        };
        TablaComponent.ctorParameters = function () { return [
            { type: core.Injector },
            { type: GenericoService },
            { type: modal.BsModalService },
            { type: router.Router }
        ]; };
        __decorate([
            core.ViewChild(paginator.MatPaginator)
        ], TablaComponent.prototype, "paginador", void 0);
        __decorate([
            core.ViewChild(BusquedaComponent)
        ], TablaComponent.prototype, "busqueda", void 0);
        TablaComponent = __decorate([
            core.Component({
                selector: 'app-tabla',
                template: "<h4 class=\"text-muted mb-4\">{{titulo}} </h4>\n<div>\n  <div class=\"card border-0 rounded-0 col-md-12\">\n    <div class=\"card-body cuerpo\" style=\"padding-left: 0px;\n    padding-right: 0px;\">\n    <div class=\"row\">\n      <div class=\"col-12 col-md-6\">\n        <app-busqueda [buscador]=\"getThis()\" ></app-busqueda>  \n      </div>\n      <div class=\"col-12 col-md-6\">\n        <app-acciones [componente]=\"getThis()\"></app-acciones>\n      </div>\n    </div> \n     \n      <div  class=\"table-responsive-md\">\n        <table class=\"table table-hover\">\n          <thead>\n            <tr>\n              <th scope=\"col\" *ngFor=\"let columna of columnas\" [ngClass]=\"{'d-none d-sm-none d-md-block':columna.ocultarMovil}\">{{columna.nombre}}</th>\n              <th scope=\"col\" [ngClass]=\"{'d-block d-sm-block d-md-none':true}\">Info</th>\n              <th scope=\"col\" [ngClass]=\"{'d-none d-sm-none d-md-block':true}\"  *ngIf=\"acciones.length>0\">Acciones</th>\n            </tr>\n          </thead>\n          <mat-progress-bar mode=\"indeterminate\" [hidden]=\"!cargando\"></mat-progress-bar>\n          <tbody [hidden]=\"cargando\">            \n            <tr *ngFor=\"let item of data\">\n              <td *ngFor=\"let columna of columnas\" [ngClass]=\"{'d-none d-sm-none d-md-block':columna.ocultarMovil}\">\n                <app-field-generico [col]=\"columna\" [elemento]=\"item\">\n                </app-field-generico>\n              </td>              \n              <td  [ngClass]=\"{'d-block d-sm-block d-md-none':true}\">\n                <button (click)=\"abrirInfo(item)\" type=\"button\" class=\"btn btn-outline-success\"><i class=\"fa fa-info-circle\"></i></button>\n              </td>\n              <td [ngClass]=\"{'d-none d-sm-none d-md-block':true}\" *ngIf=\"acciones.length>0\">                \n                  <app-field-generico style=\"padding: 0.1rem;\" *ngFor=\"let accion of acciones\" [col]=\"accion\" [elemento]=\"item\" [observador]=\"getThis()\" ></app-field-generico>\n              </td>\n            </tr>\n          </tbody>\n        </table>\n      </div>\n    </div>\n  </div>\n</div>\n<mat-paginator [length]=\"opcionesPaginacion.length\" [pageSize]=\"opcionesPaginacion.pageSize\"\n  [pageSizeOptions]=\"opcionesPaginacion.pageSizeOptions\">\n</mat-paginator>\n\n",
                styles: [".cuerpo{padding-left:0!important;padding-right:0!important}"]
            })
        ], TablaComponent);
        return TablaComponent;
    }());

    var ListaComponent = /** @class */ (function () {
        function ListaComponent() {
        }
        ListaComponent.prototype.ngOnInit = function () {
        };
        ListaComponent = __decorate([
            core.Component({
                selector: 'app-lista',
                template: "<p>\n  lista works!\n</p>\n",
                styles: [""]
            })
        ], ListaComponent);
        return ListaComponent;
    }());

    var MostrarTextoComponent = /** @class */ (function () {
        function MostrarTextoComponent() {
        }
        MostrarTextoComponent.prototype.ngOnInit = function () {
        };
        MostrarTextoComponent = __decorate([
            core.Component({
                selector: 'app-mostrar-texto',
                template: "<p>\n  mostrar-texto works!\n</p>\n",
                styles: [""]
            })
        ], MostrarTextoComponent);
        return MostrarTextoComponent;
    }());

    var MaterialModule = /** @class */ (function () {
        function MaterialModule() {
        }
        MaterialModule = __decorate([
            core.NgModule({
                declarations: [],
                imports: [
                    common.CommonModule,
                    forms.ReactiveFormsModule,
                    autocomplete.MatAutocompleteModule,
                    forms.FormsModule,
                    paginator.MatPaginatorModule,
                    progressBar.MatProgressBarModule,
                    stepper.MatStepperModule,
                    formField.MatFormFieldModule,
                    input.MatInputModule,
                    material.FormlyMaterialModule,
                    toggle.FormlyMatToggleModule,
                    list.MatListModule,
                    table.MatTableModule,
                    icon.MatIconModule
                ],
                exports: [
                    forms.ReactiveFormsModule,
                    autocomplete.MatAutocompleteModule,
                    forms.FormsModule,
                    paginator.MatPaginatorModule,
                    progressBar.MatProgressBarModule,
                    stepper.MatStepperModule,
                    formField.MatFormFieldModule,
                    input.MatInputModule,
                    material.FormlyMaterialModule,
                    toggle.FormlyMatToggleModule,
                    list.MatListModule,
                    table.MatTableModule,
                    icon.MatIconModule
                ]
            })
        ], MaterialModule);
        return MaterialModule;
    }());

    var AutocompleteTypeComponent = /** @class */ (function (_super) {
        __extends(AutocompleteTypeComponent, _super);
        function AutocompleteTypeComponent() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        AutocompleteTypeComponent.prototype.ngOnInit = function () {
            var _this = this;
            _super.prototype.ngOnInit.call(this);
            this.filter = this.formControl.valueChanges
                .pipe(operators.startWith(''), operators.switchMap(function (term) { return _this.to.filter(term); }));
        };
        AutocompleteTypeComponent.prototype.ngAfterViewInit = function () {
            _super.prototype.ngAfterViewInit.call(this);
            // temporary fix for https://github.com/angular/material2/issues/6728
            this.autocomplete._formField = this.formField;
        };
        AutocompleteTypeComponent.prototype.borrar = function () {
            this.value = "";
        };
        AutocompleteTypeComponent.prototype.seleccionar = function (value) {
        };
        __decorate([
            core.ViewChild(input.MatInput)
        ], AutocompleteTypeComponent.prototype, "formFieldControl", void 0);
        __decorate([
            core.ViewChild(autocomplete.MatAutocompleteTrigger)
        ], AutocompleteTypeComponent.prototype, "autocomplete", void 0);
        AutocompleteTypeComponent = __decorate([
            core.Component({
                selector: 'formly-autocomplete-type',
                template: "  <div class=\"row\">\n    \n    \n     <input class=\"col\" style=\"padding-left: 1rem;\" matInput \n      [matAutocomplete]=\"auto\"\n      [formControl]=\"formControl\"\n      [formlyAttributes]=\"field\"\n      [placeholder]=\"to.placeholder\"\n      [errorStateMatcher]=\"errorStateMatcher\"\n      >\n      <div class=\"col-1\" style=\"color: red;\">\n        <button  style=\"padding-left: 0px;\" class=\"btn btn-link \" *ngIf=\"value\" aria-label=\"Clear\" (click)=\"borrar()\">\n          <i class=\"fa fa-trash\"></i>\n         </button>\n      </div>\n      <mat-autocomplete  #auto=\"matAutocomplete\">\n      \n        <mat-option  (click)=\"seleccionar(value)\" *ngFor=\"let value of filter | async\" [value]=\"value\">\n          {{ value }}\n        </mat-option>      \n      </mat-autocomplete>\n  </div>\n  \n\n \n  \n  \n    \n"
            })
        ], AutocompleteTypeComponent);
        return AutocompleteTypeComponent;
    }(material.FieldType));

    var GenericoModule = /** @class */ (function () {
        function GenericoModule() {
        }
        GenericoModule = __decorate([
            core.NgModule({
                declarations: [ListaComponent, TablaComponent, TextoComponent, FieldGenericoComponent, ElementoTablaComponent, BotonComponent, BusquedaComponent, ModalTablaComponent, AccionesComponent, BotonIconoComponent, EditarComponent, MostrarTextoComponent, AutocompleteTypeComponent, PanelBotonAtrasComponent],
                imports: [
                    common.CommonModule,
                    forms.ReactiveFormsModule,
                    core$1.FormlyModule.forRoot({ types: [{
                                name: 'autocomplete',
                                component: AutocompleteTypeComponent,
                                wrappers: ['form-field'],
                            }],
                        validationMessages: [
                            { name: 'required', message: 'Campo requerido.' },
                        ] }),
                    MaterialModule
                ],
                exports: [core$1.FormlyModule, TablaComponent, TextoComponent, FieldGenericoComponent, BotonComponent, BusquedaComponent, ModalTablaComponent, AccionesComponent, MaterialModule, PanelBotonAtrasComponent, MaterialModule]
            })
        ], GenericoModule);
        return GenericoModule;
    }());

    exports.AccionesComponent = AccionesComponent;
    exports.BotonComponent = BotonComponent;
    exports.BotonIconoComponent = BotonIconoComponent;
    exports.BusquedaComponent = BusquedaComponent;
    exports.Configuracion = Configuracion;
    exports.EditarComponent = EditarComponent;
    exports.FieldGenericoComponent = FieldGenericoComponent;
    exports.GenericoModule = GenericoModule;
    exports.GenericoService = GenericoService;
    exports.LogisoftStorage = LogisoftStorage;
    exports.MaterialModule = MaterialModule;
    exports.MensajesService = MensajesService;
    exports.ModalTablaComponent = ModalTablaComponent;
    exports.PanelBotonAtrasComponent = PanelBotonAtrasComponent;
    exports.TablaComponent = TablaComponent;
    exports.TextoComponent = TextoComponent;
    exports.ɵa = ElementoTablaComponent;
    exports.ɵb = ListaComponent;
    exports.ɵc = MostrarTextoComponent;
    exports.ɵd = AutocompleteTypeComponent;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=ivai-generico-libreria.umd.js.map
